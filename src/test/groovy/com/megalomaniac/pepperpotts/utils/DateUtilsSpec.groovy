package com.megalomaniac.pepperpotts.utils

import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month

class DateUtilsSpec extends Specification {

	DateUtils dateUtils

	void setup() {
		dateUtils = new DateUtils()
	}

	void "convertOrdinalDateToLocalDate should convert dates with ordinal numbers to LocalDate format."() {
		when:
		LocalDate result = dateUtils.convertOrdinalDateToLocalDate(givenDate)

		then:
		assert expected == result

		where:
		givenDate				|	expected
		"May 18th, 2020"		|	LocalDate.of(2020, 05, 18)
		"January 2nd, 1991"		|	LocalDate.of(1991, 01, 02)
	}

	void "convertStringToLocalDateTime should convert date string to LocalDateTime"() {
		given:
		LocalDateTime expected = LocalDateTime.of(2020, Month.JANUARY, 20, 03, 05, 30)

		when:
		LocalDateTime actual = dateUtils.convertStringToLocalDateTime("01/20/2020-03:05.30", "MM/dd/yyyy-HH:mm.ss")

		then:
		assert expected == actual
	}
}
