package com.megalomaniac.pepperpotts.db.tosaccount

import spock.lang.Specification

class TosAccountServiceSpec extends Specification {

	TosAccountService service
	TosAccountRepository tosAccountRepository = Mock(TosAccountRepository)

	void setup() {
		service = new TosAccountService(tosAccountRepository)
	}

	void "createTosAccount should create and save a tosAccount with an empty list of characters."() {
		given:
		String accountId = "12345"

		when:
		service.createTosAccount(accountId)

		then:
		1 * tosAccountRepository.save(_ as TosAccount) >> { TosAccount tosAccount ->
			assert accountId == tosAccount.getInGameAccountId()
			assert [] == tosAccount.getCharacters()
		}
	}
}
