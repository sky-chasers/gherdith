package com.megalomaniac.pepperpotts.db.character

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount
import com.megalomaniac.pepperpotts.utils.DateUtils
import spock.lang.Specification

import java.time.LocalDateTime

class CharacterServiceSpec extends Specification {

	CharacterService service

	DateUtils dateUtils = Mock(DateUtils)
	CharacterRepository characterRepository = Mock(CharacterRepository)

	void setup() {
		service = new CharacterService(
				dateUtils,
				characterRepository
		)
	}

	void "updateTeamNames should update team names of a given tos account."() {
		given:
		LocalDateTime now = LocalDateTime.now()

		Character character1 = Mock(Character)
		Character character2 = Mock(Character)

		List<Character> existingCharacters = [
				character1,
				character2
		]

		List<String> teamNamesToAdd = [
				"Oslo"
		]

		TosAccount tosAccount = Mock(TosAccount)
		tosAccount.getInGameAccountId() >> "123456"
		tosAccount.getCharacters() >> existingCharacters

		characterRepository.findByTosAccountAndTeamName(tosAccount, _ as String) >> { Optional.empty() }
		dateUtils.getCurrentDateTime() >> now

		when:
		service.updateTeamNames(tosAccount, teamNamesToAdd)

		then:
		1 * characterRepository.save(_ as Character) >> { Character actualCharacterSaved ->
			assert teamNamesToAdd[0] == actualCharacterSaved.getTeamName()
			assert now == actualCharacterSaved.getDateUpdated()
		}
	}

	void "updateTeamNames should not update if the given team name already exists."() {
		given:
		LocalDateTime now = LocalDateTime.now()

		Character character1 = Mock(Character)
		Character character2 = Mock(Character)

		List<Character> existingCharacters = [
				character1,
				character2
		]

		List<String> teamNamesToAdd = [
				"Oslo"
		]

		TosAccount tosAccount = Mock(TosAccount)
		tosAccount.getInGameAccountId() >> "123456"
		tosAccount.getCharacters() >> existingCharacters

		characterRepository.findByTosAccountAndTeamName(tosAccount, "Oslo") >> {
			Optional.of(character1)
		}
		dateUtils.getCurrentDateTime() >> now

		when:
		service.updateTeamNames(tosAccount, teamNamesToAdd)

		then:
		0 * characterRepository.save(*_)
	}
}
