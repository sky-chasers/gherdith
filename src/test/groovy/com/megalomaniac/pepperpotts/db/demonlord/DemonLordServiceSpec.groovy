package com.megalomaniac.pepperpotts.db.demonlord

import com.megalomaniac.pepperpotts.ghervis.api.DemonLord
import spock.lang.Specification

import java.time.LocalDateTime

class DemonLordServiceSpec extends Specification {

	DemonLordService demonLordService

	DemonLordRepository demonLordRepository = Mock(DemonLordRepository)

	void setup() {
		demonLordService = new DemonLordService(demonLordRepository)
	}

	void "fetchOrCreateDemonLord should create a demon lord if it does not exist."() {
		given:
		DemonLord demonLordName = DemonLord.DEMON_LORD_MIRTIS
		demonLordRepository.findByName(demonLordName) >> Optional.empty()
		LocalDateTime dateTimeSpawned = LocalDateTime.now()

		when:
		DemonLordSpawnDetails actualDemonLord = demonLordService.fetchOrCreateDemonLord(demonLordName, dateTimeSpawned)

		then:
		assert demonLordName == actualDemonLord.getName()
		assert dateTimeSpawned == actualDemonLord.getDateAppeared()
		assert null == actualDemonLord.getId()
	}

	void "fetchOrCreateDemonLord should return an existing demon lord if it exists."() {
		given:
		DemonLord demonLordName = DemonLord.DEMON_LORD_MIRTIS
		DemonLordSpawnDetails expectedDemonLord = Mock(DemonLordSpawnDetails)
		expectedDemonLord.getDateAppeared() >> LocalDateTime.now().minusHours(1)
		demonLordRepository.findByName(demonLordName) >> Optional.of(expectedDemonLord)

		LocalDateTime dateTimeSpawned = LocalDateTime.now()

		when:
		DemonLordSpawnDetails actualDemonLord = demonLordService.fetchOrCreateDemonLord(demonLordName, dateTimeSpawned)

		then:
		assert expectedDemonLord == actualDemonLord
		assert !actualDemonLord.getDateAppeared().isEqual(dateTimeSpawned)
	}
}
