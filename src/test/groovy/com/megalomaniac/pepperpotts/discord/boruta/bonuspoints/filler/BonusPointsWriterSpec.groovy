package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler

import com.google.api.services.sheets.v4.model.ValueRange
import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.BiddingPointDetail
import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.BiddingPointsTabReader
import com.megalomaniac.pepperpotts.google.sheets.SheetWriter
import spock.lang.Specification

class BonusPointsWriterSpec extends Specification {

	BonusPointsWriter service
	SheetWriter sheetWriter = Mock(SheetWriter)
	BiddingPointsTabReader biddingPointsTabReader = Mock(BiddingPointsTabReader)

	void setup() {
		service = new BonusPointsWriter(sheetWriter, biddingPointsTabReader)
	}

	void "writeBonusPointsToGoogleSheetWithId should throw a FailedToWriteBonusPointsException when an IOException is met while writing to the googleSheet."() {
		given:
		String googleSheetId = "googleSheetId000930"

		BonusPointsOutput bonusPointsOutput1 = Mock(BonusPointsOutput)
		bonusPointsOutput1.getTeamName() >> "Ghervis"
		bonusPointsOutput1.getBonusPoints() >> 10

		BonusPointsOutput bonusPointsOutput2 = Mock(BonusPointsOutput)
		bonusPointsOutput2.getTeamName() >> "Pepper"
		bonusPointsOutput2.getBonusPoints() >> 41

		List<BonusPointsOutput> bonusPointsOutputList = []

		BiddingPointDetail biddingPointDetail1 = Mock(BiddingPointDetail)
		biddingPointDetail1.getTeamName() >> "Pepper"
		biddingPointDetail1.getTotalBiddingPoints() >> 25
		biddingPointDetail1.getBorutaBonusPointsColumnLetter() >> "B"
		biddingPointDetail1.getGtwPoints() >> 7
		biddingPointDetail1.getBorutaPoints() >> 3
		biddingPointDetail1.getRowNumber() >> 1

		BiddingPointDetail biddingPointDetail2 = Mock(BiddingPointDetail)
		biddingPointDetail2.getTeamName() >> "Ghervis"
		biddingPointDetail2.getTotalBiddingPoints() >> 2
		biddingPointDetail2.getBorutaBonusPointsColumnLetter() >> "B"
		biddingPointDetail2.getGtwPoints() >> 1
		biddingPointDetail2.getBorutaPoints() >> 3
		biddingPointDetail2.getRowNumber() >> 2

		List< BiddingPointDetail> biddingPointDetailList = []

		biddingPointsTabReader.fetchBiddingPointDetails(googleSheetId) >> biddingPointDetailList
		sheetWriter.update(googleSheetId, _) >> { throw new IOException() }

		when:
		service.writeBonusPointsToGoogleSheetWithId(googleSheetId, bonusPointsOutputList)

		then:
		thrown(FailedToWriteBonusPointsException)
	}

	void "writeBonusPointsToGoogleSheetWithId write the correct details to the google sheet."() {
		given:
		String expectedGoogleSheetId = "googleSheetId000930"

		BonusPointsOutput bonusPointsOutput1 = Mock(BonusPointsOutput)
		bonusPointsOutput1.getTeamName() >> "Ghervis"
		bonusPointsOutput1.getBonusPoints() >> 10

		BonusPointsOutput bonusPointsOutput2 = Mock(BonusPointsOutput)
		bonusPointsOutput2.getTeamName() >> "Pepper"
		bonusPointsOutput2.getBonusPoints() >> 41

		List<BonusPointsOutput> bonusPointsOutputList = [
				bonusPointsOutput1,
				bonusPointsOutput2
		]

		BiddingPointDetail biddingPointDetail1 = Mock(BiddingPointDetail)
		biddingPointDetail1.getTeamName() >> "Pepper"
		biddingPointDetail1.getTotalBiddingPoints() >> 25
		biddingPointDetail1.getBorutaBonusPointsColumnLetter() >> "B"
		biddingPointDetail1.getGtwPoints() >> 7
		biddingPointDetail1.getBorutaPoints() >> 3
		biddingPointDetail1.getRowNumber() >> 1

		BiddingPointDetail biddingPointDetail2 = Mock(BiddingPointDetail)
		biddingPointDetail2.getTeamName() >> "Ghervis"
		biddingPointDetail2.getTotalBiddingPoints() >> 2
		biddingPointDetail2.getBorutaBonusPointsColumnLetter() >> "B"
		biddingPointDetail2.getGtwPoints() >> 1
		biddingPointDetail2.getBorutaPoints() >> 3
		biddingPointDetail2.getRowNumber() >> 2

		List< BiddingPointDetail> biddingPointDetailList = [
				biddingPointDetail1,
				biddingPointDetail2
		]

		biddingPointsTabReader.fetchBiddingPointDetails(expectedGoogleSheetId) >> biddingPointDetailList

		when:
		service.writeBonusPointsToGoogleSheetWithId(expectedGoogleSheetId, bonusPointsOutputList)

		then:
		1 * sheetWriter.update(expectedGoogleSheetId, _ as List<ValueRange>) >> { String actualGoogleSheetId, List<ValueRange> actualValueWritten ->
			assert expectedGoogleSheetId == actualGoogleSheetId

			assert "Bidding Points!B1" == actualValueWritten.get(0).getRange()
			assert "41" == actualValueWritten.get(0).getValues().get(0).get(0).toString()

			assert "Bidding Points!B2" == actualValueWritten.get(1).getRange()
			assert "10" == actualValueWritten.get(1).getValues().get(0).get(0).toString()
		}
	}

	void "writeBonusPointsToGoogleSheetWithId should write the value as 0 if the team name in the bidding point detail does not exist in the bonus points output."() {
		given:
		String expectedGoogleSheetId = "googleSheetId000930"

		BonusPointsOutput bonusPointsOutput1 = Mock(BonusPointsOutput)
		bonusPointsOutput1.getTeamName() >> "Ghervis"
		bonusPointsOutput1.getBonusPoints() >> 10

		List<BonusPointsOutput> bonusPointsOutputList = [
				bonusPointsOutput1
		]

		BiddingPointDetail biddingPointDetail1 = Mock(BiddingPointDetail)
		biddingPointDetail1.getTeamName() >> "Pepper"
		biddingPointDetail1.getTotalBiddingPoints() >> 25
		biddingPointDetail1.getBorutaBonusPointsColumnLetter() >> "B"
		biddingPointDetail1.getGtwPoints() >> 7
		biddingPointDetail1.getBorutaPoints() >> 3
		biddingPointDetail1.getRowNumber() >> 1

		BiddingPointDetail biddingPointDetail2 = Mock(BiddingPointDetail)
		biddingPointDetail2.getTeamName() >> "Ghervis"
		biddingPointDetail2.getTotalBiddingPoints() >> 2
		biddingPointDetail2.getBorutaBonusPointsColumnLetter() >> "B"
		biddingPointDetail2.getGtwPoints() >> 1
		biddingPointDetail2.getBorutaPoints() >> 3
		biddingPointDetail2.getRowNumber() >> 2

		List< BiddingPointDetail> biddingPointDetailList = [
				biddingPointDetail1,
				biddingPointDetail2
		]

		biddingPointsTabReader.fetchBiddingPointDetails(expectedGoogleSheetId) >> biddingPointDetailList

		when:
		service.writeBonusPointsToGoogleSheetWithId(expectedGoogleSheetId, bonusPointsOutputList)

		then:
		1 * sheetWriter.update(expectedGoogleSheetId, _ as List<ValueRange>) >> { String actualGoogleSheetId, List<ValueRange> actualValueWritten ->
			assert expectedGoogleSheetId == actualGoogleSheetId

			assert "Bidding Points!B1" == actualValueWritten.get(0).getRange()
			assert "0" == actualValueWritten.get(0).getValues().get(0).get(0).toString()

			assert "Bidding Points!B2" == actualValueWritten.get(1).getRange()
			assert "10" == actualValueWritten.get(1).getValues().get(0).get(0).toString()
		}
	}
}
