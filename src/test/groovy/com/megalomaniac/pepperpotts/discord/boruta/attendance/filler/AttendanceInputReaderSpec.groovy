package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler

import com.megalomaniac.pepperpotts.google.sheets.SheetReader
import spock.lang.Specification

class AttendanceInputReaderSpec extends Specification {

	AttendanceInputReader service
	SheetReader sheetReader = Mock(SheetReader)
	String borutaAttendanceInputRange = "scratch!A2:BU"
	RaidInputContentsReader raidInputContentsReader = Mock(RaidInputContentsReader)
	RaidTypeInputRangeMapper raidTypeInputRangeMapper = Mock(RaidTypeInputRangeMapper)

	void setup() {
		service = new AttendanceInputReader(sheetReader, raidTypeInputRangeMapper, raidInputContentsReader)
	}

	void "fetchContentsOfGoogleSheetId should throw an EmptyAttendanceInputException if the input is empty."() {
		given:
		String googleSheetId = "1234567"

		List<List<Object>> response = []
		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, borutaAttendanceInputRange) >> response

		when:
		service.fetchContentsOfGoogleSheetId(googleSheetId, RaidType.GILTINE)

		then:
		thrown(EmptyAttendanceInputException)
	}

	void "fetchContentsOfGoogleSheetId should throw an EmptyAttendanceInputException if the input is null."() {
		given:
		String googleSheetId = "1234567"

		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, borutaAttendanceInputRange) >> null

		when:
		service.fetchContentsOfGoogleSheetId(googleSheetId, RaidType.GILTINE)

		then:
		thrown(EmptyAttendanceInputException)
	}

	void "fetchContentsOfGoogleSheetId should throw an AttendanceInputReaderIOException if the api failed to fetch the resource."() {
		given:
		String googleSheetId = "1234567"
		String borutaAttendanceInputRange = "Boruta Attendance!A2:BU"

		raidTypeInputRangeMapper.getInputRangeForRaidType(RaidType.BORUTA) >> borutaAttendanceInputRange

		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, borutaAttendanceInputRange) >> {
			throw new IOException()
		}

		when:
		service.fetchContentsOfGoogleSheetId(googleSheetId, RaidType.BORUTA)

		then:
		thrown(AttendanceInputReaderIOException)
	}

	void "fetchContentsOfGoogleSheetId should return the transformed contents of the given google sheet id."() {
		given:
		String googleSheetId = "1234567"
		String inputRange =  "Boruta Attendance!A2:BU"

		List<List<Object>> response = [
				[ "Butter", "Vedas Plateau (3760)", "Vedas Plateau (3760)" ],
				[ "Eggs", "Sulivinas Lair (4952)", "Offline" ]
		]

		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, inputRange) >> response
		raidTypeInputRangeMapper.getInputRangeForRaidType(RaidType.BORUTA) >> inputRange

		InputContents inputContents1 = Mock()
		inputContents1.getName() >> "Butter"
		inputContents1.getLocation() >> "Vedas Plateau (3760)"

		InputContents inputContents2 = Mock()
		inputContents2.getName() >> "Eggs"
		inputContents2.getLocation() >> "Sulivinas Lair (4952)"

		List<InputContents> expectedInputContents = [
				inputContents1,
				inputContents2
		]

		raidInputContentsReader.readLocationsWithRaidType(RaidType.BORUTA, response) >> expectedInputContents

		when:
		List<InputContents> actualInputContents = service.fetchContentsOfGoogleSheetId(googleSheetId, RaidType.BORUTA)

		then:
		expectedInputContents == actualInputContents
	}

}
