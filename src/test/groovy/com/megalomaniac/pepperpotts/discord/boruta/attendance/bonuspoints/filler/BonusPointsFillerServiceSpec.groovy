package com.megalomaniac.pepperpotts.discord.boruta.attendance.bonuspoints.filler

import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.FailedToReadBiddingPointsException
import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.LabelNotFoundException
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.*
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler.*
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPointsReader
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.FailedToFetchBonusPointsSheetException
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPoints
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import spock.lang.Specification

class BonusPointsFillerServiceSpec extends Specification {

	BonusPointsFillerService service
	BorutaAttendanceSheetReader borutaAttendanceSheetReader = Mock(BorutaAttendanceSheetReader)
	MessageSender messageSender = Mock(MessageSender)
	BonusPointsCalculator bonusPointsCalculator = Mock(BonusPointsCalculator)
	BonusPointDetailsCombiner detailsCombiner = Mock(BonusPointDetailsCombiner)
	RaidBonusPointsReader borutaBonusPointsReader = Mock(RaidBonusPointsReader)
	BonusPointsWriter bonusPointsWriter = Mock(BonusPointsWriter)
	GiltineAttendanceSheetReader giltineAttendanceSheetReader = Mock(GiltineAttendanceSheetReader)
	SessionCountCalculator sessionCountCalculator = Mock(SessionCountCalculator)

	void setup() {
		service = new BonusPointsFillerService(
				borutaAttendanceSheetReader,
				messageSender,
				bonusPointsCalculator,
				borutaBonusPointsReader,
				detailsCombiner,
				bonusPointsWriter,
				giltineAttendanceSheetReader,
				sessionCountCalculator
		)
	}

	void "fillBonusPoints should respond with an error message when a FailedToFetchBorutaAttendanceException is met." () {
		given:
		Message message = Mock(Message)
		String sheetId = "attendanceSheetId"

		FillBonusPointsCommand command = Mock(FillBonusPointsCommand)
		command.getMessage() >> message
		command.getAttendanceSheetId() >> sheetId

		borutaAttendanceSheetReader.fetchBorutaAttendance(sheetId) >> {
			throw new FailedToFetchBorutaAttendanceException()
		}

		when:
		service.fillBonusPoints(command)

		then:
		1 * messageSender.sendErrorMessage(message, "Failed to fetch Boruta attendance sheet with id: attendanceSheetId")
	}

	void "fillBonusPoints should respond with an error message when a FailedToFetchGiltineAttendanceException is met." () {
		given:
		Message message = Mock(Message)
		String sheetId = "attendanceSheetId"

		FillBonusPointsCommand command = Mock(FillBonusPointsCommand)
		command.getMessage() >> message
		command.getAttendanceSheetId() >> sheetId

		borutaAttendanceSheetReader.fetchBorutaAttendance(sheetId) >> [ ]
		giltineAttendanceSheetReader.fetchGiltineAttendance(sheetId) >> {
			throw  new FailedToFetchGiltineAttendanceException();
		}

		when:
		service.fillBonusPoints(command)

		then:
		1 * messageSender.sendErrorMessage(message, "Failed to fetch Giltine sheet with id: attendanceSheetId")
	}

	void "fillBonusPoints should respond with an error message when a FailedToFetchBonusPointsSheetException is met." () {
		given:
		Message message = Mock(Message)
		String sheetId = "attendanceSheetId"

		FillBonusPointsCommand command = Mock(FillBonusPointsCommand)
		command.getMessage() >> message
		command.getAttendanceSheetId() >> sheetId

		borutaAttendanceSheetReader.fetchBorutaAttendance(sheetId) >> []
		borutaBonusPointsReader.fetchBonusPointsDetails() >> {
			throw new FailedToFetchBonusPointsSheetException()
		}

		when:
		service.fillBonusPoints(command)

		then:
		1 * messageSender.sendErrorMessage(message, "Failed to fetch Boruta bonus points sheet.")
	}

	void "fillBonusPoints should respond with an error message when a FailedToReadBiddingPointsException is met." () {
		given:
		Message message = Mock(Message)
		int borutaSessionCount = 1
		int giltineSessionCount = 4
		String sheetId = "attendanceSheetId"

		FillBonusPointsCommand command = Mock(FillBonusPointsCommand)
		command.getMessage() >> message
		command.getAttendanceSheetId() >> sheetId

		List<BorutaAttendanceDetails> borutaAttendanceDetails = [ ]
		List<GiltineAttendanceDetails> giltineAttendanceDetails = [ ]
		List<RaidBonusPoints> raidBonusPoints = [ ]
		List<TeamBonusPointDetails> teamBonusPointDetails = [ ]

		borutaAttendanceSheetReader.fetchBorutaAttendance(sheetId) >> borutaAttendanceDetails
		giltineAttendanceSheetReader.fetchGiltineAttendance(sheetId) >> giltineAttendanceDetails
		borutaBonusPointsReader.fetchBonusPointsDetails() >> raidBonusPoints
		sessionCountCalculator.calculateBorutaSessionCount(borutaAttendanceDetails) >> borutaSessionCount
		sessionCountCalculator.calculateGiltineSessionCount(giltineAttendanceDetails) >> giltineSessionCount
		detailsCombiner.combineDetails(borutaAttendanceDetails, giltineAttendanceDetails, raidBonusPoints, giltineSessionCount, borutaSessionCount) >> teamBonusPointDetails
		bonusPointsWriter.writeBonusPointsToGoogleSheetWithId(sheetId, []) >> { throw new FailedToReadBiddingPointsException() }

		when:
		service.fillBonusPoints(command)

		then:
		1 * messageSender.sendErrorMessage(message, "Failed to read bidding points sheet with id: ${sheetId} Please make sure it's in the correct format.")
	}

	void "fillBonusPoints should respond with an error message when a FailedToWriteBonusPointsException is met." () {
		given:
		Message message = Mock(Message)
		int borutaSessionCount = 1
		int giltineSessionCount = 4
		String sheetId = "attendanceSheetId"

		FillBonusPointsCommand command = Mock(FillBonusPointsCommand)
		command.getMessage() >> message
		command.getAttendanceSheetId() >> sheetId

		List<BorutaAttendanceDetails> borutaAttendanceDetails = [ ]
		List<GiltineAttendanceDetails> giltineAttendanceDetails = [ ]
		List<RaidBonusPoints> raidBonusPoints = [ ]
		List<TeamBonusPointDetails> teamBonusPointDetails = [ ]

		borutaAttendanceSheetReader.fetchBorutaAttendance(sheetId) >> borutaAttendanceDetails
		giltineAttendanceSheetReader.fetchGiltineAttendance(sheetId) >> giltineAttendanceDetails
		borutaBonusPointsReader.fetchBonusPointsDetails() >> raidBonusPoints
		sessionCountCalculator.calculateBorutaSessionCount(borutaAttendanceDetails) >> borutaSessionCount
		sessionCountCalculator.calculateGiltineSessionCount(giltineAttendanceDetails) >> giltineSessionCount
		detailsCombiner.combineDetails(borutaAttendanceDetails, giltineAttendanceDetails, raidBonusPoints, giltineSessionCount, borutaSessionCount) >> teamBonusPointDetails
		bonusPointsWriter.writeBonusPointsToGoogleSheetWithId(sheetId, []) >> { throw new FailedToWriteBonusPointsException() }

		when:
		service.fillBonusPoints(command)

		then:
		1 * messageSender.sendErrorMessage(message, "Failed to write bonus bidding points to sheet with id: ${sheetId}")
	}

	void "fillBonusPoints should respond with a success message if no exception was met and the bonus points were properly allocated." () {
		given:
		Message message = Mock(Message)
		int borutaSessionCount = 1
		int giltineSessionCount = 4
		String sheetId = "attendanceSheetId"

		FillBonusPointsCommand command = Mock(FillBonusPointsCommand)
		command.getMessage() >> message
		command.getAttendanceSheetId() >> sheetId

		List<BorutaAttendanceDetails> borutaAttendanceDetails = [ ]
		List<GiltineAttendanceDetails> giltineAttendanceDetails = [ ]
		List<RaidBonusPoints> raidBonusPoints = [ ]
		List<TeamBonusPointDetails> teamBonusPointDetails = [ ]

		borutaAttendanceSheetReader.fetchBorutaAttendance(sheetId) >> borutaAttendanceDetails
		giltineAttendanceSheetReader.fetchGiltineAttendance(sheetId) >> giltineAttendanceDetails
		borutaBonusPointsReader.fetchBonusPointsDetails() >> raidBonusPoints
		sessionCountCalculator.calculateBorutaSessionCount(borutaAttendanceDetails) >> borutaSessionCount
		sessionCountCalculator.calculateGiltineSessionCount(giltineAttendanceDetails) >> giltineSessionCount
		detailsCombiner.combineDetails(borutaAttendanceDetails, giltineAttendanceDetails, raidBonusPoints, giltineSessionCount, borutaSessionCount) >> teamBonusPointDetails

		when:
		service.fillBonusPoints(command)

		then:
		1 * bonusPointsWriter.writeBonusPointsToGoogleSheetWithId(sheetId, [])
		1 * messageSender.sendSuccessMessage(message, _ as String)
	}

	void "fillBonusPoints should respond with an error message if LabelNotFoundException is thrown." () {
		given:
		Message message = Mock(Message)
		int borutaSessionCount = 1
		int giltineSessionCount = 4
		String sheetId = "attendanceSheetId"

		FillBonusPointsCommand command = Mock(FillBonusPointsCommand)
		command.getMessage() >> message
		command.getAttendanceSheetId() >> sheetId

		List<BorutaAttendanceDetails> borutaAttendanceDetails = [ ]
		List<GiltineAttendanceDetails> giltineAttendanceDetails = [ ]
		List<RaidBonusPoints> raidBonusPoints = [ ]
		List<TeamBonusPointDetails> teamBonusPointDetails = [ ]

		borutaAttendanceSheetReader.fetchBorutaAttendance(sheetId) >> borutaAttendanceDetails
		giltineAttendanceSheetReader.fetchGiltineAttendance(sheetId) >> giltineAttendanceDetails
		borutaBonusPointsReader.fetchBonusPointsDetails() >> raidBonusPoints
		sessionCountCalculator.calculateBorutaSessionCount(borutaAttendanceDetails) >> borutaSessionCount
		sessionCountCalculator.calculateGiltineSessionCount(giltineAttendanceDetails) >> giltineSessionCount
		detailsCombiner.combineDetails(borutaAttendanceDetails, giltineAttendanceDetails, raidBonusPoints, giltineSessionCount, borutaSessionCount) >> teamBonusPointDetails
		bonusPointsWriter.writeBonusPointsToGoogleSheetWithId(sheetId, []) >> { throw new LabelNotFoundException("bonus points", "error message")}

		when:
		service.fillBonusPoints(command)

		then:
		1 * messageSender.sendErrorMessage(message, "Failed to read google sheet with id: ${sheetId}. Label: **bonus points** does not exist.")
	}
}
