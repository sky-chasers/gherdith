package com.megalomaniac.pepperpotts.discord.boruta.attendance.bonuspoints.reader

import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPoints
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPointsReader
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.FailedToFetchBonusPointsSheetException
import com.megalomaniac.pepperpotts.google.sheets.SheetReader
import spock.lang.Specification

class RaidBonusPointsReaderSpec extends Specification {

	RaidBonusPointsReader service

	SheetReader sheetReader = Mock(SheetReader)
	String bonusPointsSheetId = "xxxxx"
	String bonusPointsCharactersRange = "Boruta Characters!A1:Z400"
	String bonusPointsBiddingPointsLabel = "Bonus Bidding Points"
	String bonusPointsTeamNameLabel = "Team Name"

	void setup() {
		service = new RaidBonusPointsReader(
				sheetReader,
				bonusPointsSheetId,
				bonusPointsCharactersRange,
				bonusPointsBiddingPointsLabel,
				bonusPointsTeamNameLabel
		)
	}

	void "fetchBonusPointsDetails should return the correct details."() {
		given:
		List<List<Object>> rawOutput = [
				["Team Name", "Physical Defence", "Bonus Bidding Points"],
				["Eggs", 10_000, 10],
				["Ghervis", 5_000, 0],
				["Ketchup", 1_000, 4],
				["Butter", 1_000],
				["Lxy", 1_000, null],
		]

		sheetReader.fetchGoogleSheetWithIdAndRange(bonusPointsSheetId, bonusPointsCharactersRange) >> rawOutput

		when:
		List<RaidBonusPoints> actualResult = service.fetchBonusPointsDetails()

		then:
		assert "Eggs" == actualResult.get(0).getTeamName()
		assert "Ghervis" == actualResult.get(1).getTeamName()
		assert "Ketchup" == actualResult.get(2).getTeamName()
		assert "Butter" == actualResult.get(3).getTeamName()
		assert "Lxy" == actualResult.get(4).getTeamName()

		assert new BigDecimal(10) == actualResult.get(0).getMaxPossibleBonusPoints()
		assert new BigDecimal(0) == actualResult.get(1).getMaxPossibleBonusPoints()
		assert new BigDecimal(4) == actualResult.get(2).getMaxPossibleBonusPoints()
		assert new BigDecimal(0) == actualResult.get(3).getMaxPossibleBonusPoints()
		assert new BigDecimal(0) == actualResult.get(4).getMaxPossibleBonusPoints()
	}

	void "fetchBonusPointsDetails should return FailedToFetchBonusPointsSheetException when an IOException is made."() {
		given:
		sheetReader.fetchGoogleSheetWithIdAndRange(bonusPointsSheetId, bonusPointsCharactersRange) >> {
			throw new IOException()
		}

		when:
		service.fetchBonusPointsDetails()

		then:
		thrown(FailedToFetchBonusPointsSheetException)
	}
}
