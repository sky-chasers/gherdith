package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler

import com.google.api.services.sheets.v4.model.ValueRange
import com.megalomaniac.pepperpotts.google.sheets.SheetWriter
import spock.lang.Specification

class AttendanceSheetWriterSpec extends Specification {

	AttendanceSheetWriter writer
	AttendanceOutputReader outputReader = Mock(AttendanceOutputReader)
	SheetWriter sheetWriter = Mock(SheetWriter)
	RaidTypeOutputRangeMapper outputRangeMapper = Mock(RaidTypeOutputRangeMapper)

	void setup() {
		writer = new AttendanceSheetWriter(outputReader, sheetWriter, outputRangeMapper)
	}

	void "recordAttendanceToGoogleSheetWithId should throw AttendanceSheetWriterException if the google sheet id is invalid."() {
		given:
		String sheetId = "1234567"
		RaidType raidType = RaidType.GILTINE
		String tabName = "Boruta Attendance"

		outputRangeMapper.getTabNameForRaidType(raidType) >> tabName

		InputContents inputContent1 = Mock(InputContents)
		inputContent1.getName() >> "Butter"

		InputContents inputContent2 = Mock(InputContents)
		inputContent2.getName() >> "Eggs"

		List<InputContents> inputContents = [
				inputContent1,
				inputContent2
		]

		OutputContents outputContent1 = Mock(OutputContents)
		outputContent1.getName() >> "Butter"

		OutputContents outputContent2 = Mock(OutputContents)
		outputContent2.getName() >> "Eggs"

		List<OutputContents> outputContents = [
				outputContent1,
				outputContent2
		]

		outputReader.fetchGoogleSheetWithId(sheetId, raidType) >> outputContents
		sheetWriter.update(sheetId, _) >> { throw new IOException() }

		when:
		writer.recordAttendanceToGoogleSheetWithId(sheetId, inputContents, raidType)

		then:
		thrown(AttendanceSheetWriterException)
	}

	void "recordAttendanceToGoogleSheetWithId should write the correct values if the no exception was thrown."() {
		given:
		String sheetId = "1234567"
		RaidType raidType = RaidType.GILTINE
		String tabName = "Boruta Attendance"

		outputRangeMapper.getTabNameForRaidType(raidType) >> tabName

		InputContents inputContent1 = Mock(InputContents)
		inputContent1.getName() >> "Butter"
		inputContent1.getLocation() >> "Vedas"

		InputContents inputContent2 = Mock(InputContents)
		inputContent2.getName() >> "Eggs"
		inputContent2.getLocation() >> "N/A"

		List<InputContents> inputContents = [
				inputContent1,
				inputContent2
		]


		OutputContents outputContent1 = Mock(OutputContents)
		outputContent1.getName() >> "Butter"
		outputContent1.getNextEmptyRange() >> "D12"

		OutputContents outputContent2 = Mock(OutputContents)
		outputContent2.getName() >> "Eggs"
		outputContent2.getNextEmptyRange() >> "D13"

		List<OutputContents> outputContents = [
				outputContent1,
				outputContent2
		]

		outputReader.fetchGoogleSheetWithId(sheetId, raidType) >> outputContents

		when:
		writer.recordAttendanceToGoogleSheetWithId(sheetId, inputContents, raidType)

		then:

		1 * sheetWriter.update(_, _) >> { String actualSheetId, List<ValueRange> actualBody ->
			assert sheetId == actualSheetId
			assert "Boruta Attendance!D12" == actualBody.get(0).getRange()
			assert "Boruta Attendance!D13" == actualBody.get(1).getRange()
			assert ["Vedas"] == actualBody.get(0).getValues().get(0)
			assert ["N/A"] == actualBody.get(1).getValues().get(0)
 		}
	}
}
