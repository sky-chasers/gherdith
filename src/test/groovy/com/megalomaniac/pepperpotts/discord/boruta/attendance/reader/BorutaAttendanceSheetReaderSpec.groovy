package com.megalomaniac.pepperpotts.discord.boruta.attendance.reader

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.BorutaLocation
import com.megalomaniac.pepperpotts.google.sheets.SheetReader
import spock.lang.Specification

class BorutaAttendanceSheetReaderSpec extends Specification {

	BorutaAttendanceSheetReader service

	SheetReader sheetReader = Mock(SheetReader)
	String borutaAttendanceRange = "Boruta Attendance!A3:M200"

	void setup() {
		service = new BorutaAttendanceSheetReader(sheetReader, borutaAttendanceRange)
	}

	void "fetchBorutaAttendance should return the correct details."() {
		given:
		String googleSheetId = "xxxxx"
		List<List<Object>> rawOutput = [
			[ "Ghervis", 1, "Sulivinas Lair (4952)" ],
			[ "Eggs", 1, "Sub" ],
			[ "Butter", 0, "N/A" ],
			[ "Ketchup", 0, "Blallalalalalala" ]
		]

		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, borutaAttendanceRange) >> rawOutput

		when:
		List<BorutaAttendanceDetails> actualResult = service.fetchBorutaAttendance(googleSheetId)

		then:
		assert "Ghervis" == actualResult.get(0).getTeamName()
		assert "Eggs" == actualResult.get(1).getTeamName()
		assert "Butter" == actualResult.get(2).getTeamName()
		assert "Ketchup" == actualResult.get(3).getTeamName()

		assert BorutaLocation.SULIVINAS_LAIR == actualResult.get(0).getLocations().get(0)
		assert BorutaLocation.SUB == actualResult.get(1).getLocations().get(0)
		assert BorutaLocation.NOT_APPLICABLE == actualResult.get(2).getLocations().get(0)
		assert BorutaLocation.NOT_APPLICABLE == actualResult.get(3).getLocations().get(0)

		assert 1 == actualResult.get(0).getScore()
		assert 1 == actualResult.get(1).getScore()
		assert 0 == actualResult.get(2).getScore()
		assert 0 == actualResult.get(3).getScore()
	}

	void "fetchBorutaAttendance it should return FailedToFetchBorutaAttendanceException when an IOException is met."() {
		given:
		String googleSheetId = "yyyyy"

		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, borutaAttendanceRange) >> {
			throw new IOException()
		}

		when:
		service.fetchBorutaAttendance(googleSheetId)

		then:
		thrown(FailedToFetchBorutaAttendanceException)
	}
}
