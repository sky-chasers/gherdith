package com.megalomaniac.pepperpotts.discord.boruta.attendance.bonuspoints.filler

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.BorutaLocation
import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.GiltineLocation
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.BorutaAttendanceDetails
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.GiltineAttendanceDetails
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler.BonusPointsCalculator
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler.TeamBonusPointDetails
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPoints
import spock.lang.Specification

class BonusPointsCalculatorSpec extends Specification {

	BonusPointsCalculator bonusPointsCalculator

	void setup() {
		bonusPointsCalculator = new BonusPointsCalculator()
	}

	void "calculateBonusPoints should calculate the bonus points correctly for boruta participants."() {
		given:
		BorutaAttendanceDetails borutaAttendance1 = Mock(BorutaAttendanceDetails)
		borutaAttendance1.getTeamName() >> teamName
		borutaAttendance1.getScore() >> score
		borutaAttendance1.getLocations() >> borutaLocations

		GiltineAttendanceDetails giltineAttendance1 = Mock(GiltineAttendanceDetails)
		giltineAttendance1.getTeamName() >> teamName
		giltineAttendance1.getScore() >> 0
		giltineAttendance1.getLocations() >> [GiltineLocation.NOT_APPLICABLE]

		RaidBonusPoints bonusPoint = Mock(RaidBonusPoints)
		bonusPoint.getTeamName() >> teamName
		bonusPoint.getMaxPossibleBonusPoints() >> maxPossiblePoints

		TeamBonusPointDetails bonusPointDetails = Mock(TeamBonusPointDetails)
		bonusPointDetails.getBorutaAttendanceDetails() >> borutaAttendance1
		bonusPointDetails.getGiltineAttendanceDetails() >> giltineAttendance1
		bonusPointDetails.getRaidBonusPoints() >> bonusPoint

		int giltineSessionCount = 1

		when:
		int actualValue = bonusPointsCalculator.calculateBonusPoints(bonusPointDetails, borutaSessionCount, giltineSessionCount)

		then:
		assert expectedValue == actualValue

		where:
		teamName	| expectedValue	| borutaSessionCount| score	| maxPossiblePoints	| borutaLocations
		"Ghervis"	| 4				| 2					| 1		| 8					| [BorutaLocation.SULIVINAS_LAIR]
		"Eggs"		| 8				| 2					| 3		| 8					| [BorutaLocation.SULIVINAS_LAIR, BorutaLocation.SUB]
		"Butter"	| 10			| 3					| 3		| 10				| [BorutaLocation.SULIVINAS_LAIR, BorutaLocation.SUB, BorutaLocation.SUB]
		"Trone"		| 4				| 3					| 1		| 10				| [BorutaLocation.SULIVINAS_LAIR]
		"SSherwin"	| 0				| 3					| 0		| 10				| []
		"Lki"		| 0				| 3					| 0		| 10				| [BorutaLocation.NOT_APPLICABLE]
		"Mark"		| 25			| 1					| 3		| 25				| [BorutaLocation.SULIVINAS_LAIR]
		"Lewis"		| 10			| 3					| 1		| 30				| [BorutaLocation.NOT_APPLICABLE, BorutaLocation.NOT_APPLICABLE, BorutaLocation.SULIVINAS_LAIR]
	}

	void "calculateBonusPoints should calculate the bonus points correctly for giltine participants."() {
		given:
		BorutaAttendanceDetails borutaAttendance1 = Mock(BorutaAttendanceDetails)
		borutaAttendance1.getTeamName() >> teamName
		borutaAttendance1.getScore() >> 0
		borutaAttendance1.getLocations() >> [BorutaLocation.NOT_APPLICABLE]

		GiltineAttendanceDetails giltineAttendance1 = Mock(GiltineAttendanceDetails)
		giltineAttendance1.getTeamName() >> teamName
		giltineAttendance1.getScore() >> score
		giltineAttendance1.getLocations() >> giltineLocations

		RaidBonusPoints bonusPoint = Mock(RaidBonusPoints)
		bonusPoint.getTeamName() >> teamName
		bonusPoint.getMaxPossibleBonusPoints() >> maxPossiblePoints

		TeamBonusPointDetails bonusPointDetails = Mock(TeamBonusPointDetails)
		bonusPointDetails.getBorutaAttendanceDetails() >> borutaAttendance1
		bonusPointDetails.getGiltineAttendanceDetails() >> giltineAttendance1
		bonusPointDetails.getRaidBonusPoints() >> bonusPoint

		int borutaSessionCount = 1

		when:
		int actualValue = bonusPointsCalculator.calculateBonusPoints(bonusPointDetails, borutaSessionCount, giltineSessionCount)

		then:
		assert expectedValue == actualValue

		where:
		teamName	| expectedValue	| giltineSessionCount	| score	| maxPossiblePoints	| giltineLocations
		"Ghervis"	| 4				| 2						| 1		| 8					| [GiltineLocation.DEMONIC_SANCTUARY]
		"Eggs"		| 8				| 2						| 3		| 8					| [GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.DEMONIC_SANCTUARY]
		"Butter"	| 10			| 3						| 3		| 10				| [GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.DEMONIC_SANCTUARY]
		"Trone"		| 5				| 2						| 1		| 10				| [GiltineLocation.DEMONIC_SANCTUARY]
		"SSherwin"	| 0				| 3						| 0		| 10				| []
		"Lki"		| 0				| 3						| 0		| 10				| [GiltineLocation.NOT_APPLICABLE]
		"Mark"		| 25			| 1						| 3		| 25				| [GiltineLocation.DEMONIC_SANCTUARY]
		"Lewis"		| 10			| 3						| 1		| 30				| [GiltineLocation.NOT_APPLICABLE, GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.NOT_APPLICABLE]
	}
}
