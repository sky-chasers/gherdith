package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler

import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import spock.lang.Specification

class AttendanceFillerServiceSpec extends Specification {

	AttendanceFillerService service
	AttendanceInputReader attendanceInputReader = Mock(AttendanceInputReader)
	MessageSender messageSender = Mock(MessageSender)
	AttendanceSheetWriter attendanceSheetWriter = Mock(AttendanceSheetWriter)

	void setup() {
		service = new AttendanceFillerService(attendanceInputReader, messageSender, attendanceSheetWriter)
	}

	void "fillAttendance should send an error message to the user when an AttendanceInputReaderIOException is met."() {
		given:
		Message message = Mock(Message)

		AttendanceFillerCommand attendanceFillerCommand = Mock(AttendanceFillerCommand)
		attendanceFillerCommand.getInputGoogleSheetId() >> "123456"
		attendanceFillerCommand.getOutputGoogleSheetId() >> "098766"
		attendanceFillerCommand.getCommand() >> "!attendance"
		attendanceFillerCommand.getMessage() >> message
		attendanceFillerCommand.getRaidType() >> RaidType.BORUTA

		attendanceInputReader.fetchContentsOfGoogleSheetId(attendanceFillerCommand.getInputGoogleSheetId(), RaidType.BORUTA) >> {
			throw new AttendanceInputReaderIOException()
		}

		when:
		service.fillAttendance(attendanceFillerCommand)

		then:
		1 * messageSender.sendErrorMessage(message, "Failed to fetch input sheet with id: 123456")
	}

	void "fillAttendance should send an error message to the user when an AttendanceSheetWriterException is met."() {
		given:
		Message message = Mock(Message)

		AttendanceFillerCommand attendanceFillerCommand = Mock(AttendanceFillerCommand)
		attendanceFillerCommand.getInputGoogleSheetId() >> "123456"
		attendanceFillerCommand.getOutputGoogleSheetId() >> "098766"
		attendanceFillerCommand.getCommand() >> "!fillBorutaAttendance"
		attendanceFillerCommand.getMessage() >> message
		attendanceFillerCommand.getRaidType() >> RaidType.BORUTA

		InputContents inputContent1 = Mock(InputContents)

		List<InputContents> inputContents = [
				inputContent1
		]

		attendanceInputReader.fetchContentsOfGoogleSheetId(attendanceFillerCommand.getInputGoogleSheetId(), RaidType.BORUTA) >> {
			return inputContents
		}

		attendanceSheetWriter.recordAttendanceToGoogleSheetWithId("098766", inputContents, RaidType.BORUTA) >> {
			throw new AttendanceSheetWriterException()
		}

		when:
		service.fillAttendance(attendanceFillerCommand)

		then:
		1 * messageSender.sendErrorMessage(message, "Failed to write output to google sheet with id: 098766")
	}

	void "fillAttendance should send an error message to the user when an EmptyAttendanceInputException is met."() {
		given:
		Message message = Mock(Message)

		AttendanceFillerCommand attendanceFillerCommand = Mock(AttendanceFillerCommand)
		attendanceFillerCommand.getInputGoogleSheetId() >> "123456"
		attendanceFillerCommand.getOutputGoogleSheetId() >> "098766"
		attendanceFillerCommand.getCommand() >> "!fillBorutaAttendance"
		attendanceFillerCommand.getMessage() >> message
		attendanceFillerCommand.getRaidType() >> RaidType.GILTINE

		attendanceInputReader.fetchContentsOfGoogleSheetId(attendanceFillerCommand.getInputGoogleSheetId(), RaidType.GILTINE) >> {
			throw new EmptyAttendanceInputException()
		}

		when:
		service.fillAttendance(attendanceFillerCommand)

		then:
		1 * messageSender.sendErrorMessage(message, "The input google sheet 123456 is empty.")
	}

	void "fillAttendance should record the attendance sheet if no exception was met."() {
		given:
		Message message = Mock(Message)

		AttendanceFillerCommand attendanceFillerCommand = Mock(AttendanceFillerCommand)
		attendanceFillerCommand.getInputGoogleSheetId() >> "123456"
		attendanceFillerCommand.getOutputGoogleSheetId() >> "098766"
		attendanceFillerCommand.getCommand() >> "!fillBorutaAttendance"
		attendanceFillerCommand.getMessage() >> message
		attendanceFillerCommand.getRaidType() >> RaidType.BORUTA

		InputContents inputContent1 = Mock(InputContents)

		List<InputContents> inputContents = [
				inputContent1
		]

		attendanceInputReader.fetchContentsOfGoogleSheetId(attendanceFillerCommand.getInputGoogleSheetId(), RaidType.BORUTA) >> {
			return inputContents
		}

		when:
		service.fillAttendance(attendanceFillerCommand)

		then:
		1 * attendanceSheetWriter.recordAttendanceToGoogleSheetWithId("098766", inputContents, RaidType.BORUTA)
		1 * messageSender.sendInfoMessage(message, "Please wait while I start filling the attendance sheet...")
		1 * messageSender.sendSuccessMessage(message, "Thank you for waiting. Attendance is successfully recorded.")
	}
}
