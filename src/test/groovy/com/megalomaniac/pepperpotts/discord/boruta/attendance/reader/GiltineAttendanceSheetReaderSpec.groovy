package com.megalomaniac.pepperpotts.discord.boruta.attendance.reader

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.GiltineLocation
import com.megalomaniac.pepperpotts.google.sheets.SheetReader
import spock.lang.Specification

class GiltineAttendanceSheetReaderSpec extends Specification {

	GiltineAttendanceSheetReader service
	SheetReader sheetReader = Mock(SheetReader)
	String giltineAttendanceRange = "Giltine Attendance!A3:M200"

	void setup() {
		service = new GiltineAttendanceSheetReader(sheetReader, giltineAttendanceRange)
	}

	void "fetchGiltineAttendance should return a list of details with the correct values."() {
		given:
		List<List<Object>> rawAttendance = [
				[ "Ghervis", 1, "Demonic Sanctuary (11230)" ],
				[ "Eggs", 1, "Sub" ],
				[ "Butter", 0, "N/A" ],
				[ "Ketchup", 0, "Blallalalalalala" ]
		]

		String sheetId = "sheetId"
		sheetReader.fetchGoogleSheetWithIdAndRange(sheetId, giltineAttendanceRange) >> rawAttendance

		when:
		List<GiltineAttendanceDetails> actualResult = service.fetchGiltineAttendance(sheetId)

		then: "team names should be correct"
		"Ghervis" == actualResult.get(0).getTeamName()
		"Eggs" == actualResult.get(1).getTeamName()
		"Butter" == actualResult.get(2).getTeamName()
		"Ketchup" == actualResult.get(3).getTeamName()

		and: "scores should be correct"
		1 == actualResult.get(0).getScore()
		1 == actualResult.get(1).getScore()
		0 == actualResult.get(2).getScore()
		0 == actualResult.get(3).getScore()

		and: "locations should be correct"
		[GiltineLocation.DEMONIC_SANCTUARY] == actualResult.get(0).getLocations()
		[GiltineLocation.SUB] == actualResult.get(1).getLocations()
		[GiltineLocation.NOT_APPLICABLE] == actualResult.get(2).getLocations()
		[GiltineLocation.NOT_APPLICABLE] == actualResult.get(3).getLocations()
	}

	void "fetchGiltineAttendance should throw FailedToFetchGiltineAttendanceException if fetching the sheet returned an error."() {
		given:
		String sheetId = "sheetId"

		sheetReader.fetchGoogleSheetWithIdAndRange(sheetId, giltineAttendanceRange) >> {
			throw new IOException()
		}

		when:
		service.fetchGiltineAttendance(sheetId)

		then:
		thrown(FailedToFetchGiltineAttendanceException)
	}
}
