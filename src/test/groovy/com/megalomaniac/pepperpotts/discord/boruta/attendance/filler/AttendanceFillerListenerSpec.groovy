package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler

import com.megalomaniac.pepperpotts.discord.command.AdminCommand
import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageAuthor
import org.javacord.api.entity.user.User
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.Specification

class AttendanceFillerListenerSpec extends Specification {

	AttendanceFillerListener attendanceFillerListener
	CommandValidator validator = Mock(CommandValidator)
	AttendanceFillerService attendanceFillerService = Mock(AttendanceFillerService)
	AdminCommand command = AdminCommand.FILL_RAID_ATTENDANCE
	MessageSender messageSender = Mock(MessageSender)

	void setup() {
		attendanceFillerListener = new AttendanceFillerListener(validator, attendanceFillerService, messageSender)
	}

	void "onMessageCreate should not fill attendance if the command is invalid."() {
		given:
		MessageCreateEvent messageCreateEvent = Mock(MessageCreateEvent)
		validator.isValidCommand(messageCreateEvent, command) >> false

		when:
		attendanceFillerListener.onMessageCreate(messageCreateEvent)

		then:
		0 * attendanceFillerService.fillAttendance(*_)
	}

	void "onMessageCreate should not fill attendance if the raid type is invalid."() {
		given:
		User recipient = Mock(User)
		MessageAuthor messageAuthor = Mock(MessageAuthor)
		messageAuthor.asUser() >> Optional.of(recipient)

		Message message = Mock(Message)
		message.getContent() >>  "!attendance xxx inputSheetId outputSheetId"

		MessageCreateEvent messageCreateEvent = Mock(MessageCreateEvent)
		messageCreateEvent.getMessage() >> message
		messageCreateEvent.getMessageAuthor() >> messageAuthor

		validator.isValidCommand(messageCreateEvent, command) >> true

		when:
		attendanceFillerListener.onMessageCreate(messageCreateEvent)

		then:
		0 * attendanceFillerService.fillAttendance(*_)
	}

	void "onMessageCreate should fill attendance if the command is valid."() {
		given:
		Message message = Mock(Message)
		message.getContent() >> "!attendance boruta googleSheetId1 googleSheetId2"

		User user = Mock(User)

		MessageAuthor messageAuthor = Mock(MessageAuthor)
		messageAuthor.asUser() >> Optional.of(user)

		MessageCreateEvent messageCreateEvent = Mock(MessageCreateEvent)
		messageCreateEvent.getMessage() >> message
		messageCreateEvent.getMessageAuthor() >> messageAuthor

		validator.isValidCommand(messageCreateEvent, command) >> true

		when:
		attendanceFillerListener.onMessageCreate(messageCreateEvent)

		then:
		1 * attendanceFillerService.fillAttendance(_ as AttendanceFillerCommand) >> {
			AttendanceFillerCommand actualCommand ->
				assert actualCommand.getMessage() == message
				assert actualCommand.getInputGoogleSheetId() == "googleSheetId1"
				assert actualCommand.getOutputGoogleSheetId() == "googleSheetId2"
		}
	}
}
