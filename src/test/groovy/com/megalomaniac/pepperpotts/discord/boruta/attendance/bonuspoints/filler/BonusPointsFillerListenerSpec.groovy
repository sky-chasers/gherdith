package com.megalomaniac.pepperpotts.discord.boruta.attendance.bonuspoints.filler

import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler.BonusPointsFillerListener
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler.BonusPointsFillerService
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler.FillBonusPointsCommand
import com.megalomaniac.pepperpotts.discord.command.AdminCommand
import com.megalomaniac.pepperpotts.discord.command.Command
import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import org.javacord.api.entity.message.Message
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.Specification

class BonusPointsFillerListenerSpec extends Specification {

	BonusPointsFillerListener listener
	CommandValidator validator = Mock(CommandValidator)
	BonusPointsFillerService bonusPointsFillerService = Mock(BonusPointsFillerService)
	Command command = AdminCommand.FILL_BORUTA_BONUS_POINTS

	void setup() {
		listener = new BonusPointsFillerListener(validator, bonusPointsFillerService)
	}

	void "onMessageCreate should do nothing if the command is invalid."() {
		given:
		String content = "!fillbonus 1cLrQTQRrsptmKJi79u0h99QE-FMJfPm0kQbTigC9L6A 3"

		Message message = Mock(Message)
		message.getContent() >> content

		MessageCreateEvent messageCreateEvent = Mock(MessageCreateEvent)
		messageCreateEvent.getMessage() >> message

		validator.isValidCommand(messageCreateEvent, command) >> false

		when:
		listener.onMessageCreate(messageCreateEvent)

		then:
		0 * bonusPointsFillerService.fillBonusPoints(_)
	}

	void "onMessageCreate should fill the bonus points if the command is valid."() {
		given:
		String attendanceSheetId = "1cLrQTQRrsptmKJi79u0h99QE-FMJfPm0kQbTigC9L6A"
		int numberOfSessions = 3
		String content = "!fillbonus ${attendanceSheetId} ${numberOfSessions}"

		Message message = Mock(Message)
		message.getContent() >> content

		MessageCreateEvent messageCreateEvent = Mock(MessageCreateEvent)
		messageCreateEvent.getMessage() >> message

		validator.isValidCommand(messageCreateEvent, command) >> true

		when:
		listener.onMessageCreate(messageCreateEvent)

		then:
		1 * bonusPointsFillerService.fillBonusPoints(_ as FillBonusPointsCommand) >> {
			FillBonusPointsCommand actualCommand ->
				assert message == actualCommand.getMessage()
				assert command.getCommandString() == actualCommand.getCommand()
				assert attendanceSheetId == actualCommand.getAttendanceSheetId()
		}
	}
}
