package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler

import com.megalomaniac.pepperpotts.google.sheets.SheetReader
import spock.lang.Specification

class AttendanceOutputReaderSpec extends Specification {

	AttendanceOutputReader reader
	SheetReader sheetReader = Mock(SheetReader)
	RaidTypeOutputRangeMapper outputRangeMapper = Mock(RaidTypeOutputRangeMapper)

	void setup() {
		reader = new AttendanceOutputReader(sheetReader, outputRangeMapper)
	}

	void "fetchGoogleSheetWithId should return a list of output contents with the correct value."() {
		given:
		RaidType raidType = RaidType.BORUTA
		String googleSheetId = "1239485jskf"

		List<List<Object>> response = [
				[ "Butter", 1, "N/A", "Vedas Plateau (3760)" ],
				[ "Eggs", 1, "N/A", "Sulivinas Lair (4952)" ]
		]

		String outputRange = "Boruta Attendance!A2:BU"

		outputRangeMapper.getOutputRangeForRaidType(raidType) >> outputRange
		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, outputRange) >> response

		when:
		List<OutputContents> actualOutput = reader.fetchGoogleSheetWithId(googleSheetId, raidType)

		then:
		assert "Butter" == actualOutput.get(0).getName()
		assert "E3" == actualOutput.get(0).getNextEmptyRange()

		assert "Eggs" == actualOutput.get(1).getName()
		assert "E4" == actualOutput.get(1).getNextEmptyRange()
	}
}
