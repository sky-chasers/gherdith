package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.BorutaLocation
import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.GiltineLocation
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.BorutaAttendanceDetails
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.GiltineAttendanceDetails;
import spock.lang.Specification;

class SessionCountCalculatorSpec extends Specification {

	SessionCountCalculator service

	void setup() {
		service = new SessionCountCalculator()
	}

	void "calculateGiltineSessionCount should return the correct value if all participants have the same number of sessions."() {
		given:
		GiltineAttendanceDetails details1 = Mock()
		details1.getTeamName() >> "Ghervis"
		details1.getLocations() >> [GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.NOT_APPLICABLE]

		GiltineAttendanceDetails details2 = Mock()
		details2.getTeamName() >> "Eggs"
		details2.getLocations() >> [GiltineLocation.NOT_APPLICABLE, GiltineLocation.DEMONIC_SANCTUARY]

		GiltineAttendanceDetails details3 = Mock()
		details3.getTeamName() >> "Butter"
		details3.getLocations() >> [GiltineLocation.NOT_APPLICABLE, GiltineLocation.NOT_APPLICABLE]

		List<GiltineAttendanceDetails> giltineAttendanceDetails = [
				details1,
				details2,
				details3
		]

		when:
		int actualSessionCount = service.calculateGiltineSessionCount(giltineAttendanceDetails)

		then:
		2 == actualSessionCount
	}

	void "calculateGiltineSessionCount should return the session count with the highest number of participants if the session counts are not equal among participants."() {
		given:
		GiltineAttendanceDetails details1 = Mock()
		details1.getTeamName() >> "Ghervis"
		details1.getLocations() >> [GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.NOT_APPLICABLE, GiltineLocation.DEMONIC_SANCTUARY]

		GiltineAttendanceDetails details2 = Mock()
		details2.getTeamName() >> "Eggs"
		details2.getLocations() >> [GiltineLocation.NOT_APPLICABLE, GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.DEMONIC_SANCTUARY]

		GiltineAttendanceDetails details3 = Mock()
		details3.getTeamName() >> "Butter"
		details3.getLocations() >> [GiltineLocation.NOT_APPLICABLE, GiltineLocation.NOT_APPLICABLE]

		GiltineAttendanceDetails details4 = Mock()
		details4.getTeamName() >> "Ketchup"
		details4.getLocations() >> [GiltineLocation.NOT_APPLICABLE, GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.DEMONIC_SANCTUARY, GiltineLocation.DEMONIC_SANCTUARY]

		List<GiltineAttendanceDetails> giltineAttendanceDetails = [
				details1,
				details2,
				details3,
				details4
		]

		when:
		int actualSessionCount = service.calculateGiltineSessionCount(giltineAttendanceDetails)

		then:
		4 == actualSessionCount
	}


	void "calculateBorutaSessionCount should return the session count with the highest number of participants if the session counts are not equal among participants."() {
		given:
		BorutaAttendanceDetails details1 = Mock()
		details1.getTeamName() >> "Ghervis"
		details1.getLocations() >> [BorutaLocation.NOT_APPLICABLE, BorutaLocation.SULIVINAS_LAIR]

		BorutaAttendanceDetails details2 = Mock()
		details2.getTeamName() >> "Eggs"
		details2.getLocations() >> [BorutaLocation.NOT_APPLICABLE, BorutaLocation.SULIVINAS_LAIR, BorutaLocation.NOT_APPLICABLE, BorutaLocation.SULIVINAS_LAIR]

		BorutaAttendanceDetails details3 = Mock()
		details3.getTeamName() >> "Butter"
		details3.getLocations() >> [BorutaLocation.NOT_APPLICABLE]

		BorutaAttendanceDetails details4 = Mock()
		details4.getTeamName() >> "Ketchup"
		details4.getLocations() >> [BorutaLocation.NOT_APPLICABLE, BorutaLocation.SULIVINAS_LAIR, BorutaLocation.NOT_APPLICABLE, BorutaLocation.SULIVINAS_LAIR]

		List<BorutaAttendanceDetails> borutaAttendanceDetails = [
				details1,
				details2,
				details3,
				details4
		]

		when:
		int actualSessionCount = service.calculateBorutaSessionCount(borutaAttendanceDetails)

		then:
		4 == actualSessionCount
	}

	void "calculateBorutaSessionCount should return the correct value if all participants have the same number of sessions."() {
		given:
		BorutaAttendanceDetails details1 = Mock()
		details1.getTeamName() >> "Ghervis"
		details1.getLocations() >> [BorutaLocation.NOT_APPLICABLE, BorutaLocation.NOT_APPLICABLE]

		BorutaAttendanceDetails details2 = Mock()
		details2.getTeamName() >> "Eggs"
		details2.getLocations() >> [BorutaLocation.NOT_APPLICABLE, BorutaLocation.NOT_APPLICABLE]

		BorutaAttendanceDetails details3 = Mock()
		details3.getTeamName() >> "Butter"
		details3.getLocations() >> [BorutaLocation.NOT_APPLICABLE, BorutaLocation.NOT_APPLICABLE]

		List<BorutaAttendanceDetails> borutaAttendanceDetails = [
				details1,
				details2,
				details3
		]

		when:
		int actualSessionCount = service.calculateBorutaSessionCount(borutaAttendanceDetails)

		then:
		2 == actualSessionCount
	}
}
