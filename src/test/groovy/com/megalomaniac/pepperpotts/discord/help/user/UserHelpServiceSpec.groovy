package com.megalomaniac.pepperpotts.discord.help.user

import com.megalomaniac.pepperpotts.discord.message.PublicMessageSender
import org.javacord.api.entity.channel.TextChannel
import org.javacord.api.entity.message.embed.EmbedBuilder
import spock.lang.Specification

class UserHelpServiceSpec extends Specification {

	UserHelpService service
	PublicMessageSender publicMessageSender = Mock(PublicMessageSender)
	UserHelpBuilder userHelpBuilder = Mock(UserHelpBuilder)

	void setup() {
		service = new UserHelpService(publicMessageSender, userHelpBuilder)
	}

	void "displayListOfValidCommands should send the help message."() {
		given:
		TextChannel textChannel = Mock(TextChannel)
		EmbedBuilder message = userHelpBuilder.buildHelpResponse()

		when:
		service.displayListOfValidCommands(textChannel)

		then:
		1 * publicMessageSender.sendMessage(textChannel, message)
	}
}
