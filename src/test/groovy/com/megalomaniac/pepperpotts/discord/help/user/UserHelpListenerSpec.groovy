package com.megalomaniac.pepperpotts.discord.help.user

import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import com.megalomaniac.pepperpotts.discord.command.GeneralCommand
import org.javacord.api.entity.channel.TextChannel
import org.javacord.api.event.message.MessageCreateEvent;
import spock.lang.Specification;

public class UserHelpListenerSpec extends Specification {

	UserHelpListener listener
	CommandValidator commandValidator = Mock(CommandValidator)
	UserHelpService userHelpService = Mock(UserHelpService)

	void setup() {
		listener = new UserHelpListener(commandValidator, userHelpService)
	}

	void "onMessageCreate should not display the list of valid commands if the command is invalid."() {
		given:
		MessageCreateEvent messageCreateEvent = Mock(MessageCreateEvent)
		commandValidator.isValidCommand(messageCreateEvent, GeneralCommand.HELP) >> false

		when:
		listener.onMessageCreate(messageCreateEvent)

		then:
		0 * userHelpService.displayListOfValidCommands(_)
	}

	void "onMessageCreate should display the list of valid commands if the command is valid."() {
		given:
		TextChannel textChannel = Mock(TextChannel)
		MessageCreateEvent messageCreateEvent = Mock(MessageCreateEvent)
		messageCreateEvent.getChannel() >> textChannel

		commandValidator.isValidCommand(messageCreateEvent, GeneralCommand.HELP) >> true

		when:
		listener.onMessageCreate(messageCreateEvent)

		then:
		1 * userHelpService.displayListOfValidCommands(textChannel)
	}
}
