package com.megalomaniac.pepperpotts.discord.help.admin

import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.embed.EmbedBuilder
import spock.lang.Specification

class AdminHelpServiceSpec extends Specification {

	AdminHelpService service
	MessageSender messageSender = Mock(MessageSender)
	AdminHelpBuilder adminHelpBuilder = Mock(AdminHelpBuilder)

	void setup() {
		service = new AdminHelpService(messageSender, adminHelpBuilder)
	}

	void "displayListOfValidCommands should build a response and send it to the user"() {
		given:
		Message messageDetails = Mock(Message)
		EmbedBuilder response = Mock(EmbedBuilder)

		adminHelpBuilder.buildHelpResponse() >> response

		when:
		service.displayListOfValidCommands(messageDetails)

		then:
		1 * messageSender.sendMessage(messageDetails, response)
	}
}
