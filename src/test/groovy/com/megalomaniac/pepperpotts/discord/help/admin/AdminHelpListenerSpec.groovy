package com.megalomaniac.pepperpotts.discord.help.admin

import com.megalomaniac.pepperpotts.discord.command.AdminCommand
import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import org.javacord.api.entity.message.Message
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.Specification

class AdminHelpListenerSpec extends Specification {

	AdminHelpListener listener
	CommandValidator commandValidator = Mock(CommandValidator)
	AdminHelpService adminHelpService = Mock(AdminHelpService)
	AdminCommand command = AdminCommand.ADMIN_HELP

	void setup() {
		listener = new AdminHelpListener(commandValidator, adminHelpService)
	}

	void "onMessageCreate should do nothing if the command is invalid."() {
		given:
		MessageCreateEvent event = Mock(MessageCreateEvent)
		commandValidator.isValidCommand(event, command) >> false

		when:
		listener.onMessageCreate(event)

		then:
		0 * adminHelpService.displayListOfValidCommands(_)
	}

	void "onMessageCreate should do display list of valid commands if the command received is valid."() {
		given:
		Message message = Mock(Message)
		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message
		commandValidator.isValidCommand(event, command) >> true

		when:
		listener.onMessageCreate(event)

		then:
		1 * adminHelpService.displayListOfValidCommands(message)
	}
}
