package com.megalomaniac.pepperpotts.discord.announce

import com.google.api.services.sheets.v4.model.SpreadsheetProperties
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import com.megalomaniac.pepperpotts.ghervis.api.GhervisApi
import com.megalomaniac.pepperpotts.google.sheets.SheetReader
import com.megalomaniac.pepperpotts.google.sheets.SheetUtils
import org.javacord.api.entity.message.Message
import org.json.JSONObject
import spock.lang.Specification

class AnnounceServiceSpec extends Specification {

	AnnounceService service
	GhervisApi ghervisApi = Mock(GhervisApi)
	AnnouncementFormatter formatter = Mock(AnnouncementFormatter)
	SheetReader sheetReader = Mock(SheetReader)
	MessageSender messageSender = Mock(MessageSender)
	SheetUtils sheetUtils = Mock(SheetUtils)

	void setup() {
		service = new AnnounceService(ghervisApi, formatter, sheetReader, messageSender, sheetUtils)
	}

	void "announceAttendance should reply with the appropriate error when the google sheet does not exist."() {
		given:
		Message message = Mock()

		AnnouncementCommand command = Mock()
		command.getAnnouncementType() >> AnnouncementType.GTW
		command.getGoogleSheetId() >> "sheet-id"
		command.getOriginalMessage() >> message

		sheetReader.fetchGoogleSheetPropertiesWithId(command.getGoogleSheetId()) >> {
			throw new IOException()
		}

		when:
		service.announceAttendance(command)

		then:
		0 * ghervisApi.sendChannelMessage(_)

		and:
		1 * messageSender.sendErrorMessage(message, "Failed to fetch google sheets with id: sheet-id")
	}

	void "announceAttendance should send the message to ghervis if the google sheets exist."() {
		given:
		Message message = Mock()

		AnnouncementCommand command = Mock()
		command.getAnnouncementType() >> AnnouncementType.GTW
		command.getGoogleSheetId() >> "sheet-id"
		command.getOriginalMessage() >> message

		SpreadsheetProperties spreadsheetProperties = new SpreadsheetProperties()
		spreadsheetProperties.setTitle("my title")

		JSONObject messageContents = Mock()

		sheetReader.fetchGoogleSheetPropertiesWithId(command.getGoogleSheetId()) >> spreadsheetProperties

		sheetUtils.constructUrlWithId(command.getGoogleSheetId()) >> "url.com"

		formatter.formatAnnouncement(AnnouncementType.GTW, "my title", "url.com") >> messageContents

		when:
		service.announceAttendance(command)

		then:
		1 * ghervisApi.sendChannelMessage(messageContents)

		and:
		0 * messageSender.sendErrorMessage(message, _)

		and:
		1 * messageSender.sendSuccessMessage(message, "Successfully announced message to GTW channel.")
	}
}
