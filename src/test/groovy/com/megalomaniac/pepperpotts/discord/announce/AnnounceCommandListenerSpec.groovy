package com.megalomaniac.pepperpotts.discord.announce

import com.megalomaniac.pepperpotts.discord.command.AdminCommand
import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.Specification

class AnnounceCommandListenerSpec extends Specification {

	AnnounceCommandListener announceCommandListener
	CommandValidator commandValidator = Mock(CommandValidator)
	AnnounceService announceService = Mock(AnnounceService)
	MessageSender messageSender = Mock(MessageSender)
	AdminCommand adminCommand = AdminCommand.ANNOUNCE_ATTENDANCE

	void setup() {
		announceCommandListener = new AnnounceCommandListener(commandValidator, announceService, messageSender)
	}

	void "onMessageCreate should do nothing if the command is invalid."() {
		given:
		Message message = Mock()
		message.getContent() >> "!some-invalid-command gtw some-google-sheet-id"

		MessageCreateEvent messageCreateEvent = Mock()
		messageCreateEvent.getMessage() >> message

		commandValidator.isValidCommand(messageCreateEvent, adminCommand) >> false

		when:
		announceCommandListener.onMessageCreate(messageCreateEvent)

		then:
		0 * announceService.announceAttendance(*_)
	}

	void "onMessageCreate should return the appropriate error if the type of announcement is invalid."() {
		given:
		Message message = Mock()
		message.getContent() >> "!announce invalid-type some-google-sheet-id"

		MessageCreateEvent messageCreateEvent = Mock()
		messageCreateEvent.getMessage() >> message

		commandValidator.isValidCommand(messageCreateEvent, adminCommand) >> true

		when:
		announceCommandListener.onMessageCreate(messageCreateEvent)

		then:
		0 * announceService.announceAttendance(*_)

		and:
		1 * messageSender.sendErrorMessage(message, "Invalid announcement type. Valid announcement types are: [`boruta` , `giltine` , `gtw` ].")
	}
}
