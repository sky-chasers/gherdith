package com.megalomaniac.pepperpotts.discord.message

import org.javacord.api.entity.channel.TextChannel
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.entity.user.User
import spock.lang.Specification

class MessageSenderSpec extends Specification {

	MessageSender service
	PublicMessageSender publicMessageSender = Mock()
	PrivateMessageSender privateMessageSender = Mock()
	MessageBuilder messageBuilder = Mock()

	void setup() {
		service = new MessageSender(privateMessageSender, publicMessageSender, messageBuilder)
	}

	void "sendMessage should reply with the message privately if the message is private."() {
		given:
		User user = Mock(User)
		Message message = Mock(Message)
		message.isPrivateMessage() >> true
		message.getUserAuthor() >> Optional.of(user)
		String messageToSend = "Hello world"

		when:
		service.sendMessage(message, messageToSend)

		then:
		1 * privateMessageSender.sendMessageToUser(user, messageToSend)
		0 * publicMessageSender.sendMessage(*_)
	}

	void "sendMessage should reply publicly if the message was received in public."() {
		given:
		TextChannel textChannel = Mock(TextChannel)
		User user = Mock(User)
		Message message = Mock(Message)
		message.isPrivateMessage() >> false
		message.getUserAuthor() >> Optional.of(user)
		message.getChannel() >> textChannel
		String messageToSend = "Hello world"

		when:
		service.sendMessage(message, messageToSend)

		then:
		0 * privateMessageSender.sendMessageToUser(*_)
		1 * publicMessageSender.sendMessage(textChannel, messageToSend)
	}

	void "sendMessage should reply with the message privately if the message is private and message to send is an EmbedBuilder."() {
		given:
		User user = Mock(User)
		Message message = Mock(Message)
		message.isPrivateMessage() >> true
		message.getUserAuthor() >> Optional.of(user)

		EmbedBuilder messageToSend = Mock(EmbedBuilder)

		when:
		service.sendMessage(message, messageToSend)

		then:
		1 * privateMessageSender.sendMessageToUser(user, messageToSend)
		0 * publicMessageSender.sendMessage(*_)
	}

	void "sendMessage should reply publicly if the message was received in public and message to send is an EmbedBuilder."() {
		given:
		TextChannel textChannel = Mock(TextChannel)
		User user = Mock(User)
		Message message = Mock(Message)
		message.isPrivateMessage() >> false
		message.getUserAuthor() >> Optional.of(user)
		message.getChannel() >> textChannel

		EmbedBuilder messageToSend = Mock(EmbedBuilder)

		when:
		service.sendMessage(message, messageToSend)

		then:
		0 * privateMessageSender.sendMessageToUser(*_)
		1 * publicMessageSender.sendMessage(textChannel, messageToSend)
	}
}
