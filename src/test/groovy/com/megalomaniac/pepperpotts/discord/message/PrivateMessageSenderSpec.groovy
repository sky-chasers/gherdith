package com.megalomaniac.pepperpotts.discord.message

import org.javacord.api.entity.message.embed.EmbedBuilder
import org.javacord.api.entity.user.User
import spock.lang.Specification

class PrivateMessageSenderSpec extends Specification {

	PrivateMessageSender service
	MessageBuilder messageBuilder = Mock()

	void setup() {
		service = new PrivateMessageSender(messageBuilder)
	}

	void "should send a private message if the user is not a bot and is not the bot himself."() {
		given:
		User validUser = Mock(User)
		validUser.isBot() >> false
		validUser.isYourself() >> false

		String message = "hello"

		when:
		service.sendMessageToUser(validUser, message)

		then:
		1 * validUser.sendMessage(message)
	}

	void "should not send a private message if the user is a bot."() {
		given:
		User invalidUser = Mock(User)
		invalidUser.isBot() >> true
		invalidUser.isYourself() >> false

		String message = "hello"

		when:
		service.sendMessageToUser(invalidUser, message)

		then:
		0 * invalidUser.sendMessage(*_)
	}

	void "should not send a private message if the user is the bot himself."() {
		given:
		User invalidUser = Mock(User)
		invalidUser.isBot() >> true
		invalidUser.isYourself() >> true

		String message = "hello"

		when:
		service.sendMessageToUser(invalidUser, message)

		then:
		0 * invalidUser.sendMessage(*_)
	}

	void "sendSuccessMessageToUser should send a success message to user."() {
		given:
		User validUser = Mock(User)
		validUser.isBot() >> false
		validUser.isYourself() >> false

		String message = "hello"
		EmbedBuilder embedBuilder = Mock()

		messageBuilder.buildSuccessMessage(message) >> embedBuilder

		when:
		service.sendSuccessMessageToUser(validUser, message)

		then:
		1 * validUser.sendMessage(embedBuilder)
	}
}
