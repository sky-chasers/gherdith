package com.megalomaniac.pepperpotts.discord.gtw.attendance

import com.google.api.services.sheets.v4.model.SpreadsheetProperties
import com.megalomaniac.pepperpotts.google.sheets.SheetReader
import com.megalomaniac.pepperpotts.discord.gtw.salary.InvalidGtwSheetException
import com.megalomaniac.pepperpotts.discord.gtw.salary.SheetNotFoundException
import spock.lang.Specification

class GtwAttendanceServiceSpec extends Specification {

	GtwAttendanceService service
	SheetReader sheetReader = Mock(SheetReader)
	String gtwAttendanceRange = "Member Attendance!A3:B"
	String gtwAttendanceIdentifier = "GTW Attendance"

	void setup() {
		service = new GtwAttendanceService(sheetReader, gtwAttendanceRange, gtwAttendanceIdentifier)
	}

	void "fetchAttendanceWithId should throw SheetNotFoundException if the service encountered an IOException while fetching."() {
		given:
		String googleSheetsId = "someID"
		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetsId, gtwAttendanceRange) >> {
			throw new IOException()
		}

		when:
		service.fetchAttendanceWithId(googleSheetsId)

		then:
		thrown(SheetNotFoundException)
	}

	void "fetchAttendanceWithId should return a list of GtwAttendanceRow if it succeeded in fetching the attendance sheet."() {
		given:
		String googleSheetsId = "someID"

		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetsId, gtwAttendanceRange) >> [
				["Butter", "50%"],
				["Eggs", "100%"]
		]

		when:
		List<GtwAttendanceRow> actualResult = service.fetchAttendanceWithId(googleSheetsId)

		then:
		"Butter" == actualResult.get(0).getName()
		50 == actualResult.get(0).getScoreInPercentage()

		"Eggs" == actualResult.get(1).getName()
		100 == actualResult.get(1).getScoreInPercentage()
	}

	void "filterParticipantsWithScoreHigherThan should return a list of participants with score that is higher than 75"() {
		given:
		int score = 75

		GtwAttendanceRow participant1= Mock(GtwAttendanceRow)
		participant1.getName() >> "Butter"
		participant1.getScoreInPercentage() >> 74

		GtwAttendanceRow participant2= Mock(GtwAttendanceRow)
		participant2.getName() >> "Eggs"
		participant2.getScoreInPercentage() >> 76

		GtwAttendanceRow participant3= Mock(GtwAttendanceRow)
		participant3.getName() >> "Gherzales"
		participant3.getScoreInPercentage() >> 75

		List<GtwAttendanceRow> participants = [
			participant1, participant2, participant3
		]

		when:
		List<GtwAttendanceRow> actualResult = service.filterParticipantsWithScoreHigherThan(participants, score)

		then:
		assert 1 == actualResult.size()
		assert participant2 == actualResult.get(0)
	}

	void "fetchGoogleSheetPropertiesWithId should return a GtwAttendanceSheet with the correct values."() {
		given:
		String googleSheetId = "someID"
		String expectedTitle = "GTW Attendance 10/27/2019 (WIN)"
		SpreadsheetProperties spreadsheetProperties = new SpreadsheetProperties()
		spreadsheetProperties.set("title", expectedTitle)

		sheetReader.fetchGoogleSheetPropertiesWithId(googleSheetId) >> spreadsheetProperties

		when:
		GtwAttendanceSheet actualResult = service.fetchGoogleSheetPropertiesWithId(googleSheetId)

		then:
		assert expectedTitle == actualResult.getTitle()
		assert googleSheetId == actualResult.getGoogleSheetId()
	}

	void "fetchGoogleSheetPropertiesWithId should throw a SheetNotFoundException if the sheetReader failed to get the spreadsheet."() {
		given:
		String googleSheetId = "someID"

		sheetReader.fetchGoogleSheetPropertiesWithId(googleSheetId) >> {
			throw new IOException()
		}

		when:
		service.fetchGoogleSheetPropertiesWithId(googleSheetId)

		then:
		thrown(SheetNotFoundException)
	}

	void "fetchGoogleSheetPropertiesWithId should throw a SheetNotFoundException if the title does not conform to the expected title (not gtw related)."() {
		given:
		String googleSheetId = "someID"
		String wrongIdentifier = "Some fake title"
		SpreadsheetProperties spreadsheetProperties = new SpreadsheetProperties()
		spreadsheetProperties.set("title", wrongIdentifier)

		sheetReader.fetchGoogleSheetPropertiesWithId(googleSheetId) >> spreadsheetProperties

		when:
		service.fetchGoogleSheetPropertiesWithId(googleSheetId)

		then:
		thrown(InvalidGtwSheetException)
	}
}
