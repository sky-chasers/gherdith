package com.megalomaniac.pepperpotts.discord.gtw.salary

import com.megalomaniac.pepperpotts.discord.command.AdminCommand
import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import com.megalomaniac.pepperpotts.discord.message.MessageDebugger
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageAuthor
import org.javacord.api.entity.user.User
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.Specification

class CalculateGtwSalaryListenerSpec extends Specification {

	CalculateGtwSalaryListener listener
	GtwSalaryCalculator responder = Mock(GtwSalaryCalculator)
	MessageSender messageSender = Mock(MessageSender)
	CommandValidator validator = Mock(CommandValidator)

	void setup() {
		listener = new CalculateGtwSalaryListener(responder, messageSender, validator)
	}

	void "onMessageCreate should do nothing if the command is invalid."() {
		given:
		User recipient = Mock(User)

		MessageAuthor author = Mock(MessageAuthor)
		author.getId() >> 123L
		author.asUser() >> Optional.of(recipient)

		Message message = Mock(Message)
		message.getId() >> 1L
		message.getContent() >> "!invalidCommand something else"
		message.isPrivateMessage() >> true
		message.getAuthor() >> author

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		validator.isValidCommand(event, AdminCommand.CALCULATE_GTW_SALARY) >> false

		when:
		listener.onMessageCreate(event)

		then:
		0 * responder.calculateSalary(*_)
	}

	void "onMessageCreate should calculate the gtw salary if the command is valid."() {
		given:
		User recipient = Mock(User)
		BigInteger totalFunds = new BigInteger("10000000000000000")
		String googleSheetId = "1DmZ2XRmTb50DRZSe7UqiX7ciWQ69qRY1aTZZ3ZyIkOk"

		MessageAuthor author = Mock(MessageAuthor)
		author.getId() >> 123L
		author.asUser() >> Optional.of(recipient)

		Message message = Mock(Message)
		message.getId() >> 1L
		message.getContent() >> "!calculateGtwSalary ${googleSheetId} ${totalFunds}"
		message.isPrivateMessage() >> true
		message.getAuthor() >> author

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		validator.isValidCommand(event, AdminCommand.CALCULATE_GTW_SALARY) >> true
		responder.hasValidTotalFunds(message.getContent()) >> true

		when:
		listener.onMessageCreate(event)

		then:
		1 * responder.calculateSalary(_ as CalculateGtwSalaryCommand) >> {
			CalculateGtwSalaryCommand actualCommand ->
				assert message == actualCommand.getMessage()
				assert totalFunds == actualCommand.getTotalFunds()
				assert googleSheetId == actualCommand.getGoogleSheetsId()
		}
	}

	void "onMessageCreate should tell the user if the total funds is an invalid number."() {
		given:
		User recipient = Mock(User)

		MessageAuthor author = Mock(MessageAuthor)
		author.getId() >> 123L
		author.asUser() >> Optional.of(recipient)

		Message message = Mock(Message)
		message.getId() >> 4L
		message.getContent() >> "!calculateGtwSalary googleSheetId 1234asdf"
		message.isPrivateMessage() >> true
		message.getAuthor() >> author

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		responder.hasValidTotalFunds(message.getContent()) >> false
		validator.isValidCommand(event, AdminCommand.CALCULATE_GTW_SALARY) >> true

		when:
		listener.onMessageCreate(event)

		then:
		0 * responder.calculateSalary(*_)
		1 * messageSender.sendErrorMessage(message, "The `totalFunds` must be a valid number greater than zero.")
	}

}
