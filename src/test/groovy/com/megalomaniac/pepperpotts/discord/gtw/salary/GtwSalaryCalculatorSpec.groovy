package com.megalomaniac.pepperpotts.discord.gtw.salary

import com.megalomaniac.pepperpotts.discord.gtw.attendance.GtwAttendanceRow
import com.megalomaniac.pepperpotts.discord.gtw.attendance.GtwAttendanceService
import com.megalomaniac.pepperpotts.discord.gtw.attendance.GtwAttendanceSheet
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import spock.lang.Specification

class GtwSalaryCalculatorSpec extends Specification {

	GtwSalaryCalculator service
	GtwAttendanceService attendance = Mock(GtwAttendanceService)
	GtwSalaryPartsCalculator partsCalculator = Mock(GtwSalaryPartsCalculator)
	CalculateGtwSalaryFormatter messageFormatter = Mock(CalculateGtwSalaryFormatter)
	GtwSalarySettingsService gtwSalarySettingsService = Mock(GtwSalarySettingsService)
	MessageSender messageSender = Mock(MessageSender)

	void setup() {
		service = new GtwSalaryCalculator(attendance, partsCalculator, messageFormatter, gtwSalarySettingsService, messageSender)
	}

	void "hasValidTotalFunds should return true if the command has valid total funds."() {
		given:
		String command = "!calculateGtwSalary someId 379667753"

		when:
		boolean actualResult = service.hasValidTotalFunds(command)

		then:
		assert actualResult
	}

	void "hasValidTotalFunds should return false if the command's total fund is not a valid number."() {
		when:
		boolean actualResult = service.hasValidTotalFunds(command)

		then:
		assert expectedResult == actualResult

		where:
		command										|	expectedResult
		"!calculateGtwSalary someId 379667753aaa"	|	false
		"!calculateGtwSalary someId abcdef"			|	false
		"!calculateGtwSalary someId 123,1245,145"	|	false
		"!calculateGtwSalary someId"				|	false
	}

	void "calculateSalary should send an error message to if fetching the sheet failed."() {
		given:
		Message message = Mock(Message)
		String googleSheetId = "GoogleSheetID"
		BigInteger totalGtwFunds = new BigInteger("123456789")

		attendance.fetchAttendanceWithId(googleSheetId) >> {
			throw new SheetNotFoundException()
		}

		CalculateGtwSalaryCommand command = Mock(CalculateGtwSalaryCommand)
		command.getMessage() >> message
		command.getGoogleSheetsId() >> googleSheetId
		command.getTotalFunds() >> totalGtwFunds

		when:
		service.calculateSalary(command)

		then:
		1 * messageSender.sendErrorMessage(message, "I'm sorry, I can't find the sheet with id: GoogleSheetID.")
	}

	void "calculateSalary should send an error message to if the sheet is invalid."() {
		given:
		Message message = Mock(Message)
		String googleSheetId = "GoogleSheetID"
		BigInteger totalGtwFunds = new BigInteger("123456789")

		CalculateGtwSalaryCommand command = Mock(CalculateGtwSalaryCommand)
		command.getMessage() >> message
		command.getGoogleSheetsId() >> googleSheetId
		command.getTotalFunds() >> totalGtwFunds

		attendance.fetchGoogleSheetPropertiesWithId(googleSheetId) >> {
			throw new InvalidGtwSheetException()
		}

		when:
		service.calculateSalary(command)

		then:
		1 * messageSender.sendErrorMessage(message, "The sheet you're trying to access is invalid. Make sure it's a valid GTW attendance sheet.")
	}


	void "calculateSalary should send the correct calculations to the user."() {
		given:
		Message message = Mock(Message)
		String googleSheetId = "GoogleSheetID"
		BigDecimal totalGtwFunds = new BigDecimal("123456789")
		BigDecimal remainingFunds = new BigDecimal("123")

		String entryMessage = "entry message"
		String remainingFundsMessage = "remaining funds"
		String nonPerfectMessage = "non perfect stuff"

		CalculateGtwSalaryCommand command = Mock(CalculateGtwSalaryCommand)
		command.getMessage() >> message
		command.getGoogleSheetsId() >> googleSheetId
		command.getTotalFunds() >> totalGtwFunds

		List<GtwAttendanceRow> participants = []

		String documentTitle = "GTW title"
		GtwAttendanceSheet attendanceSheet = Mock(GtwAttendanceSheet)
		attendanceSheet.getGoogleSheetId() >> googleSheetId
		attendanceSheet.getTitle() >> documentTitle

		attendance.filterParticipantsWithScoreHigherThan(*_) >> participants
		attendance.fetchGoogleSheetPropertiesWithId(googleSheetId) >> attendanceSheet

		List<GtwSalarySetting> settings = []
		gtwSalarySettingsService.fetchGtwSettings() >> settings

		List<GtwSalaryOutput> outputList = []
		partsCalculator.calculateBasedOnSettings(totalGtwFunds, settings, participants) >> outputList
		partsCalculator.calculateRemainingFunds(outputList) >> remainingFunds

		messageFormatter.formatGtwSalaryEntryMessage(documentTitle, totalGtwFunds, outputList) >> entryMessage
		messageFormatter.formatParticipantSalary(outputList, participants.size()) >> nonPerfectMessage
		messageFormatter.formatRemainingSilver(remainingFunds) >> remainingFundsMessage

		when:
		service.calculateSalary(command)

		then:
		1 * messageSender.sendMessage(message, entryMessage)
		1 * messageSender.sendMessage(message, nonPerfectMessage)
		1 * messageSender.sendMessage(message, remainingFundsMessage)
	}
}
