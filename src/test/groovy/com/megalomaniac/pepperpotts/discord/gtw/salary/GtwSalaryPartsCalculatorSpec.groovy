package com.megalomaniac.pepperpotts.discord.gtw.salary

import com.megalomaniac.pepperpotts.discord.gtw.attendance.GtwAttendanceRow
import spock.lang.Specification

class GtwSalaryPartsCalculatorSpec extends Specification {

	GtwSalaryPartsCalculator service
	int gtwAttendanceScoreThresholdPercentage = 75

	void setup() {
		service = new GtwSalaryPartsCalculator(gtwAttendanceScoreThresholdPercentage)
	}

	void "calculateRemainingFunds should return the correct value if there is salary per participant."() {
		given:
		GtwSalarySetting setting1 = Mock()
		setting1.getLabel() >> "Guild Facility Fund"
		setting1.getPercentage() >> 50

		GtwSalarySetting setting2 = Mock()
		setting2.getLabel() >> "Participant Funds"
		setting2.getPercentage() >> 50
		setting2.isDividedByParticipant() >> true

		GtwSalaryOutput guildFacilityFund = Mock()
		guildFacilityFund.getSalaryValue() >> new BigDecimal(1_000_000)
		guildFacilityFund.getSetting() >> setting1
		guildFacilityFund.getSalaryPerParticipant() >> null

		GtwAttendanceSalary nonPerfectParticipant = Mock()
		nonPerfectParticipant.getFinalSalary() >> new BigDecimal(300_000)

		List<GtwAttendanceSalary> nonPerfectSalary = [
				nonPerfectParticipant
		]

		GtwSalaryOutput participantFunds = Mock()
		participantFunds.getSalaryValue() >> new BigDecimal(1_000_000)
		participantFunds.getSetting() >> setting2
		participantFunds.getSalaryPerParticipant() >> new BigDecimal(500_000)
		participantFunds.getNonPerfectParticipantSalary() >> nonPerfectSalary

		List<GtwSalaryOutput> outputList = [
				guildFacilityFund,
				participantFunds
		]

		when:
		BigDecimal actualRemainingFunds = service.calculateRemainingFunds(outputList)

		then:
		new BigDecimal(200_000) == actualRemainingFunds
	}

	void "calculateRemainingFunds should return 0 if all participants got perfect attendance."() {
		given:
		GtwSalarySetting setting1 = Mock()
		setting1.getLabel() >> "Guild Facility Fund"
		setting1.getPercentage() >> 50
		setting1.isDividedByParticipant() >> false

		GtwSalarySetting setting2 = Mock()
		setting2.getLabel() >> "Participant Funds"
		setting2.getPercentage() >> 50
		setting2.isDividedByParticipant() >> true

		GtwSalaryOutput guildFacilityFund = Mock()
		guildFacilityFund.getSalaryValue() >> new BigDecimal(1_000_000)
		guildFacilityFund.getSetting() >> setting1
		guildFacilityFund.getSalaryPerParticipant() >> null

		GtwSalaryOutput participantFunds = Mock()
		participantFunds.getSalaryValue() >> new BigDecimal(1_000_000)
		participantFunds.getSetting() >> setting2
		participantFunds.getSalaryPerParticipant() >> new BigDecimal(500_000)
		participantFunds.getNonPerfectParticipantSalary() >> []

		List<GtwSalaryOutput> outputList = [
				guildFacilityFund,
				participantFunds
		]

		when:
		BigDecimal actualRemainingFunds = service.calculateRemainingFunds(outputList)

		then:
		BigDecimal.ZERO == actualRemainingFunds
	}

	void "calculateBasedOnSettings should return the correct value based on the calculations"(){
		given:
		BigDecimal totalGtwFunds = new BigDecimal(1_000_000)

		GtwSalarySetting guildFund = Mock()
		guildFund.getLabel() >> "Guild Facility Fund"
		guildFund.getPercentage() >> 50
		guildFund.isDividedByParticipant() >> false

		GtwSalarySetting participantFund = Mock()
		participantFund.getLabel() >> "Participant Funds"
		participantFund.getPercentage() >> 50
		participantFund.isDividedByParticipant() >> true

		List<GtwSalarySetting> settingsList = [
				participantFund,
				guildFund
		]

		GtwAttendanceRow row1 = Mock()
		row1.getName() >> "Eggs"
		row1.getScoreInPercentage() >> 100

		GtwAttendanceRow row2 = Mock()
		row2.getName() >> "Ghervis"
		row2.getScoreInPercentage() >> 70

		List<GtwAttendanceRow> gtwAttendanceRowList = [
				row1,
				row2
		]

		BigDecimal expectedSalaryPerParticipant = new BigDecimal(250_000)
		BigDecimal expectedGuildFund = new BigDecimal(500_000)
		BigDecimal expectedParticipantFund = new BigDecimal(500_000)

		BigDecimal expectedSalaryGhervis = new BigDecimal(175_000)

		when:
		List<GtwSalaryOutput> outputList = service.calculateBasedOnSettings(totalGtwFunds, settingsList, gtwAttendanceRowList)

		then:
		participantFund == outputList.get(0).getSetting()
		expectedSalaryPerParticipant == outputList.get(0).getSalaryPerParticipant()
		expectedParticipantFund == outputList.get(0).getSalaryValue()
		1 == outputList.get(0).getNonPerfectParticipantSalary().size()

		expectedSalaryGhervis == outputList.get(0).getNonPerfectParticipantSalary().get(0).getFinalSalary()
		"Ghervis" == outputList.get(0).getNonPerfectParticipantSalary().get(0).getGtwAttendanceRow().getName()

		guildFund == outputList.get(1).getSetting()
		expectedGuildFund == outputList.get(1).getSalaryValue()
		null == outputList.get(1).getSalaryPerParticipant()
		null == outputList.get(1).getNonPerfectParticipantSalary()
	}
}
