package com.megalomaniac.pepperpotts.discord.bossbalita

import com.megalomaniac.pepperpotts.db.character.Character
import com.megalomaniac.pepperpotts.db.character.CharacterRepository
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.user.User
import spock.lang.Specification

class BossBalitaKeyGeneratorSpec extends Specification {

	BossBalitaKeyGenerator service
	CharacterRepository characterRepository = Mock(CharacterRepository)
	BossBalitaKeyGeneratorReplyBuilder replyBuilder = Mock(BossBalitaKeyGeneratorReplyBuilder)
	MessageSender messageSender = Mock(MessageSender)
	BossBalitaKeyGeneratorHelper processor = Mock(BossBalitaKeyGeneratorHelper)

	void setup() {
		service = new BossBalitaKeyGenerator(
				characterRepository,
				replyBuilder,
				messageSender,
				processor
		)
	}

	void "generateKey should send an invalid discord id message to admin if the user has an invalid discord id."() {
		given:
		User author = Mock(User)
		Message message = Mock(Message)
		message.getUserAuthor() >> Optional.of(author)

		GenerateBossBalitaKeyCommand command = Mock(GenerateBossBalitaKeyCommand)
		command.getTeamName() >> "Ghervis"
		command.getDiscordUserId() >> 123456789L
		command.getMessage() >> message
		command.getCommand() >> "!generateKey"

		characterRepository.findByTeamName(command.getTeamName()) >> Optional.empty()
		processor.fetchDiscordUser(command.getDiscordUserId()) >> Optional.empty()

		when:
		service.generateKey(command)

		then:
		1 * processor.sendInvalidDiscordIdMessageToAdmin(command)
		0 * processor.processKeyGeneration(*_)
		0 * messageSender.sendInfoMessage(*_)
	}

	void "generateKey should generate a key if the user's discord id is valid and the admin user is valid."() {
		given:
		User admin = Mock(User)
		Message message = Mock(Message)
		message.getUserAuthor() >> Optional.of(admin)

		User user = Mock(User)

		GenerateBossBalitaKeyCommand command = Mock(GenerateBossBalitaKeyCommand)
		command.getTeamName() >> "Ghervis"
		command.getDiscordUserId() >> 123456789L
		command.getMessage() >> message
		command.getCommand() >> "!generateKey"

		Character character = Mock(Character)

		characterRepository.findByTeamName(command.getTeamName()) >> Optional.of(character)
		processor.fetchDiscordUser(command.getDiscordUserId()) >> Optional.of(user)

		when:
		service.generateKey(command)

		then:
		0 * processor.sendInvalidDiscordIdMessageToAdmin(_)
		1 * processor.processKeyGeneration(command, user, character)
		0 * messageSender.sendInfoMessage(*_)
	}

	void "generateKey should send an info message if the user's character cannot be found in the database."() {
		given:
		User admin = Mock(User)
		Message message = Mock(Message)
		message.getUserAuthor() >> Optional.of(admin)

		User user = Mock(User)

		GenerateBossBalitaKeyCommand command = Mock(GenerateBossBalitaKeyCommand)
		command.getTeamName() >> "Ghervis"
		command.getDiscordUserId() >> 123456789L
		command.getMessage() >> message
		command.getCommand() >> "!generateKey"

		String infoMessage = "some info message...."
		characterRepository.findByTeamName(command.getTeamName()) >> Optional.empty()
		processor.fetchDiscordUser(command.getDiscordUserId()) >> Optional.of(user)
		replyBuilder.buildErrorMessage(command.getTeamName()) >> infoMessage

		when:
		service.generateKey(command)

		then:
		0 * processor.sendInvalidDiscordIdMessageToAdmin(_)
		0 * processor.processKeyGeneration(*_)
		1 * messageSender.sendInfoMessage(message, infoMessage)
	}
}
