package com.megalomaniac.pepperpotts.discord.bossbalita

import com.megalomaniac.pepperpotts.discord.command.AdminCommand
import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import org.javacord.api.entity.message.Message
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.Specification

class GenerateBossBalitaKeyListenerSpec extends Specification {

	GenerateBossBalitaKeyListener listener
	CommandValidator commandValidator = Mock(CommandValidator)
	BossBalitaKeyGenerator bossBalitaKeyGenerator = Mock(BossBalitaKeyGenerator)

	AdminCommand adminCommand = AdminCommand.GENERATE_BOSS_BALITA_KEYS

	void setup() {
		listener = new GenerateBossBalitaKeyListener(commandValidator, bossBalitaKeyGenerator)
	}

	void "onMessageCreate should do nothing if the command is invalid."() {
		given:
		Message message = Mock(Message)

		MessageCreateEvent messageCreateEvent = Mock(MessageCreateEvent)
		messageCreateEvent.getMessage() >> message

		commandValidator.isValidCommand(messageCreateEvent, adminCommand) >> false

		when:
		listener.onMessageCreate(messageCreateEvent)

		then:
		0 * bossBalitaKeyGenerator.generateKey(_)
	}

	void "onMessageCreate should generate kets if the command is valid."() {
		given:
		Message message = Mock(Message)
		message.getContent() >> "!generateKey Ghervis 1234567890"

		MessageCreateEvent messageCreateEvent = Mock(MessageCreateEvent)
		messageCreateEvent.getMessage() >> message

		commandValidator.isValidCommand(messageCreateEvent, adminCommand) >> true

		when:
		listener.onMessageCreate(messageCreateEvent)

		then:
		1 * bossBalitaKeyGenerator.generateKey(_)
	}
}
