package com.megalomaniac.pepperpotts.discord.bossbalita

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter
import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporterRepository
import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporterService
import com.megalomaniac.pepperpotts.db.character.Character
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import com.megalomaniac.pepperpotts.discord.message.PrivateMessageSender
import org.javacord.api.DiscordApi
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.user.User
import spock.lang.Specification

import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionException

class BossBalitaKeyGeneratorHelperSpec extends Specification {

	BossBalitaKeyGeneratorHelper service
	BossBalitaReporterRepository bossBalitaReporterRepository = Mock(BossBalitaReporterRepository)
	BossBalitaKeyGeneratorReplyBuilder replyBuilder = Mock(BossBalitaKeyGeneratorReplyBuilder)
	MessageSender messageSender = Mock(MessageSender)
	BossBalitaReporterService bossBalitaReporterService = Mock(BossBalitaReporterService)
	PrivateMessageSender privateMessageSender = Mock(PrivateMessageSender)
	DiscordApi discordApi = Mock(DiscordApi)

	void setup() {
		service = new BossBalitaKeyGeneratorHelper(
				bossBalitaReporterRepository,
				replyBuilder,
				messageSender,
				bossBalitaReporterService,
				privateMessageSender,
				discordApi
		)
	}

	void "fetchDiscordUser should return an empty user if the discord id is invalid."() {
		given:
		Long discordUserId = 12345678L

		discordApi.getUserById(discordUserId) >> {
			throw new CompletionException()
		}

		when:
		Optional<User> actualUser = service.fetchDiscordUser(discordUserId)

		then:
		assert actualUser.isEmpty()
	}

	void "fetchDiscordUser should return a user if the discord id is valid."() {
		given:
		Long discordUserId = 12345678L
		User user = Mock(User)

		discordApi.getUserById(discordUserId) >> CompletableFuture.completedFuture(user)

		when:
		Optional<User> actualUser = service.fetchDiscordUser(discordUserId)

		then:
		assert user == actualUser.get()
	}

	void "sendInvalidDiscordIdMessageToAdmin should send an invalid discord user id error message to the admin."() {
		given:
		Message message = Mock(Message)
		Long discordUserId = 1234567L
		String errorMessage = "some error occurred."
		replyBuilder.buildInvalidDiscordIdMessage(discordUserId) >> errorMessage

		GenerateBossBalitaKeyCommand command = Mock(GenerateBossBalitaKeyCommand)
		command.getTeamName() >> "Ghervis"
		command.getMessage() >> message
		command.getDiscordUserId() >> discordUserId

		when:
		service.sendInvalidDiscordIdMessageToAdmin(command)

		then:
		1 * messageSender.sendErrorMessage(message, errorMessage)
	}

	void "processKeyGeneration should update the keys and send success messages if the reporter already exists."(){
		given:
		Message message = Mock(Message)

		GenerateBossBalitaKeyCommand command = Mock(GenerateBossBalitaKeyCommand)
		command.getTeamName() >> "Ghervis"
		command.getDiscordUserId() >> 12309455L
		command.getMessage() >> message

		User user = Mock(User)

		TosAccount tosAccount = Mock(TosAccount)
		tosAccount.getInGameAccountId() >> "123456788"

		Character character = Mock(Character)
		character.getTosAccount() >> tosAccount

		BossBalitaReporter reporter = Mock(BossBalitaReporter)

		String successMessage = "success message"
		bossBalitaReporterRepository.findByTeamName(command.getTeamName()) >> Optional.of(reporter)
		replyBuilder.buildSuccessMessage(reporter) >> successMessage

		when:
		service.processKeyGeneration(command, user, character)

		then: "update the keys"
		1 * bossBalitaReporterService.updateReporterKey(reporter, command.getDiscordUserId()) >> reporter
		0 * bossBalitaReporterService.createReporter(*_)

		and: "send success message to the user"
		1 * privateMessageSender.sendSuccessMessageToUser(user, successMessage)

		and: "send success message to the admin"
		1 * messageSender.sendSuccessMessage(message, successMessage)
	}

	void "processKeyGeneration should create new keys, reportar, and send success messages."(){
		given:
		Message message = Mock(Message)

		GenerateBossBalitaKeyCommand command = Mock(GenerateBossBalitaKeyCommand)
		command.getTeamName() >> "Ghervis"
		command.getDiscordUserId() >> 12309455L
		command.getMessage() >> message

		User user = Mock(User)

		TosAccount tosAccount = Mock(TosAccount)
		tosAccount.getInGameAccountId() >> "123456788"

		Character character = Mock(Character)
		character.getTosAccount() >> tosAccount

		BossBalitaReporter reporter = Mock(BossBalitaReporter)

		String successMessage = "success message"
		bossBalitaReporterRepository.findByTeamName(command.getTeamName()) >> Optional.empty()
		replyBuilder.buildSuccessMessage(reporter) >> successMessage

		when:
		service.processKeyGeneration(command, user, character)

		then: "update the keys"
		1 * bossBalitaReporterService.createReporter(command.getTeamName(), tosAccount.getInGameAccountId(), command.getDiscordUserId()) >> reporter
		0 * bossBalitaReporterService.updateReporterKey(*_)

		and: "send success message to the user"
		1 * privateMessageSender.sendSuccessMessageToUser(user, successMessage)

		and: "send success message to the admin"
		1 * messageSender.sendSuccessMessage(message, successMessage)
	}

}
