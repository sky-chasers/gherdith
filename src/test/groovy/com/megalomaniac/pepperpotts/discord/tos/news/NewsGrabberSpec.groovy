package com.megalomaniac.pepperpotts.discord.tos.news

import com.megalomaniac.pepperpotts.discord.message.MessageSender
import com.megalomaniac.pepperpotts.tos.news.TosNews
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi
import com.megalomaniac.pepperpotts.utils.GetRequestException
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.embed.EmbedBuilder
import spock.lang.Specification

class NewsGrabberSpec extends Specification {

	NewsGrabber service
	MessageSender messageSender = Mock(MessageSender)
	NewsReplyFormatter newsReplyFormatter = Mock(NewsReplyFormatter)
	TosNewsApi tosNewsApi = Mock(TosNewsApi)

	void setup() {
		service = new NewsGrabber(messageSender, newsReplyFormatter, tosNewsApi)
	}

	void "respondWithLatestNews should fetch the news and reply with it."() {
		given:
		Message messageFromUser = Mock(Message)
		EmbedBuilder expectedReply = Mock(EmbedBuilder)
		List< TosNews> tosNewsList = [
		        Mock(TosNews)
		]

		tosNewsApi.fetchAllItosNews() >> tosNewsList
		newsReplyFormatter.format(tosNewsList) >> expectedReply

		when:
		service.respondWithLatestNews(messageFromUser)

		then:
		1 * messageSender.sendMessage(messageFromUser, expectedReply)
	}

	void "respondWithLatestNews should reply with an error if it encountered an exception while fethcing the news."() {
		given:
		Message messageFromUser = Mock(Message)
		EmbedBuilder expectedReply = Mock(EmbedBuilder)
		List< TosNews> tosNewsList = [
				Mock(TosNews)
		]

		tosNewsApi.fetchAllItosNews() >> { throw new GetRequestException() }
		newsReplyFormatter.format(tosNewsList) >> expectedReply

		when:
		service.respondWithLatestNews(messageFromUser)

		then:
		1 * messageSender.sendErrorMessage(messageFromUser, "Sorry, I encountered an error while fetching the news. Please try again later.")
	}
}
