package com.megalomaniac.pepperpotts.discord.tos.news

import com.megalomaniac.pepperpotts.tos.news.TosNews
import spock.lang.Specification

class TosNewsSpec extends Specification {

	void "getTrimmedContent should return the content where the max length is the same as given."() {
		given:
		TosNews tosNews = new TosNews()
		tosNews.setContent("hello world")

		when:
		String trimmedContent = tosNews.getTrimmedContent(7)

		then:
		assert "hello w" == trimmedContent
	}
}
