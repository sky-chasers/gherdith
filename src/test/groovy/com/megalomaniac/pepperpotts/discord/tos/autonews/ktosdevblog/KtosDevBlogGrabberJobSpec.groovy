package com.megalomaniac.pepperpotts.discord.tos.autonews.ktosdevblog

import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNews
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNewsRepository
import com.megalomaniac.pepperpotts.db.archivednews.NewsSource
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsService
import com.megalomaniac.pepperpotts.tos.news.TosNews
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi
import com.megalomaniac.pepperpotts.utils.GetRequestException
import spock.lang.Specification

class KtosDevBlogGrabberJobSpec extends Specification {

	KtosDevBlogGrabberJob job

	TosNewsApi tosNewsApi = Mock()
	AutoNewsService autoNewsService = Mock()
	ArchivedNewsRepository archivedNewsRepository = Mock()

	void setup() {
		job = new KtosDevBlogGrabberJob(tosNewsApi, autoNewsService, archivedNewsRepository)
	}

	void "fetchAndPublishLatestDevBlogs should only publish and archive the news that hasn't been published yet."() {
		given:
		TosNews tosNews1 = Mock()
		tosNews1.getUrl() >> "tos1.com"
		TosNews tosNews2 = Mock()
		tosNews2.getUrl() >> "tos2.com"

		List<TosNews> frontPageNews = [ tosNews1, tosNews2 ]

		tosNewsApi.fetchAllDevBlogs() >> frontPageNews

		ArchivedNews archivedNews1 = Mock()

		archivedNewsRepository.findByUrlAndNewsSource(tosNews1.getUrl(), NewsSource.KTOS_DEVBLOG) >> Optional.of(archivedNews1)
		archivedNewsRepository.findByUrlAndNewsSource(tosNews2.getUrl(), NewsSource.KTOS_DEVBLOG) >> Optional.empty()

		when:
		job.fetchAndPublishLatestDevBlogs()

		then:
		1 * autoNewsService.publishLatestNews([tosNews2], NewsSource.KTOS_DEVBLOG, _)
		1 * autoNewsService.updateNewsArchive([tosNews2], NewsSource.KTOS_DEVBLOG)
	}

	void "fetchAndPublishLatestDevBlogs should delete stale news."() {
		given:
		TosNews tosNews1 = Mock()
		tosNews1.getUrl() >> "tos1.com"
		TosNews tosNews2 = Mock()
		tosNews2.getUrl() >> "tos2.com"

		List<TosNews> frontPageNews = [ tosNews1, tosNews2 ]

		tosNewsApi.fetchAllDevBlogs() >> frontPageNews

		ArchivedNews archivedNews1 = Mock()

		archivedNewsRepository.findByUrlAndNewsSource(tosNews1.getUrl(), NewsSource.KTOS_DEVBLOG) >> Optional.of(archivedNews1)
		archivedNewsRepository.findByUrlAndNewsSource(tosNews2.getUrl(), NewsSource.KTOS_DEVBLOG) >> Optional.empty()

		when:
		job.fetchAndPublishLatestDevBlogs()

		then:
		1 * autoNewsService.deleteStaleNews(frontPageNews, NewsSource.KTOS_DEVBLOG)
	}

	void "fetchAndPublishLatestNews should do nothing if a GetRequestException occurred."() {
		given:
		tosNewsApi.fetchAllDevBlogs() >> { throw new GetRequestException() }

		when:
		job.fetchAndPublishLatestDevBlogs()

		then:
		0 * autoNewsService.publishLatestNews(_)
		0 * autoNewsService.updateNewsArchive(_)
		0 * autoNewsService.deleteStaleNews(_)
	}
}
