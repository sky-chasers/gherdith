package com.megalomaniac.pepperpotts.discord.tos.autonews.ktos

import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNews
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNewsRepository
import com.megalomaniac.pepperpotts.db.archivednews.NewsSource
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsService
import com.megalomaniac.pepperpotts.tos.news.TosNews
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi
import com.megalomaniac.pepperpotts.utils.GetRequestException
import spock.lang.Specification

class KtosNewsGrabberJobSpec extends Specification {

	KtosNewsGrabberJob job
	TosNewsApi tosNewsApi = Mock(TosNewsApi)
	ArchivedNewsRepository archivedNewsRepository = Mock(ArchivedNewsRepository)
	AutoNewsService ktosAutoNewsService = Mock(AutoNewsService)

	void setup() {
		job = new KtosNewsGrabberJob(tosNewsApi, archivedNewsRepository, ktosAutoNewsService)
	}

	void "fetchAndPublishLatestNews should only publish and archive the news that hasn't been published yet."() {
		given:
		TosNews tosNews1 = Mock()
		tosNews1.getUrl() >> "tos1.com"
		TosNews tosNews2 = Mock()
		tosNews2.getUrl() >> "tos2.com"

		List<TosNews> frontPageNews = [ tosNews1, tosNews2 ]

		tosNewsApi.fetchAllKtosNews() >> frontPageNews

		ArchivedNews archivedNews1 = Mock()

		archivedNewsRepository.findByUrlAndNewsSource(tosNews1.getUrl(), NewsSource.KTOS) >> Optional.of(archivedNews1)
		archivedNewsRepository.findByUrlAndNewsSource(tosNews2.getUrl(), NewsSource.KTOS) >> Optional.empty()

		when:
		job.fetchAndPublishLatestNews()

		then:
		1 * ktosAutoNewsService.publishLatestNews([tosNews2], NewsSource.KTOS, _)
		1 * ktosAutoNewsService.updateNewsArchive([tosNews2], NewsSource.KTOS)
	}

	void "fetchAndPublishLatestNews should delete stale news."() {
		given:
		TosNews tosNews1 = Mock()
		tosNews1.getUrl() >> "tos1.com"
		TosNews tosNews2 = Mock()
		tosNews2.getUrl() >> "tos2.com"

		List<TosNews> frontPageNews = [ tosNews1, tosNews2 ]

		tosNewsApi.fetchAllKtosNews() >> frontPageNews

		ArchivedNews archivedNews1 = Mock()

		archivedNewsRepository.findByUrlAndNewsSource(tosNews1.getUrl(), NewsSource.KTOS) >> Optional.of(archivedNews1)
		archivedNewsRepository.findByUrlAndNewsSource(tosNews2.getUrl(), NewsSource.KTOS) >> Optional.empty()

		when:
		job.fetchAndPublishLatestNews()

		then:
		1 * ktosAutoNewsService.deleteStaleNews(frontPageNews, NewsSource.KTOS)
	}

	void "fetchAndPublishLatestNews should do nothing if a GetRequestException occurred."() {
		given:
		tosNewsApi.fetchAllKtosNews() >> { throw new GetRequestException() }

		when:
		job.fetchAndPublishLatestNews()

		then:
		0 * ktosAutoNewsService.publishLatestNews(_)
		0 * ktosAutoNewsService.updateNewsArchive(_)
		0 * ktosAutoNewsService.deleteStaleNews(_)
	}

}
