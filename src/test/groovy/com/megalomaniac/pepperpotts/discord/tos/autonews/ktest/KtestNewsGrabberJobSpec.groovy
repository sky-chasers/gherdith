package com.megalomaniac.pepperpotts.discord.tos.autonews.ktest

import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNews
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNewsRepository
import com.megalomaniac.pepperpotts.db.archivednews.NewsSource
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsService
import com.megalomaniac.pepperpotts.tos.news.TosNews
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi
import spock.lang.Specification

class KtestNewsGrabberJobSpec extends Specification {

	KtestNewsGrabberJob job

	TosNewsApi tosNewsApi = Mock(TosNewsApi)
	AutoNewsService ktestAutoNewsService = Mock(AutoNewsService)
	ArchivedNewsRepository archivedNewsRepository = Mock(ArchivedNewsRepository)

	void setup() {
		job = new KtestNewsGrabberJob(tosNewsApi, ktestAutoNewsService, archivedNewsRepository)
	}

	void "fetchAndPublishLatestNews should publish the latest news and update the latest archive if there are new articles available."() {
		given:
		TosNews tosNews1 = Mock()
		tosNews1.getUrl() >> "url1"

		TosNews tosNews2 = Mock()
		tosNews2.getUrl() >> "url2"

		List<TosNews> allKtestNews = [
				tosNews1,
				tosNews2
		]

		archivedNewsRepository.findByUrlAndNewsSource("url1", NewsSource.KTEST) >> Optional.empty()
		archivedNewsRepository.findByUrlAndNewsSource("url2", NewsSource.KTEST) >> Optional.empty()

		tosNewsApi.fetchAllKtestNews() >> allKtestNews

		List<TosNews> newsToPublish = [
				tosNews1,
				tosNews2
		]

		when:
		job.fetchAndPublishLatestNews()

		then:
		1 * ktestAutoNewsService.publishLatestNews(newsToPublish, NewsSource.KTEST, _)
		1 * ktestAutoNewsService.updateNewsArchive(allKtestNews, NewsSource.KTEST)

		and:
		1 * ktestAutoNewsService.deleteStaleNews(allKtestNews, NewsSource.KTEST)
	}

	void "fetchAndPublishLatestNews should not publish the latest news and update the latest archive if there are no new articles available."() {
		given:
		TosNews tosNews1 = Mock()
		tosNews1.getUrl() >> "url1"

		TosNews tosNews2 = Mock()
		tosNews2.getUrl() >> "url2"

		List<TosNews> allKtestNews = [
				tosNews1,
				tosNews2
		]

		ArchivedNews archivedNews1 = Mock()
		ArchivedNews archivedNews2 = Mock()

		archivedNewsRepository.findByUrlAndNewsSource("url1", NewsSource.KTEST) >> Optional.of(archivedNews1)
		archivedNewsRepository.findByUrlAndNewsSource("url2", NewsSource.KTEST) >> Optional.of(archivedNews2)

		tosNewsApi.fetchAllKtestNews() >> allKtestNews

		List<TosNews> newsToPublish = [
				tosNews1,
				tosNews2
		]

		when:
		job.fetchAndPublishLatestNews()

		then:
		0 * ktestAutoNewsService.publishLatestNews(newsToPublish, NewsSource.KTEST, _)
		0 * ktestAutoNewsService.updateNewsArchive(allKtestNews, NewsSource.KTEST)

		and:
		1 * ktestAutoNewsService.deleteStaleNews(allKtestNews, NewsSource.KTEST)
	}
}
