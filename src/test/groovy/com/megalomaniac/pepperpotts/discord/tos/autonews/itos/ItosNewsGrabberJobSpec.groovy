package com.megalomaniac.pepperpotts.discord.tos.autonews.itos

import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNews
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNewsRepository
import com.megalomaniac.pepperpotts.db.archivednews.NewsSource
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsService
import com.megalomaniac.pepperpotts.tos.news.TosNews
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi
import com.megalomaniac.pepperpotts.utils.GetRequestException
import spock.lang.Specification

class ItosNewsGrabberJobSpec extends Specification {

	ItosNewsGrabberJob job
	TosNewsApi tosNewsApi = Mock(TosNewsApi)
	ArchivedNewsRepository archivedNewsRepository = Mock(ArchivedNewsRepository)
	AutoNewsService autoNewsService = Mock(AutoNewsService)

	void setup() {
		job = new ItosNewsGrabberJob(tosNewsApi, archivedNewsRepository, autoNewsService)
	}

	void "fetchAndPublishLatestNews should publish the latest news if there are any."() {
		given:
		TosNews latestNews1 = Mock(TosNews)
		latestNews1.getUrl() >> "url1"

		TosNews latestNews2 = Mock(TosNews)
		latestNews2.getUrl() >> "url2"

		List<TosNews> latestNews = [
				latestNews1,
				latestNews2
		]

		archivedNewsRepository.findByUrlAndNewsSource(latestNews1.getUrl(), NewsSource.ITOS) >> Optional.empty()
		archivedNewsRepository.findByUrlAndNewsSource(latestNews2.getUrl(), NewsSource.ITOS) >> Optional.empty()

		tosNewsApi.fetchAllItosNews() >> latestNews

		when:
		job.fetchAndPublishLatestNews()

		then:
		1 * autoNewsService.publishLatestNews([latestNews1, latestNews2], NewsSource.ITOS, _)
	}

	void "fetchAndPublishLatestNews should not publish the latest news if there are none."() {
		given:
		TosNews latestNews1 = Mock(TosNews)
		latestNews1.getUrl() >> "url1"

		TosNews latestNews2 = Mock(TosNews)
		latestNews2.getUrl() >> "url2"

		List<TosNews> latestNews = [
				latestNews1,
				latestNews2
		]

		archivedNewsRepository.findByUrlAndNewsSource(latestNews1.getUrl(), NewsSource.ITOS) >> Optional.of(Mock(ArchivedNews))
		archivedNewsRepository.findByUrlAndNewsSource(latestNews2.getUrl(), NewsSource.ITOS) >> Optional.of(Mock(ArchivedNews))

		tosNewsApi.fetchAllItosNews() >> latestNews

		when:
		job.fetchAndPublishLatestNews()

		then:
		0 * autoNewsService.publishLatestNews(_)
	}

	void "fetchAndPublishLatestNews should only publish the latest news."() {
		given:
		TosNews latestNews1 = Mock(TosNews)
		latestNews1.getUrl() >> "url1"

		TosNews latestNews2 = Mock(TosNews)
		latestNews2.getUrl() >> "url2"

		List<TosNews> latestNews = [
				latestNews1,
				latestNews2
		]

		archivedNewsRepository.findByUrlAndNewsSource(latestNews1.getUrl(), NewsSource.ITOS) >> Optional.of(Mock(ArchivedNews))
		archivedNewsRepository.findByUrlAndNewsSource(latestNews2.getUrl(), NewsSource.ITOS) >> Optional.empty()

		tosNewsApi.fetchAllItosNews() >> latestNews

		when:
		job.fetchAndPublishLatestNews()

		then:
		1 * autoNewsService.publishLatestNews([latestNews2], NewsSource.ITOS, _)
	}

	void "fetchAndPublishLatestNews should update the news archive after publishing the latest news."() {
		given:
		TosNews latestNews1 = Mock(TosNews)
		latestNews1.getUrl() >> "url1"

		TosNews latestNews2 = Mock(TosNews)
		latestNews2.getUrl() >> "url2"

		List<TosNews> latestNews = [
				latestNews1,
				latestNews2
		]

		archivedNewsRepository.findByUrlAndNewsSource(latestNews1.getUrl(), NewsSource.ITOS) >> Optional.of(Mock(ArchivedNews))
		archivedNewsRepository.findByUrlAndNewsSource(latestNews2.getUrl(), NewsSource.ITOS) >> Optional.empty()

		tosNewsApi.fetchAllItosNews() >> latestNews

		when:
		job.fetchAndPublishLatestNews()

		then:
		1 * autoNewsService.updateNewsArchive(latestNews, NewsSource.ITOS)
	}

	void "fetchAndPublishLatestNews not publish anything if fetching the latest news gives out an error."() {
		given:
		tosNewsApi.fetchAllItosNews() >> { throw new GetRequestException() }

		when:
		job.fetchAndPublishLatestNews()

		then:
		0 * autoNewsService.updateNewsArchive(_)
	}

	void "fetchAndPublishLatestNews should delete stale news."() {
		given:
		TosNews latestNews1 = Mock(TosNews)
		latestNews1.getUrl() >> "url1"

		TosNews latestNews2 = Mock(TosNews)
		latestNews2.getUrl() >> "url2"

		List<TosNews> frontPageNews = [
				latestNews1,
				latestNews2
		]

		archivedNewsRepository.findByUrlAndNewsSource(latestNews1.getUrl(), NewsSource.ITOS) >> Optional.of(Mock(ArchivedNews))
		archivedNewsRepository.findByUrlAndNewsSource(latestNews2.getUrl(), NewsSource.ITOS) >> Optional.empty()

		tosNewsApi.fetchAllItosNews() >> frontPageNews

		when:
		job.fetchAndPublishLatestNews()

		then:
		1 * autoNewsService.deleteStaleNews(frontPageNews, NewsSource.ITOS)
	}
}
