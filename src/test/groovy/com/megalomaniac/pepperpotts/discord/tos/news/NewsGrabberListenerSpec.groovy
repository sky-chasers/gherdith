package com.megalomaniac.pepperpotts.discord.tos.news

import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import com.megalomaniac.pepperpotts.discord.command.GeneralCommand
import org.javacord.api.entity.message.Message
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.Specification

class NewsGrabberListenerSpec extends Specification {

	NewsGrabberListener listener
	CommandValidator commandValidator = Mock(CommandValidator)
	NewsGrabber newsGrabber = Mock(NewsGrabber)

	void setup() {
		listener = new NewsGrabberListener(commandValidator, newsGrabber)
	}

	void "onMessageCreate should respond with the latest news when the command is valid."() {
		given:
		Message message = Mock(Message)

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		commandValidator.isValidCommand(event, GeneralCommand.NEWS) >> true

		when:
		listener.onMessageCreate(event)

		then:
		1 * newsGrabber.respondWithLatestNews(message)
	}

	void "onMessageCreate should not respond with the latest news when the command is invalid."() {
		given:
		Message message = Mock(Message)

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		commandValidator.isValidCommand(event, GeneralCommand.NEWS) >> false

		when:
		listener.onMessageCreate(event)

		then:
		0 * newsGrabber.respondWithLatestNews(_)
	}
}
