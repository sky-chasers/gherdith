package com.megalomaniac.pepperpotts.discord.character.whoisaccountid

import com.megalomaniac.pepperpotts.discord.command.GeneralCommand
import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageAuthor
import org.javacord.api.entity.user.User
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.Specification

class WhoIsAccountIdListenerSpec extends Specification{

	WhoIsAccountIdListener listener

	WhoIsAccountIdService whoIsAccountIdService = Mock(WhoIsAccountIdService)
	CommandValidator validator = Mock(CommandValidator)

	void setup() {
		listener = new WhoIsAccountIdListener(
				whoIsAccountIdService,
				validator
		)
	}

	void "onMessageCreate should respond if the right conditions were met."() {
		given:
		String accountId = "123456789"
		User user = Mock(User)

		MessageAuthor messageAuthor = Mock(MessageAuthor)
		messageAuthor.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >> "!whoIsAccountId 123456789"
		message.getAuthor() >> messageAuthor

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		validator.isValidCommand(event, GeneralCommand.WHO_IS_ACCOUNT_ID) >> true

		when:
		listener.onMessageCreate(event)

		then:
		1 * whoIsAccountIdService.respondWithAccountInfoPrivately(_ as WhoIsAccountIdCommand) >> { WhoIsAccountIdCommand actualCommand ->
			assert accountId == actualCommand.getAccountId()
			assert message == actualCommand.getMessage()
		}
	}

	void "onMessageCreate should not respond if the command is not valid."() {
		given:
		User user = Mock(User)

		MessageAuthor messageAuthor = Mock(MessageAuthor)
		messageAuthor.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >> "!whoIsAccountId 123456789"
		message.getAuthor() >> messageAuthor

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		validator.isValidCommand(event, GeneralCommand.WHO_IS_ACCOUNT_ID) >> false

		when:
		listener.onMessageCreate(event)

		then:
		0 * whoIsAccountIdService.respondWithAccountInfoPrivately(*_)
	}
}
