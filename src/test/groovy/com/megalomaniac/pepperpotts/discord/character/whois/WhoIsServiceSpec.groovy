package com.megalomaniac.pepperpotts.discord.character.whois

import com.megalomaniac.pepperpotts.db.character.Character
import com.megalomaniac.pepperpotts.db.character.CharacterRepository
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import spock.lang.Specification

class WhoIsServiceSpec extends Specification {

	WhoIsService service
	MessageSender messageSender = Mock(MessageSender)
	CharacterRepository characterRepository = Mock(CharacterRepository)
	WhoIsResponseBuilder responseBuilder = Mock(WhoIsResponseBuilder)

	void setup() {
		service = new WhoIsService(messageSender, characterRepository, responseBuilder)
	}

	void "respond should send the correct message if we failed to find the character by team name."() {
		given:
		String teamNameToSearch = "Eggs"
		Message message = Mock(Message)
		WhoIsCommand command = Mock(WhoIsCommand)
		command.getTeamName() >> teamNameToSearch

		characterRepository.findByTeamName(teamNameToSearch) >> Optional.empty()

		when:
		service.respond(message, command)

		then:
		1 * responseBuilder.buildFailedResponse(teamNameToSearch) >> "failed response"
		1 * messageSender.sendInfoMessage(message, "failed response")
	}

	void "respond should send the correct message if we succeeded to find the character by team name."() {
		given:
		String teamNameToSearch = "Eggs"
		Message message = Mock(Message)
		TosAccount tosAccount = Mock(TosAccount)
		Character character = Mock(Character)
		character.getTosAccount() >> tosAccount
		WhoIsCommand command = Mock(WhoIsCommand)
		command.getTeamName() >> teamNameToSearch

		characterRepository.findByTeamName(teamNameToSearch) >> Optional.of(character)

		when:
		service.respond(message, command)

		then:
		1 * responseBuilder.buildSuccessResponse(tosAccount, teamNameToSearch) >> "success response"
		1 * messageSender.sendSuccessMessage(message, "success response")
	}
}
