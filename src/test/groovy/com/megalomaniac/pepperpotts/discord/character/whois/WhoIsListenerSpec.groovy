package com.megalomaniac.pepperpotts.discord.character.whois

import com.megalomaniac.pepperpotts.discord.command.CommandValidator
import com.megalomaniac.pepperpotts.discord.command.GeneralCommand
import org.javacord.api.entity.message.Message
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.Specification

class WhoIsListenerSpec extends Specification{

	WhoIsListener listener
	CommandValidator validator = Mock(CommandValidator)
	WhoIsService whoIsService =  Mock(WhoIsService)

	void setup() {
		listener = new WhoIsListener(validator, whoIsService)
	}

	void "listener should not respond if the the command is invalid."() {
		given:
		String rawCommand = "!whoIs Butter"
		Message message = Mock(Message)
		message.getContent() >> rawCommand

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		validator.isValidCommand(event, GeneralCommand.WHO_IS_TEAM_NAME) >> false

		when:
		listener.onMessageCreate(event)

		then:
		0 * whoIsService.respond(*_)
	}

	void "listener should respond if the the command is valid."() {
		given:
		String rawCommand = "!whoIs Butter"
		Message message = Mock(Message)
		message.getContent() >> rawCommand

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		validator.isValidCommand(event, GeneralCommand.WHO_IS_TEAM_NAME) >> true

		when:
		listener.onMessageCreate(event)

		then:
		1 * whoIsService.respond(message, _)
	}
}
