package com.megalomaniac.pepperpotts.discord.character.whoisaccountid

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccountRepository
import com.megalomaniac.pepperpotts.discord.message.MessageSender
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.user.User
import spock.lang.Specification

class WhoIsAccountIdServiceSpec extends Specification {

	WhoIsAccountIdService service

	TosAccountRepository tosAccountRepository = Mock(TosAccountRepository)
	WhoIsAccountIdResponseBuilder whoIsAccountIdResponseBuilder = Mock(WhoIsAccountIdResponseBuilder)
	MessageSender messageSender = Mock(MessageSender)

	void setup() {
		service = new WhoIsAccountIdService(
				tosAccountRepository,
				whoIsAccountIdResponseBuilder,
				messageSender
		)
	}

	void "respondWithAccountInfoPrivately should respond with 0 response if the account does not exist."() {
		given:
		Message message = Mock(Message)
		String accountId = "1234567890"

		WhoIsAccountIdCommand command = Mock(WhoIsAccountIdCommand)
		command.getAccountId() >>  accountId
		command.getMessage() >> message

		tosAccountRepository.findByInGameAccountId(accountId) >> Optional.empty()

		when:
		service.respondWithAccountInfoPrivately(command)

		then:
		1 * whoIsAccountIdResponseBuilder.buildFoundCharacterResponse(0, accountId)
		0 * whoIsAccountIdResponseBuilder.buildFoundCharacterResponse(_)
	}

	void "respondWithAccountInfoPrivately should respond with appropriate details if the account exist."() {
		given:
		Message message = Mock(Message)
		String accountId = "1234567890"

		WhoIsAccountIdCommand command = Mock(WhoIsAccountIdCommand)
		command.getAccountId() >>  accountId
		command.getMessage() >> message

		TosAccount tosAccount = Mock(TosAccount)
		tosAccountRepository.findByInGameAccountId(accountId) >> Optional.of(tosAccount)

		when:
		service.respondWithAccountInfoPrivately(command)

		then:
		0 * whoIsAccountIdResponseBuilder.buildFoundCharacterResponse(0, accountId)
		1 * whoIsAccountIdResponseBuilder.buildFoundCharacterResponse(tosAccount)
	}

	void "respondWithAccountInfoPrivately should send the response to the user after building the appropriate response for nonexistent account."() {
		given:
		Message message = Mock(Message)
		String accountId = "1234567890"
		String infoMessage = "Hello"

		WhoIsAccountIdCommand command = Mock(WhoIsAccountIdCommand)
		command.getAccountId() >>  accountId
		command.getMessage() >> message

		tosAccountRepository.findByInGameAccountId(accountId) >> Optional.empty()
		whoIsAccountIdResponseBuilder.buildFoundCharacterResponse(0, accountId) >> infoMessage

		when:
		service.respondWithAccountInfoPrivately(command)

		then:
		1 * messageSender.sendInfoMessage(message, infoMessage)
	}

	void "respondWithAccountInfoPrivately should send the response to the user after building the appropriate response for an existing account."() {
		given:
		String accountId = "1234567890"
		String successMessage = "Helloworld"
		Message message = Mock(Message)

		WhoIsAccountIdCommand command = Mock(WhoIsAccountIdCommand)
		command.getAccountId() >>  accountId
		command.getMessage() >> message

		TosAccount tosAccount = Mock(TosAccount)
		tosAccountRepository.findByInGameAccountId(accountId) >> Optional.of(tosAccount)
		whoIsAccountIdResponseBuilder.buildFoundCharacterResponse(tosAccount) >> successMessage

		when:
		service.respondWithAccountInfoPrivately(command)

		then:
		1 * messageSender.sendSuccessMessage(message, successMessage)
	}
}
