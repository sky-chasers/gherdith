package com.megalomaniac.pepperpotts.discord.user


import org.javacord.api.entity.user.User
import spock.lang.Specification

class UserSpec extends Specification {

	UserService service
	UserConfig userConfig = Mock(UserConfig)

	List<String> adminList = [
			"1234567890",
			"0987654321"
	]

	void setup() {
		userConfig.getMegalomaniacAdminIds() >> adminList
		service = new UserService(userConfig)
	}

	void "isAdmin should return true if the user has the admin role."() {
		given:
		User user = Mock()
		user.getIdAsString() >> "1234567890"

		when:
		boolean actualResult = service.isAdmin(user)

		then:
		assert actualResult
	}

	void "isAdmin should return false if the user has no admin role."() {
		given:
		User user = Mock()
		user.getIdAsString() >> "7777777777777777"

		when:
		boolean actualResult = service.isAdmin(user)

		then:
		assert !actualResult
	}

}
