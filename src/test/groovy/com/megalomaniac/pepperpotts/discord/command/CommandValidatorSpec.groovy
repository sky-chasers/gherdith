package com.megalomaniac.pepperpotts.discord.command

import com.megalomaniac.pepperpotts.db.generalcommand.CommandChannelPermissionRepository
import com.megalomaniac.pepperpotts.discord.message.MessageDebugger
import com.megalomaniac.pepperpotts.discord.user.UserService
import org.javacord.api.entity.channel.TextChannel
import org.javacord.api.entity.message.Message
import org.javacord.api.entity.message.MessageAuthor
import org.javacord.api.entity.user.User
import org.javacord.api.event.message.MessageCreateEvent
import spock.lang.IgnoreRest
import spock.lang.Specification
import spock.lang.Unroll

class CommandValidatorSpec extends Specification {

	CommandValidator service

	MessageDebugger messageDebugger = Mock(MessageDebugger)
	CommandHelper commandHelper = Mock(CommandHelper)
	UserService userService = Mock(UserService)
	CommandChannelPermissionRepository channelPermissionRepository = Mock(CommandChannelPermissionRepository)

	void setup() {
		service = new CommandValidator(messageDebugger, commandHelper, userService, channelPermissionRepository)
	}

	@Unroll
	void "isValidCommand should return #expectedResult if the command  is private: #isPrivateMessage and the command isAllowedInPrivateChat: #isAllowedInPrivateChat and command isAllowedInPublicChat: #isAllowedInPublicChat"() {
		given:
		String rawCommand = "my generalcommand"
		User user = Mock(User)
		long allowedChannel = 10L

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >>  rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> isPrivateMessage

		TextChannel currentChannel = Mock()
		currentChannel.getId() >> allowedChannel

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message
		event.getChannel() >> currentChannel

		Command command = Mock(Command)
		command.getCommandString() >> "my"
		command.isAllowedInPrivateChat() >> isAllowedInPrivateChat
		command.isAllowedInPublicChat() >> isAllowedInPublicChat
		command.startsWithValidCommand(rawCommand) >> true
		command.hasCorrectArgumentCount(rawCommand) >> true
		command.getEnumString() >> "MY_COMMAND"

		channelPermissionRepository.findAllByCommand(command.getEnumString()) >> [ allowedChannel ]

		when:
		boolean actualResult = service.isValidCommand(event, command)

		then:
		assert expectedResult == actualResult

		where:
		expectedResult	|	isPrivateMessage	|	isAllowedInPrivateChat	|	isAllowedInPublicChat
		true			|	true				|	true					|	true
		true			|	true				|	true					|	false
		true			|	false				|	true					|	true
		false			|	false				|	true					|	false
		false			|	true				|	false					|	false
		false			|	true				|	false					|	true
	}

	void "isValidCommand should return false if the message is not allowed in the current channel."() {
		given:
		String rawCommand = "my generalcommand"
		User user = Mock(User)
		long allowedChannel = 1L
		long currentChannelId = 2L

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >>  rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> false

		TextChannel currentChannel = Mock()
		currentChannel.getId() >> currentChannelId

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message
		event.getChannel() >> currentChannel

		Command command = Mock(Command)
		command.getCommandString() >> "my"
		command.isAllowedInPrivateChat() >> false
		command.isAllowedInPublicChat() >> true
		command.startsWithValidCommand(rawCommand) >> true
		command.hasCorrectArgumentCount(rawCommand) >> true
		command.getEnumString() >> "MY_COMMAND"

		channelPermissionRepository.findAllByCommand(command.getEnumString()) >> [ allowedChannel ]

		when:
		boolean actualResult = service.isValidCommand(event, command)

		then:
		!actualResult
	}

	void "isValidCommand should return true if the message is allowed in the current channel."() {
		given:
		String rawCommand = "my generalcommand"
		User user = Mock(User)
		long allowedChannel = 1L
		long currentChannelId = 1L

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >>  rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> false

		TextChannel currentChannel = Mock()
		currentChannel.getId() >> currentChannelId

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message
		event.getChannel() >> currentChannel

		Command command = Mock(Command)
		command.getCommandString() >> "my"
		command.isAllowedInPrivateChat() >> false
		command.isAllowedInPublicChat() >> true
		command.startsWithValidCommand(rawCommand) >> true
		command.hasCorrectArgumentCount(rawCommand) >> true
		command.getEnumString() >> "MY_COMMAND"

		channelPermissionRepository.findAllByCommand(command.getEnumString()) >> [ allowedChannel ]

		when:
		boolean actualResult = service.isValidCommand(event, command)

		then:
		actualResult
	}


	void "isValidCommand should return false if the message does not start with a valid command."() {
		given:
		String rawCommand = "my generalcommand"
		User user = Mock(User)

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >> rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> true

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		Command command = Mock(Command)
		command.isAllowedInPrivateChat() >> true
		command.startsWithValidCommand(rawCommand) >> false

		when:
		boolean actualResult = service.isValidCommand(event, command)

		then:
		assert !actualResult
	}

	void "isValidCommand should return false if the use is empty."() {
		given:
		String rawCommand = "my generalcommand"

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.empty()

		Message message = Mock(Message)
		message.getContent() >> rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> true

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		Command command = Mock(Command)
		command.isAllowedInPrivateChat() >> true
		command.startsWithValidCommand(rawCommand) >> true

		when:
		boolean actualResult = service.isValidCommand(event, command)

		then:
		assert !actualResult
	}

	void "isValidCommand should return false if the message has the correct argument count."() {
		given:
		String rawCommand = "my generalcommand"
		User user = Mock(User)

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >> rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> true

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		Command command = Mock(Command)
		command.isAllowedInPrivateChat() >> true
		command.startsWithValidCommand(rawCommand) >> true
		command.hasCorrectArgumentCount(rawCommand) >> false

		when:
		boolean actualResult = service.isValidCommand(event, command)

		then:
		assert !actualResult
	}

	void "isValidCommand should return false if the user with no admin role tried to execute an admin command."() {
		given:
		String rawCommand = "!adminHelp"
		User user = Mock(User)

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >> rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> true

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		AdminCommand command = AdminCommand.ADMIN_HELP

		userService.isAdmin(user) >> false

		when:
		boolean actualResult = service.isValidCommand(event, command)

		then:
		assert !actualResult
	}

	void "isValidCommand should return true if the user with admin role tried to execute an admin command."() {
		given:
		String rawCommand = "!adminHelp"
		User user = Mock(User)

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >> rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> true

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message

		AdminCommand command = AdminCommand.ADMIN_HELP

		userService.isAdmin(user) >> true

		when:
		boolean actualResult = service.isValidCommand(event, command)

		then:
		assert actualResult
	}

	void "isValidCommand should return true if the user with admin role tried to execute a non admin command."() {
		given:
		String rawCommand = "!my command"
		User user = Mock(User)

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >> rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> true

		Long currentChannelId = 1234L
		TextChannel currentChannel = Mock()
		currentChannel.getId() >> currentChannelId

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message
		event.getChannel() >> currentChannel

		Command nonAdminCommand = Mock(Command)
		nonAdminCommand.isAllowedInPrivateChat() >> true
		nonAdminCommand.startsWithValidCommand(rawCommand) >> true
		nonAdminCommand.hasCorrectArgumentCount(rawCommand) >> true

		userService.isAdmin(user) >> true
		channelPermissionRepository.findAllByCommand(nonAdminCommand.getEnumString()) >> [currentChannelId]

		when:
		boolean actualResult = service.isValidCommand(event, nonAdminCommand)

		then:
		assert actualResult
	}

	void "isValidCommand should return true if the user with non admin role tried to execute a non admin command."() {
		given:
		String rawCommand = "!my command"
		User user = Mock(User)

		MessageAuthor author = Mock(MessageAuthor)
		author.asUser() >> Optional.of(user)

		Message message = Mock(Message)
		message.getContent() >> rawCommand
		message.getAuthor() >> author
		message.isPrivateMessage()  >> true

		Long currentChannelId = 1234L
		TextChannel currentChannel = Mock()
		currentChannel.getId() >> currentChannelId

		MessageCreateEvent event = Mock(MessageCreateEvent)
		event.getMessage() >> message
		event.getChannel() >> currentChannel

		Command nonAdminCommand = Mock(Command)
		nonAdminCommand.isAllowedInPrivateChat() >> true
		nonAdminCommand.startsWithValidCommand(rawCommand) >> true
		nonAdminCommand.hasCorrectArgumentCount(rawCommand) >> true

		userService.isAdmin(user) >> false
		channelPermissionRepository.findAllByCommand(nonAdminCommand.getEnumString()) >> [currentChannelId]

		when:
		boolean actualResult = service.isValidCommand(event, nonAdminCommand)

		then:
		assert actualResult
	}
}
