package com.megalomaniac.pepperpotts.discord.command

import spock.lang.Specification

class AdminCommandSpec extends Specification {

	void "CALCULATE_GTW_SALARY.startsWithValidCommand should return true if the command starts with valid command."() {
		when:
		boolean result = AdminCommand.CALCULATE_GTW_SALARY.startsWithValidCommand(command)

		then:
		assert result

		where:
		command << [
				"!calculateGtwSalary googleSheetsId 1000000"
		]
	}

	void "CALCULATE_GTW_SALARY.startsWithValidCommand should return false if the command does not start with a valid command."() {
		when:
		boolean result = AdminCommand.CALCULATE_GTW_SALARY.startsWithValidCommand(command)

		then:
		assert !result

		where:
		command << [
				"!calculateGtwSalarys googleSheetsId 1000000",
				"!invalidCommand googleSheetsId 1000000"
		]
	}

	void "CALCULATE_GTW_SALARY.hasCorrectArgumentCount should return true if the command has 2 arguments."() {
		when:
		boolean result = AdminCommand.CALCULATE_GTW_SALARY.hasCorrectArgumentCount(command)

		then:
		assert result

		where:
		command << [
				"!calculateGtwSalary googleSheetsId2 1000000",
				"!calculateGtwSalary googleSheetsId3 1245687"
		]
	}

	void "CALCULATE_GTW_SALARY.hasCorrectArgumentCount should return true if the command does not have 2 arguments."() {
		when:
		boolean result = AdminCommand.CALCULATE_GTW_SALARY.hasCorrectArgumentCount(command)

		then:
		assert !result

		where:
		command << [
				"!calculateGtwSalary googleSheetsId 1000000 12333123",
				"!calculateGtwSalary googleSheetsId"
		]
	}
}
