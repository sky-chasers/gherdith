package com.megalomaniac.pepperpotts.discord.command

import spock.lang.Specification

class GeneralCommandSpec extends Specification {

	void "WHO_IS_ACCOUNT_ID.startsWithValidCommand should return false if the command does not begin with the correct command."() {
		when:
		boolean result = GeneralCommand.WHO_IS_ACCOUNT_ID.startsWithValidCommand(command)

		then:
		assert !result

		where:
		command << [
				"!whoisaccountid 123123",
				"!who? 123123 123123",
				"!hello fsdfsdfsdf",
				"!whoisaccountid 123124"
		]
	}

	void "WHO_IS_ACCOUNT_ID.startsWithValidCommand should return true if the command begins with the correct command."() {
		when:
		boolean result = GeneralCommand.WHO_IS_ACCOUNT_ID.startsWithValidCommand(command)

		then:
		assert result

		where:
		command << [
				"!whoIsAccountId 12314"
		]
	}

	void "WHO_IS_ACCOUNT_ID.hasCorrectArgumentCount should return true if the command has the correct argument count."() {
		when:
		boolean result = GeneralCommand.WHO_IS_ACCOUNT_ID.hasCorrectArgumentCount(command)

		then:
		assert result

		where:
		command << [
				"!whoIsAccountId 12314"
		]
	}

	void "WHO_IS_ACCOUNT_ID.hasCorrectArgumentCount should return false if the command does not have correct argument count command."() {
		when:
		boolean result = GeneralCommand.WHO_IS_ACCOUNT_ID.hasCorrectArgumentCount(command)

		then:
		assert !result

		where:
		command << [
				"!whoIsAccountId 12314 123123",
				"!whoIsAccountId 12314 123123 123123123",
				"!whoIsAccountId"
		]
	}

}
