package com.megalomaniac.pepperpotts.discord.biddingpoints.reader

import com.megalomaniac.pepperpotts.google.sheets.SheetReader
import spock.lang.Specification

class BiddingPointsTabReaderSpec extends Specification {

	BiddingPointsTabReader service
	SheetReader sheetReader = Mock(SheetReader)

	String biddingPointsTabName = "Bidding Points"
	String biddingPointsTeamName = "Name\\Time"
	String biddingPointsTotalBiddingPoints = "Total Bidding Points"
	String biddingPointsBorutaPoints = "Boruta Points"
	String biddingPointsGtwPoints = "GTW Points"
	String biddingPointsBonusBorutaPoints = "Bonus Points"

	List<String> CORRECT_LABELS = [
			biddingPointsTeamName,
			biddingPointsTotalBiddingPoints,
			biddingPointsGtwPoints,
			biddingPointsBorutaPoints,
			biddingPointsBonusBorutaPoints
	]

	void setup() {
		service = new BiddingPointsTabReader(
				sheetReader,
				biddingPointsTabName,
				biddingPointsTeamName,
				biddingPointsTotalBiddingPoints,
				biddingPointsBorutaPoints,
				biddingPointsGtwPoints,
				biddingPointsBonusBorutaPoints
		)
	}

	void "fetchBiddingPointDetails should return the correct details when the google sheet is fetched successfully."() {
		given:
		String googleSheetId = "12359586ks9rj"
		String range = "Bidding Points!A2:M1000"


		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId,range) >> [
				CORRECT_LABELS,
				["Eggs", 35, 7, 3, 10],
				["Ghervis", 13, 5, 2, 6]
		]

		when:
		List<BiddingPointDetail> actualResult = service.fetchBiddingPointDetails(googleSheetId)

		then:
		assert "Eggs" == actualResult.get(0).getTeamName()
		assert new BigDecimal(3) == actualResult.get(0).getBorutaPoints()
		assert new BigDecimal(7) == actualResult.get(0).getGtwPoints()
		assert new BigDecimal(3) == actualResult.get(0).getRowNumber()
		assert new BigDecimal(35) == actualResult.get(0).getTotalBiddingPoints()
		assert "E" == actualResult.get(0).getBorutaBonusPointsColumnLetter()

		assert "Ghervis" == actualResult.get(1).getTeamName()
		assert new BigDecimal(2) == actualResult.get(1).getBorutaPoints()
		assert new BigDecimal(5) == actualResult.get(1).getGtwPoints()
		assert new BigDecimal(4) == actualResult.get(1).getRowNumber()
		assert new BigDecimal(13) == actualResult.get(1).getTotalBiddingPoints()
		assert "E" == actualResult.get(1).getBorutaBonusPointsColumnLetter()
	}

	void "fetchBiddingPointDetails should transform 'N/A' to 0 when the value of the numbers cannot be parsed."() {
		given:
		String googleSheetId = "12359586ks9rj"
		String range = "Bidding Points!A2:M1000"


		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId,range) >> [
				CORRECT_LABELS,
				["Eggs", 35, 7, "N/A", 10],
				["Ghervis", 13, "Something", 2, 6]
		]

		when:
		List<BiddingPointDetail> actualResult = service.fetchBiddingPointDetails(googleSheetId)

		then:
		assert new BigDecimal(0) == actualResult.get(0).getBorutaPoints()
		assert new BigDecimal(0) == actualResult.get(1).getGtwPoints()
	}

	void "fetchBiddingPointDetails should throw FailedToReadBiddingPointsException if the sheet reader could not fetch the google sheets."() {
		given:
		String googleSheetId = "12359586ks9rj"
		String range = "Bidding Points!A2:M1000"


		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId,range) >> {throw new IOException() }

		when:
		service.fetchBiddingPointDetails(googleSheetId)

		then:
		thrown(FailedToReadBiddingPointsException)
	}

	void "fetchBiddingPointDetails should throw LabelNotFoundException the sheet was successfully fetched, but one of the labels could not be read."() {
		given:
		String googleSheetId = "12359586ks9rj"
		String range = "Bidding Points!A2:M1000"

		List<String> incorrectLabels = [
				"teamName",
				biddingPointsTotalBiddingPoints,
				biddingPointsGtwPoints,
				biddingPointsBorutaPoints,
				biddingPointsBonusBorutaPoints
		]

		sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId,range) >> [
				incorrectLabels,
				["Eggs", 35, 7, 3, 10],
				["Ghervis", 13, 5, 2, 6]
		]

		when:
		service.fetchBiddingPointDetails(googleSheetId)

		then:
		thrown(LabelNotFoundException)
	}
}
