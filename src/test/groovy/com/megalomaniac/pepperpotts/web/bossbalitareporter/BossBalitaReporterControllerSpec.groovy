package com.megalomaniac.pepperpotts.web.bossbalitareporter

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter
import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporterRepository
import com.megalomaniac.pepperpotts.ghervis.api.GhervisApi
import org.javacord.api.DiscordApi
import org.javacord.api.entity.user.User
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification

import java.util.concurrent.CompletableFuture
import java.util.concurrent.CompletionException

class BossBalitaReporterControllerSpec extends Specification {

	BossBalitaReporterController controller

	BossBalitaReporterRepository reporterRepository = Mock(BossBalitaReporterRepository)
	DiscordApi discordApi = Mock(DiscordApi)
	GhervisApi ghervisApi = Mock(GhervisApi)

	void setup() {
		controller = new BossBalitaReporterController(
				reporterRepository,
				discordApi,
				ghervisApi
		)
	}

	void "setupReporter should return ACCEPTED if the user exists in our database and their discord id exists."() {
		given:
		String accountId = "12346"
		String key = "my key"
		Long discordId = 12346677

		BossBalitaReporter reporter = Mock(BossBalitaReporter)
		reporter.getDiscordUserId() >> discordId
		User user = Mock(User)

		reporterRepository.findByInGameAccountIdAndKey(accountId, key) >> Optional.of(reporter)
		discordApi.getUserById(discordId) >> CompletableFuture.completedFuture(user)

		when:
		ResponseEntity response = controller.setupReporter(accountId, key)

		then:
		1 * ghervisApi.sendSuccessfulBossBalitaReporterSetupMessage(user)
		0 * ghervisApi.sendFailedBossBalitaReporterSetupMessage(_)

		and:
		assert HttpStatus.ACCEPTED == response.getStatusCode()
	}

	void "setupReporter should return NOT_FOUND if the user does not exist in our database."() {
		given:
		String accountId = "12346"
		String key = "my key"

		reporterRepository.findByInGameAccountIdAndKey(accountId, key) >> Optional.empty()

		when:
		ResponseEntity response = controller.setupReporter(accountId, key)

		then:
		0 * ghervisApi.sendFailedBossBalitaReporterSetupMessage(_)
		0 * ghervisApi.sendSuccessfulBossBalitaReporterSetupMessage(_)

		and:
		assert HttpStatus.NOT_FOUND == response.getStatusCode()
	}

	void "setupReporter should return NOT_FOUND if the user exists in our database and their discord id is invalid."() {
		given:
		String accountId = "12346"
		String key = "my key"
		Long discordId = 12346677

		reporterRepository.findByInGameAccountIdAndKey(accountId, key) >> Optional.empty()
		discordApi.getUserById(discordId) >> {
			throw new CompletionException()
		}

		when:
		ResponseEntity response = controller.setupReporter(accountId, key)

		then:
		0 * ghervisApi.sendSuccessfulBossBalitaReporterSetupMessage(_)
		0 * ghervisApi.sendFailedBossBalitaReporterSetupMessage(_)

		and:
		assert HttpStatus.NOT_FOUND == response.getStatusCode()
	}

}
