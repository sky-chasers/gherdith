package com.megalomaniac.pepperpotts.web.bossbalita

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter
import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporterRepository
import spock.lang.Specification

class BossBalitaControllerSpec extends Specification {

	BossBalitaController controller

	BossBalitaService bossBalitaService = Mock(BossBalitaService)
	BossBalitaReporterRepository bossBalitaReporterRepository = Mock(BossBalitaReporterRepository)

	void setup() {
		controller = new BossBalitaController(bossBalitaService, bossBalitaReporterRepository)
	}

	void "announceBossArrival should send an alert to discord when a request is made." () {
		given:
		String accountId = "1234567890"
		String key = "somekey"
		Map<String, String> bossLog = [
				"Demon Lord Mirtis" : "01/20/2020-01:20.40",
				"Demon Lord Blut" : "01/20/2020-01:20.40"
		]

		BossBalitaBodyRequest bossBalitaBodyRequest = new BossBalitaBodyRequest()
		bossBalitaBodyRequest.setBossLog(bossLog)
		bossBalitaBodyRequest.setTeamName("someone")

		BossBalitaReporter reporter = Mock(BossBalitaReporter)
		reporter.getTeamName() >> "Ghervis"
		reporter.getKey() >> key
		reporter.getInGameAccountId() >> accountId

		bossBalitaReporterRepository.findByInGameAccountIdAndKey(accountId, key) >> Optional.of(reporter)

		List<BossBalitaRequest> expectedRequests = [
				Mock(BossBalitaRequest),
				Mock(BossBalitaRequest)
		]

		bossBalitaService.generateRequests(reporter, bossLog) >> expectedRequests

		when:
		controller.announceBossArrival(accountId, key, bossBalitaBodyRequest)

		then:
		1 * bossBalitaService.sendBossAlertToDiscord(expectedRequests)
	}

	void "announceBossArrival should ignore the alert if the reporter is not recognized." () {
		given:
		String accountId = "1234567890"
		String key = "somekey"
		Map<String, String> bossLog = [
				"Demon Lord Mirtis" : "01/20/2020-01:20.40",
				"Demon Lord Blut" : "01/20/2020-01:20.40"
		]

		BossBalitaBodyRequest bossBalitaBodyRequest = new BossBalitaBodyRequest()
		bossBalitaBodyRequest.setBossLog(bossLog)
		bossBalitaBodyRequest.setTeamName("someone")

		bossBalitaReporterRepository.findByInGameAccountIdAndKey(accountId, key) >> Optional.empty()

		when:
		controller.announceBossArrival(accountId, key, bossBalitaBodyRequest)

		then:
		0 * bossBalitaService.sendBossAlertToDiscord(_)
	}
}
