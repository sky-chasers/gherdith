package com.megalomaniac.pepperpotts.web.bossbalita

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter
import com.megalomaniac.pepperpotts.db.demonlord.DemonLordSpawnDetails
import com.megalomaniac.pepperpotts.db.demonlord.DemonLordRepository
import com.megalomaniac.pepperpotts.db.demonlord.DemonLordService
import com.megalomaniac.pepperpotts.ghervis.api.DemonLord
import com.megalomaniac.pepperpotts.ghervis.api.GhervisApi
import com.megalomaniac.pepperpotts.utils.DateUtils
import spock.lang.Specification

import java.time.LocalDateTime

class BossBalitaServiceSpec extends Specification {

	BossBalitaService service

	DateUtils dateUtils = Mock(DateUtils)
	DemonLordRepository demonLordRepository = Mock(DemonLordRepository)
	DemonLordService demonLordService = Mock(DemonLordService)
	BossBalitaRecorder bossBalitaRecorder = Mock(BossBalitaRecorder)

	GhervisApi ghervisApi = Mock(GhervisApi)
	String DATE_TIME_FORMAT = "MM/dd/yyyy-HH:mm.ss"

	void setup() {
		service = new BossBalitaService(dateUtils, demonLordRepository, demonLordService, ghervisApi, bossBalitaRecorder)
	}

	void "generateRequests should generate a list of Boss Balita Requests with the correct details"() {
		given:
		String dateTimeMirtis = "03/28/2020-01:44.40"
		String dateTimeBlut = "03/28/2020-01:44.50"

		BossBalitaReporter reportedBy = Mock(BossBalitaReporter)
		reportedBy.getTeamName() >> "Ghervis"

		Map<String, String> bossSpawnDetails = [
				"Demon Lord Mirtis" : dateTimeMirtis,
				"Demon Lord Blut" : dateTimeBlut
		]

		LocalDateTime expectedTimeForMirtis = LocalDateTime.of(2020, 03, 28, 01,44, 40)
		LocalDateTime expectedTimeForBlut = LocalDateTime.of(2020, 03, 28, 01,44, 50)

		dateUtils.convertStringToLocalDateTime(dateTimeMirtis, DATE_TIME_FORMAT) >> expectedTimeForMirtis
		dateUtils.convertStringToLocalDateTime(dateTimeBlut, DATE_TIME_FORMAT) >> expectedTimeForBlut

		when:
		List<BossBalitaRequest> actualRequests = service.generateRequests(reportedBy, bossSpawnDetails)

		then:
		assert expectedTimeForMirtis == actualRequests[0].getDateTimeAppeared()
		assert reportedBy == actualRequests[0].getReportedBy()
		assert DemonLord.DEMON_LORD_MIRTIS == actualRequests[0].getBoss()

		and:
		assert expectedTimeForBlut == actualRequests[1].getDateTimeAppeared()
		assert reportedBy == actualRequests[1].getReportedBy()
		assert DemonLord.DEMON_LORD_BLUT == actualRequests[1].getBoss()
	}

	void "sendBossAlertToDiscord should send an alert to discord if the dateTime recorded in our database before the time indicated in the request."() {
		given:
		LocalDateTime expectedTimeForMirtis = LocalDateTime.now()
		LocalDateTime expectedTimeForBlut = LocalDateTime.now()

		BossBalitaReporter reporter = Mock(BossBalitaReporter)

		BossBalitaRequest bossBalitaRequest1 = Mock(BossBalitaRequest)
		bossBalitaRequest1.getBoss() >> DemonLord.DEMON_LORD_BLUT
		bossBalitaRequest1.getDateTimeAppeared() >> expectedTimeForBlut
		bossBalitaRequest1.getReportedBy() >> reporter

		BossBalitaRequest bossBalitaRequest2 = Mock(BossBalitaRequest)
		bossBalitaRequest2.getBoss() >> DemonLord.DEMON_LORD_MIRTIS
		bossBalitaRequest2.getDateTimeAppeared() >> expectedTimeForMirtis
		bossBalitaRequest2.getReportedBy() >> reporter

		List<BossBalitaRequest> requestList = [
		        bossBalitaRequest1,
				bossBalitaRequest2
		]

		DemonLordSpawnDetails blut = Mock(DemonLordSpawnDetails)
		blut.getDateAppeared() >> expectedTimeForBlut.minusHours(4)
		blut.getDiscordTagId() >> 19L

		DemonLordSpawnDetails mirtis = Mock(DemonLordSpawnDetails)
		mirtis.getDateAppeared() >> expectedTimeForMirtis.minusHours(4)
		mirtis.getDiscordTagId() >> 90L

		demonLordService.fetchOrCreateDemonLord(bossBalitaRequest1.getBoss(), expectedTimeForBlut) >> blut
		demonLordService.fetchOrCreateDemonLord(bossBalitaRequest2.getBoss(), expectedTimeForMirtis) >> mirtis

		when:
		service.sendBossAlertToDiscord(requestList)

		then:
		1 * ghervisApi.sendBossBalitaAlertToGhervis(bossBalitaRequest1, blut.getDiscordTagId())
		1 * ghervisApi.sendBossBalitaAlertToGhervis(bossBalitaRequest2, mirtis.getDiscordTagId())
	}

	void "sendBossAlertToDiscord should update the demon lord records if the dateTime recorded in our database before the time indicated in the request."() {
		given:
		LocalDateTime expectedTimeForBlut = LocalDateTime.now()

		BossBalitaRequest bossBalitaRequest1 = Mock(BossBalitaRequest)
		bossBalitaRequest1.getBoss() >> DemonLord.DEMON_LORD_BLUT
		bossBalitaRequest1.getDateTimeAppeared() >> expectedTimeForBlut

		List<BossBalitaRequest> requestList = [
				bossBalitaRequest1
		]

		DemonLordSpawnDetails blut = new DemonLordSpawnDetails()
		blut.setDateAppeared(expectedTimeForBlut.minusHours(4))

		demonLordService.fetchOrCreateDemonLord(bossBalitaRequest1.getBoss(), expectedTimeForBlut) >> blut

		when:
		service.sendBossAlertToDiscord(requestList)

		then:
		1 * demonLordRepository.save(_) >> { DemonLordSpawnDetails savedDemonLord ->
			assert expectedTimeForBlut == savedDemonLord.getDateAppeared()
		}
	}
}
