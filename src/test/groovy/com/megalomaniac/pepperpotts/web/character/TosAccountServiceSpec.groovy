package com.megalomaniac.pepperpotts.web.character

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccountService
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccountRepository
import spock.lang.Specification

class TosAccountServiceSpec extends Specification {

	TosAccountService service
	TosAccountRepository tosAccountRepository = Mock(TosAccountRepository)

	void setup() {
		service = new TosAccountService(tosAccountRepository)
	}

	void "createTosAccount should create a tos account with an empty team names list."() {
		given:
		String accountId = "123456"

		when:
		service.createTosAccount(accountId)

		then:
		1 * tosAccountRepository.save(_ as TosAccount) >> { TosAccount savedTosAccount ->
			assert accountId == savedTosAccount.getInGameAccountId()
			assert [] == savedTosAccount.getCharacters()
		}
	}

}
