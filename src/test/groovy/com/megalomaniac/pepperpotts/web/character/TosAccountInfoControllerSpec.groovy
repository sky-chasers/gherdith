package com.megalomaniac.pepperpotts.web.character

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccountRepository
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccountService
import com.megalomaniac.pepperpotts.db.character.CharacterService
import spock.lang.Specification

class TosAccountInfoControllerSpec extends Specification {

	CharacterInfoController controller

	TosAccountService tosAccountService = Mock(TosAccountService)
	TosAccountRepository characterRepository = Mock(TosAccountRepository)
	CharacterService characterService = Mock(CharacterService)

	void setup() {
		controller = new CharacterInfoController(
				tosAccountService,
				characterRepository,
				characterService
		)
	}

	void "saveCharacterInfo should create a new character if the character with that account id does not exist yet."() {
		given:
		String accountId = "1234567"
		List<String> teamNames = [
				"Rio",
				"Denver",
				"Helsinki"
		]

		CharacterInfoRequest characterInfo = Mock(CharacterInfoRequest)
		characterInfo.getAccountId() >> accountId
		characterInfo.getTeamNames() >> teamNames

		characterRepository.findByInGameAccountId(accountId) >> Optional.empty()

		when:
		controller.saveTeamNames(characterInfo)

		then:
		1 * tosAccountService.createTosAccount(accountId)
	}

	void "saveCharacterInfo should save the team names if the character with that account id exists."() {
		given:
		String accountId = "1234567"
		List<String> teamNames = [
				"Rio",
				"Denver",
				"Helsinki"
		]

		CharacterInfoRequest characterInfo = Mock(CharacterInfoRequest)
		characterInfo.getAccountId() >> accountId
		characterInfo.getTeamNames() >> teamNames

		TosAccount character = Mock(TosAccount)

		characterRepository.findByInGameAccountId(accountId) >> Optional.of(character)

		when:
		controller.saveTeamNames(characterInfo)

		then:
		1 * this.characterService.updateTeamNames(character, teamNames)
	}

	void "saveCharacterInfo should return the request after saving."() {
		given:
		String accountId = "1234567"
		List<String> teamNames = [
				"Rio",
				"Denver",
				"Helsinki"
		]

		CharacterInfoRequest request = Mock(CharacterInfoRequest)
		request.getAccountId() >> accountId
		request.getTeamNames() >> teamNames

		TosAccount character = Mock(TosAccount)

		characterRepository.findByInGameAccountId(accountId) >> Optional.of(character)

		when:
		CharacterInfoRequest actualResponse = controller.saveTeamNames(request)

		then:
		assert request == actualResponse
	}

}
