package com.megalomaniac.pepperpotts.ghervis.api

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter
import com.megalomaniac.pepperpotts.utils.RestApi
import com.megalomaniac.pepperpotts.web.bossbalita.BossBalitaRequest
import org.javacord.api.entity.user.User
import org.json.JSONObject
import spock.lang.Specification

import java.time.LocalDateTime

class GhervisApiSpec extends Specification {

	GhervisApi api

	RestApi restApi = Mock(RestApi)
	BossBalitaRequestBuilder requestBuilder = Mock(BossBalitaRequestBuilder)
	ReporterSetupMessageBuilder reporterSetupMessageBuilder = Mock(ReporterSetupMessageBuilder)
	String GHERVIS_BASE_URL = "baseurl.com"


	void setup() {
		api = new GhervisApi(restApi, requestBuilder, reporterSetupMessageBuilder, GHERVIS_BASE_URL)
	}

	void "sendBossBalitaAlertToGhervis() should send a POST request to ghervis when all conditions are met."() {
		given:
		LocalDateTime dateTimeAppeared = LocalDateTime.now()
		LocalDateTime estimatedTimeOfArrival = dateTimeAppeared.plusMinutes(10)

		BossBalitaReporter reporter = Mock(BossBalitaReporter)
		reporter.getTeamName() >> "Eggs"

		BossBalitaRequest bossBalitaRequest = Mock(BossBalitaRequest)
		bossBalitaRequest.getBoss() >> DemonLord.DEMON_LORD_BLUT
		bossBalitaRequest.getReportedBy() >> reporter
		bossBalitaRequest.getEstimatedDateOfArrival() >> estimatedTimeOfArrival
		bossBalitaRequest.getDateTimeAppeared() >> dateTimeAppeared

		Long discordTagId = 1234567

		JSONObject expectedJsonRequest = Mock(JSONObject)
		requestBuilder.buildRequest(bossBalitaRequest, discordTagId) >> expectedJsonRequest

		String expectedUrl = GHERVIS_BASE_URL + "send-channel-message"

		when:
		api.sendBossBalitaAlertToGhervis(bossBalitaRequest, discordTagId)

		then:
		1 * restApi.postRequest(expectedUrl, expectedJsonRequest)
	}

	void "sendSuccessfulBossBalitaReporterSetupMessage() should send a POST request to ghervis when all conditions are met."() {
		given:
		User user = Mock(User)
		JSONObject message = Mock(JSONObject)
		reporterSetupMessageBuilder.buildSuccessSetupMessage(user) >> message

		String expectedUrl = GHERVIS_BASE_URL + "send-private-message"

		when:
		api.sendSuccessfulBossBalitaReporterSetupMessage(user)

		then:
		1 * restApi.postRequest(expectedUrl, message)
	}

	void "sendFailedBossBalitaReporterSetupMessage() should send a POST request to ghervis when all conditions are met."() {
		given:
		User user = Mock(User)
		JSONObject message = Mock(JSONObject)
		reporterSetupMessageBuilder.buildFailedSetupMessage(user) >> message

		String expectedUrl = GHERVIS_BASE_URL + "send-private-message"

		when:
		api.sendFailedBossBalitaReporterSetupMessage(user)

		then:
		1 * restApi.postRequest(expectedUrl, message)
	}
}
