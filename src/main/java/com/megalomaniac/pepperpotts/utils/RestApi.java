package com.megalomaniac.pepperpotts.utils;

import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Log4j2
@Service
public class RestApi {

	private final RestTemplate restTemplate;

	public RestApi(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public String getRequest(String url) throws GetRequestException {
		UriComponents uriComponents = UriComponentsBuilder
				.fromUriString(url)
				.build();

		URI uri = uriComponents.toUri();
		HttpEntity<String> entity = new HttpEntity<>("", buildHeaders());
		log.info("Sending GET request to url {}", uriComponents.toUriString());

		try {

			ResponseEntity<String> response = restTemplate.exchange(
					uri, HttpMethod.GET, entity, new ParameterizedTypeReference<String>() {}
			);

			return response.getBody();
		} catch (HttpStatusCodeException exception) {
			log.error("Failed to fetch from url: {} | \n Status Code: {} |  \n Error: {}", url, exception.getStatusCode(), exception.getLocalizedMessage());
			throw new GetRequestException();
		} catch (Exception exception) {
			//I shouldn't do this. Bad butter!
			log.error("Failed to fetch from url: {} | \n Error: {}", url, exception.getMessage());
			throw new GetRequestException();
		}
	}

	public String postRequest(String url, JSONObject messageBody) throws PostRequestException {
		UriComponents uriComponents = UriComponentsBuilder
				.fromUriString(url)
				.build();

		URI uri = uriComponents.toUri();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(messageBody.toString(), headers);

		try {

			ResponseEntity<String> response = restTemplate.exchange(
					uri, HttpMethod.POST, entity, new ParameterizedTypeReference<String>() {}
			);

			return response.getBody();
		} catch (HttpStatusCodeException exception) {
			log.error("Failed to send POST request to url: {} | \n Status Code: {} |  \n Error: {}", url, exception.getStatusCode(), exception.getLocalizedMessage());
			throw new PostRequestException();
		} catch (Exception exception) {
			log.error("Failed to send POST request to url: {} | \n Error: {}", url, exception.getMessage());
			throw new PostRequestException();
		}
	}


	private HttpHeaders buildHeaders() {
		return new HttpHeaders();
	}
}
