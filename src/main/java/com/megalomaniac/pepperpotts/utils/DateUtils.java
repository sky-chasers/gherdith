package com.megalomaniac.pepperpotts.utils;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;

@Log4j2
@Service
public class DateUtils {

	public LocalDateTime getCurrentDateTime() {
		return LocalDateTime.now();
	}

	public LocalDate convertOrdinalDateToLocalDate(String date) {
		String[] subdividedDate = date.split(" ");

		Month month = Month.valueOf(subdividedDate[0].toUpperCase());
		int day = Integer.parseInt(subdividedDate[1].replaceAll("[^\\d.]", ""));
		int year = Integer.parseInt(subdividedDate[2]);

		return LocalDate.of(year, month.getValue(), day);
	}

	public LocalDate convertKtestNewsDateToLocalDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
		return LocalDate.parse(date, formatter);
	}

	public LocalDate convertKtosDevBlogDateToLocalDate(String rawDate) {
		//rawDate: 2020.09.25 오후 01:00
		String cleanedDate = rawDate.substring(0, 10);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd");
		return LocalDate.parse(cleanedDate, formatter);
	}

	public LocalDateTime convertStringToLocalDateTime(String dateTimeString, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return LocalDateTime.parse(dateTimeString, formatter);
	}

}
