package com.megalomaniac.pepperpotts.tos.news;

import com.megalomaniac.pepperpotts.utils.DateUtils;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class TosNewsExtractor {

	private final String tosHomePageUrl;
	private final DateUtils dateUtils;

	public TosNewsExtractor(String tosHomePageUrl, DateUtils dateUtils) {
		this.tosHomePageUrl = tosHomePageUrl;
		this.dateUtils = dateUtils;
	}

	List<TosNews> extractItosNewsFromDocument(Document document) {
		List<TosNews> tosNewsList = new ArrayList<>();

		Elements newsBox = document.select("div.news_box");

		newsBox.forEach(element -> {
			Elements title = element.select(".contents h3.ellipsis");
			Elements content = element.select("p.ellipsis");
			Element url = element.select("a").first();
			Element datePublished = element.select(".date.right").first();

			String cleanContent = Jsoup.clean(content.text(), Whitelist.none());
			String cleanTitle = Jsoup.clean(title.text(), Whitelist.none());
			LocalDate cleanDatePublished = dateUtils.convertOrdinalDateToLocalDate(datePublished.text());

			TosNews tosNews = new TosNews();
			tosNews.setTitle(cleanTitle);
			tosNews.setUrl(tosHomePageUrl + url.attr("href"));
			tosNews.setContent(cleanContent);
			tosNews.setDatePublished(cleanDatePublished);

			tosNewsList.add(tosNews);
		});

		return tosNewsList;
	}

	List<TosNews> extractKNewsFromDocument(String baseUrl, Document document) {
		List<TosNews> tosNewsList = new ArrayList<>();

		Elements noticeBoxes = document.select("table.table tbody tr");

		noticeBoxes.forEach(element ->  {
			Elements title = element.select("td.txtLeft p.oneline");
			Element url = element.select("a").first();
			String datePublished = element.select("td.txtLeft p").get(1).text();

			String cleanTitle = Jsoup.clean(title.text(), Whitelist.none());
			LocalDate cleanDatePublished = dateUtils.convertKtestNewsDateToLocalDate(datePublished);

			TosNews tosNews = new TosNews();
			tosNews.setTitle(cleanTitle);
			tosNews.setUrl(baseUrl + url.attr("href"));
			tosNews.setContent("");
			tosNews.setDatePublished(cleanDatePublished);

			if (newsIsUnique(tosNewsList, tosNews)) {
				tosNewsList.add(tosNews);
			}
		});

		return tosNewsList;
	}

	List<TosNews> extractKtosDevBlogsFromDocument(Document document) {
		List<TosNews> tosNewsList = new ArrayList<>();
		String baseUrl = "https://tos.nexon.com";

		Elements newsBoxes = document.select("ul.middle_list li");

		newsBoxes.forEach(element -> {
			Elements title = element.select("div.contenttop_left .oneline");
			Element url = element.select("a").first();
			Elements datePublished = element.select("div.contenttop_left>span");

			if (url != null && title != null && datePublished.size() > 0) {
				String cleanTitle = Jsoup.clean(title.text(), Whitelist.none());
				LocalDate cleanDatePublished = dateUtils.convertKtosDevBlogDateToLocalDate(datePublished.get(0).text());

				TosNews tosNews = new TosNews();
				tosNews.setTitle(cleanTitle);
				tosNews.setUrl(baseUrl + url.attr("href"));
				tosNews.setDatePublished(cleanDatePublished);

				tosNewsList.add(tosNews);
			}
		});

		return tosNewsList;
	}

	private boolean newsIsUnique(List<TosNews> list, TosNews news) {
		return list
				.stream()
				.noneMatch(tosNews -> tosNews.getUrl().equals(news.getUrl()));
	}
}
