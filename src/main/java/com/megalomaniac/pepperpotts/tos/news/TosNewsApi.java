package com.megalomaniac.pepperpotts.tos.news;

import com.megalomaniac.pepperpotts.utils.GetRequestException;
import com.megalomaniac.pepperpotts.utils.RestApi;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j2
@Service
public class TosNewsApi {

	private final RestApi restApi;
	private final TosNewsExtractor tosNewsExtractor;
	private final String tosAllNewsBaseUrl;
	private final String ktestNewsBaseUrl;
	private final String ktosAllNewsBaseUrl;
	private final String ktestHomePageUrl;
	private final String ktosNewsPageUrl;
	private final String ktosDevBlogUrl;

	public TosNewsApi(RestApi restApi, TosNewsExtractor tosNewsExtractor, String tosAllNewsBaseUrl, String ktestNewsBaseUrl,
					  String ktosAllNewsBaseUrl, String ktestHomePageUrl, String ktosNewsPageUrl, String ktosDevBlogUrl) {
		this.restApi = restApi;
		this.tosNewsExtractor = tosNewsExtractor;
		this.tosAllNewsBaseUrl = tosAllNewsBaseUrl;
		this.ktestNewsBaseUrl = ktestNewsBaseUrl;
		this.ktosAllNewsBaseUrl = ktosAllNewsBaseUrl;
		this.ktestHomePageUrl = ktestHomePageUrl;
		this.ktosNewsPageUrl = ktosNewsPageUrl;
		this.ktosDevBlogUrl = ktosDevBlogUrl;
	}

	public List<TosNews> fetchAllItosNews() throws GetRequestException {
		String tosNewsResultHtml = restApi.getRequest(tosAllNewsBaseUrl);
		Document document = Jsoup.parse(tosNewsResultHtml);

		return tosNewsExtractor.extractItosNewsFromDocument(document);
	}

	public List<TosNews> fetchAllKtestNews() throws GetRequestException {
		String tosNewsResultHtml = restApi.getRequest(ktestNewsBaseUrl);
		Document document = Jsoup.parse(tosNewsResultHtml);

		return tosNewsExtractor.extractKNewsFromDocument(ktestHomePageUrl, document);
	}

	public List<TosNews> fetchAllKtosNews() throws GetRequestException {
		String tosNewsResultHtml = restApi.getRequest(ktosNewsPageUrl);
		Document document = Jsoup.parse(tosNewsResultHtml);

		return tosNewsExtractor.extractKNewsFromDocument(ktosAllNewsBaseUrl, document);
	}

	public List<TosNews> fetchAllDevBlogs() throws GetRequestException {
		String tosNewsResultHtml = restApi.getRequest(ktosDevBlogUrl);
		Document document = Jsoup.parse(tosNewsResultHtml);

		return tosNewsExtractor.extractKtosDevBlogsFromDocument(document);
	}
}
