package com.megalomaniac.pepperpotts.tos.news;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class TosNews {

	private String title;
	private String content;
	private String url;
	private LocalDate datePublished;

	public String getTrimmedContent(int maxLength) {
		if(this.content.length() > 0 && this.content.length() > maxLength) {
			return this.content.substring(0, maxLength);
		}

		return this.content;
	}
}
