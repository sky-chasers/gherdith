package com.megalomaniac.pepperpotts.naver.papago;

import com.megalomaniac.pepperpotts.naver.papago.transalatedresult.PapagoResponse;
import com.megalomaniac.pepperpotts.utils.PostRequestException;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Log4j2
@Service
public class PapagoTranslationApi {

	private final String papagoUrl="https://openapi.naver.com/v1/papago/n2mt";
	private final String papagoClientId;
	private final String papagoClientSecret;

	private final RestTemplate restTemplate;

	public PapagoTranslationApi(String papagoClientId, String papagoClientSecret, RestTemplate restTemplate) {
		this.papagoClientId = papagoClientId;
		this.papagoClientSecret = papagoClientSecret;
		this.restTemplate = restTemplate;
	}


	public String translate(PapagoLanguage source, PapagoLanguage target, String text) throws PostRequestException {
		UriComponents uriComponents = UriComponentsBuilder
				.fromUriString(papagoUrl)
				.build();

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		headers.set("X-Naver-Client-Id", papagoClientId);
		headers.set("X-Naver-Client-Secret", papagoClientSecret);

		MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
		body.add("source", source.getLanguageCode());
		body.add("target", target.getLanguageCode());
		body.add("text", text);

		HttpEntity<MultiValueMap> entity = new HttpEntity<>(body, headers);

		try {
			ResponseEntity<PapagoResponse> rawResponse = restTemplate.exchange(
					uriComponents.toUri(), HttpMethod.POST, entity, new ParameterizedTypeReference<PapagoResponse>() {}
			);

			return rawResponse.getBody()
					.getMessage()
					.getResult()
					.getTranslatedText();
		} catch (Exception e) {
			log.error("Failed to translate due to exception message: {}", e.getMessage());
			throw new PostRequestException();
		}
	}

}
