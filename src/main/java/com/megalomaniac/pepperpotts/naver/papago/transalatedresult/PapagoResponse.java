package com.megalomaniac.pepperpotts.naver.papago.transalatedresult;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class PapagoResponse {

    private TranslatedMessage message;

    public PapagoResponse(){}
}
