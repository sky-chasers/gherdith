package com.megalomaniac.pepperpotts.naver.papago;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PapagoConfig {


    @Value("${papago.clientId}")
    private String papagoClientId;

    @Value("${papago.clientSecret}")
    private String papagoClientSecret;

    @Bean("papagoClientId")
    public String getPapagoClientId() {
        return papagoClientId;
    }

    @Bean("papagoClientSecret")
    public String getPapagoClientSecret() {
        return papagoClientSecret;
    }
}
