package com.megalomaniac.pepperpotts.naver.papago.transalatedresult;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TranslatedResult {

    private String tarLangType;
    private String translatedText;
    private String srcLangType;
    private String engineType;
    private String pivot;

    public TranslatedResult() {}

}
