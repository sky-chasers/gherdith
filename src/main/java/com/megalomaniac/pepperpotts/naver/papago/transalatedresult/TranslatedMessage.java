package com.megalomaniac.pepperpotts.naver.papago.transalatedresult;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TranslatedMessage {

    private TranslatedResult result;

    @JsonProperty("@type")
    private String type;

    @JsonProperty("@version")
    private String version;

    @JsonProperty("@service")
    private String service;

    public TranslatedMessage() {
    }
}
