package com.megalomaniac.pepperpotts.naver.papago;

import lombok.Getter;

public enum PapagoLanguage {

	ENGLISH("English", "en"),
	KOREAN("Korean", "ko");

	@Getter
	private final String languageCode;

	@Getter
	private final String language;

	PapagoLanguage(String language, String languageCode) {
		this.language = language;
		this.languageCode = languageCode;
	}
}
