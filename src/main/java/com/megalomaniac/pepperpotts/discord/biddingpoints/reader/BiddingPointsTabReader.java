package com.megalomaniac.pepperpotts.discord.biddingpoints.reader;

import com.megalomaniac.pepperpotts.google.sheets.SheetReader;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@Service
public class BiddingPointsTabReader {

	private final SheetReader sheetReader;
	private final String biddingPointsTabName;
	private final String biddingPointsTeamName;
	private final String biddingPointsTotalBiddingPoints;
	private final String biddingPointsBorutaPoints;
	private final String biddingPointsGtwPoints;
	private final String biddingPointsBonusBorutaPoints;

	public BiddingPointsTabReader(SheetReader sheetReader,
								  String biddingPointsTabName,
								  String biddingPointsTeamName,
								  String biddingPointsTotalBiddingPoints,
								  String biddingPointsBorutaPoints,
								  String biddingPointsGtwPoints,
								  String biddingPointsBonusBorutaPoints) {
		this.sheetReader = sheetReader;
		this.biddingPointsTabName = biddingPointsTabName;
		this.biddingPointsTeamName = biddingPointsTeamName;
		this.biddingPointsTotalBiddingPoints = biddingPointsTotalBiddingPoints;
		this.biddingPointsBorutaPoints = biddingPointsBorutaPoints;
		this.biddingPointsGtwPoints = biddingPointsGtwPoints;
		this.biddingPointsBonusBorutaPoints = biddingPointsBonusBorutaPoints;
	}

	public List<BiddingPointDetail> fetchBiddingPointDetails(String googleSheetId) throws FailedToReadBiddingPointsException, LabelNotFoundException {
		try {
			List<BiddingPointDetail> finalOutput = new ArrayList<>();
			String RANGE = biddingPointsTabName + "!A2:M1000";

			List<List<Object>> rawOutput = sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, RANGE);
			List<String> labels = rawOutput.get(0).stream()
					.map(object -> Objects.toString(object, null))
					.collect(Collectors.toList());

			Integer teamNameLabelIndex = findIndexOfLabel(labels, biddingPointsTeamName, googleSheetId);
			Integer totalBiddingPointsIndex = findIndexOfLabel(labels, biddingPointsTotalBiddingPoints, googleSheetId);
			Integer borutaPointsIndex = findIndexOfLabel(labels, biddingPointsBorutaPoints, googleSheetId);
			Integer gtwPointsIndex = findIndexOfLabel(labels, biddingPointsGtwPoints, googleSheetId);
			Integer borutaBonusPointsIndex = findIndexOfLabel(labels, biddingPointsBonusBorutaPoints, googleSheetId);


			String borutaBonusPointsColumnLetter = getLetterWithIndex(borutaBonusPointsIndex);

			int currentRowNumber = 3;

			for (List <Object> row : rawOutput.subList(1, rawOutput.size())) {
				String teamName = row.get(teamNameLabelIndex).toString();
				BigDecimal gtwPoints = convertStringToNumber(row.get(gtwPointsIndex).toString());
				BigDecimal totalBiddingPoints = convertStringToNumber(row.get(totalBiddingPointsIndex).toString());
				BigDecimal borutaPoints =  convertStringToNumber(row.get(borutaPointsIndex).toString());

				BiddingPointDetail biddingPointDetail = new BiddingPointDetail();
				biddingPointDetail.setRowNumber(currentRowNumber);
				biddingPointDetail.setTeamName(teamName);
				biddingPointDetail.setTotalBiddingPoints(totalBiddingPoints);
				biddingPointDetail.setGtwPoints(gtwPoints);
				biddingPointDetail.setBorutaPoints(borutaPoints);
				biddingPointDetail.setBorutaBonusPointsColumnLetter(borutaBonusPointsColumnLetter);

				currentRowNumber = currentRowNumber + 1;
				finalOutput.add(biddingPointDetail);
			}

			return finalOutput;

		} catch (IOException e) {
			log.error("Failed to read bidding points in google sheet with id: {}", googleSheetId);
			log.error(e.getMessage());
			throw new FailedToReadBiddingPointsException();
		}
	}

	private Integer findIndexOfLabel(List<String> labels, String label, String googleSheetId) throws LabelNotFoundException {
			Integer labelIndex = labels.indexOf(label);

			if (labelIndex == -1) {
				log.error("Failed to read bidding points in google sheet with id: {}. One of the labels: {} is not found.", googleSheetId, label);
				throw new LabelNotFoundException(label, "The " + label + " label was not found.");
			}

			return labelIndex;
	}

	private String getLetterWithIndex(int index) {
		return Character.toString("ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(index));
	}

	private BigDecimal convertStringToNumber(String numberString) {
		try {
			return new BigDecimal(numberString);
		} catch (NumberFormatException e) {
			return BigDecimal.ZERO;
		}
	}
}
