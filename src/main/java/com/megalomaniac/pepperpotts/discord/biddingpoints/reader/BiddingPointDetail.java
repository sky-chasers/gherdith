package com.megalomaniac.pepperpotts.discord.biddingpoints.reader;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

public class BiddingPointDetail {

	@Getter
	@Setter
	private String teamName;

	@Getter
	@Setter
	private Integer rowNumber;

	@Getter
	@Setter
	private String borutaBonusPointsColumnLetter;

	@Getter
	@Setter
	private BigDecimal totalBiddingPoints;

	@Getter
	@Setter
	private BigDecimal gtwPoints;

	@Getter
	@Setter
	private BigDecimal borutaPoints;

}
