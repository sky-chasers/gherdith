package com.megalomaniac.pepperpotts.discord.biddingpoints.reader;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BiddingPointsReaderConfig {

	@Value("${megalomaniac.pepperpotts.googleSheets.consolidatedAttendance.biddingPointsTab.name}")
	private String biddingPointsTabName;

	@Bean(name = "biddingPointsTabName")
	public String getBiddingPointsTabName() {
		return biddingPointsTabName;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.consolidatedAttendance.biddingPointsTab.teamName}")
	private String biddingPointsTeamName;

	@Bean(name = "biddingPointsTeamName")
	public String getBiddingPointsTeamName() {
		return biddingPointsTeamName;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.consolidatedAttendance.biddingPointsTab.totalBiddingPoints}")
	private String totalBiddingPoints;

	@Bean(name = "biddingPointsTotalBiddingPoints")
	public String getTotalBiddingPoints() {
		return totalBiddingPoints;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.consolidatedAttendance.biddingPointsTab.borutaPoints}")
	private String borutaPoints;

	@Bean(name = "biddingPointsBorutaPoints")
	public String getBorutaPoints() {
		return borutaPoints;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.consolidatedAttendance.biddingPointsTab.gtwPoints}")
	private String gtwPoints;

	@Bean(name = "biddingPointsGtwPoints")
	public String getGtwPoints() {
		return gtwPoints;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.consolidatedAttendance.biddingPointsTab.bonusBorutaPoints}")
	private String bonusBorutaPoints;

	@Bean(name = "biddingPointsBonusBorutaPoints")
	public String getBonusBorutaPoints() {
		return bonusBorutaPoints;
	}
}
