package com.megalomaniac.pepperpotts.discord.biddingpoints.reader;

import lombok.Getter;
import lombok.Setter;

public class LabelNotFoundException extends Exception {

	@Getter
	@Setter
	private String message;

	@Getter
	@Setter
	private String offendingLabel;

	public LabelNotFoundException(String offendingLabel, String message) {
		this.offendingLabel = offendingLabel;
		this.message = message;
	}

}
