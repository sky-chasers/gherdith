package com.megalomaniac.pepperpotts.discord.help.user;

import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import com.megalomaniac.pepperpotts.discord.command.GeneralCommand;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class UserHelpListener implements MessageCreateListener {

	private final CommandValidator validator;
	private final GeneralCommand command = GeneralCommand.HELP;
	private final UserHelpService userHelpService;

	public UserHelpListener(CommandValidator validator,
							UserHelpService userHelpService) {
		this.validator = validator;
		this.userHelpService = userHelpService;
	}

	@Override
	public void onMessageCreate(MessageCreateEvent messageCreateEvent) {

		if (!validator.isValidCommand(messageCreateEvent, command)) {
			return;
		}

		userHelpService.displayListOfValidCommands(messageCreateEvent.getChannel());
	}

}
