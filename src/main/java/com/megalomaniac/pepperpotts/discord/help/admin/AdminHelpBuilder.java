package com.megalomaniac.pepperpotts.discord.help.admin;

import com.megalomaniac.pepperpotts.discord.command.AdminCommand;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

import java.awt.*;

@Service
public class AdminHelpBuilder {

	public EmbedBuilder buildHelpResponse() {
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setTitle("Pepper Admin Commands");
		embedBuilder.setColor(Color.orange);

		AdminCommand[] generalCommands = AdminCommand.values();

		for (AdminCommand command: generalCommands) {
			StringBuilder descriptionWithExample = new StringBuilder();
			descriptionWithExample.append(command.getDescription());
			descriptionWithExample.append("\nformat: \n```");
			descriptionWithExample.append(command.getFormat());
			descriptionWithExample.append("```");
			descriptionWithExample.append("example: \n```");
			descriptionWithExample.append(command.getExample());
			descriptionWithExample.append("```");
			descriptionWithExample.append("\u200B");

			embedBuilder.addField(command.getCommandString(), descriptionWithExample.toString());
		}

		return embedBuilder;
	}
}
