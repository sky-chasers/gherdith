package com.megalomaniac.pepperpotts.discord.help.user;

import com.megalomaniac.pepperpotts.discord.command.GeneralCommand;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

import java.awt.*;

@Service
public class UserHelpBuilder {

	EmbedBuilder buildHelpResponse() {
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setTitle("Pepper Commands");
		embedBuilder.setColor(Color.orange);

		GeneralCommand[] generalCommands = GeneralCommand.values();

		for (GeneralCommand command: generalCommands) {
			StringBuilder descriptionWithExample = new StringBuilder();
			descriptionWithExample.append(command.getDescription());
			descriptionWithExample.append("\n");
			descriptionWithExample.append("example: `");
			descriptionWithExample.append(command.getExample());
			descriptionWithExample.append("`");

			embedBuilder.addField(command.getCommandString(), descriptionWithExample.toString());
		}

		return embedBuilder;
	}
}
