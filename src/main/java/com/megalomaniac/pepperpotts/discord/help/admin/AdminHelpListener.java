package com.megalomaniac.pepperpotts.discord.help.admin;

import com.megalomaniac.pepperpotts.discord.command.AdminCommand;
import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class AdminHelpListener implements MessageCreateListener {

	private final CommandValidator validator;
	private final AdminHelpService adminHelpService;
	private final AdminCommand command = AdminCommand.ADMIN_HELP;

	public AdminHelpListener(CommandValidator validator,
							 AdminHelpService adminHelpService) {
		this.validator = validator;
		this.adminHelpService = adminHelpService;
	}

	@Override
	public void onMessageCreate(MessageCreateEvent event) {

		if (!validator.isValidCommand(event, command)) {
			return;
		}

		adminHelpService.displayListOfValidCommands(event.getMessage());
	}
}
