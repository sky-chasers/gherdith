package com.megalomaniac.pepperpotts.discord.help.user;

import com.megalomaniac.pepperpotts.discord.message.PublicMessageSender;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

@Service
public class UserHelpService {

	private final PublicMessageSender publicMessageSender;
	private final UserHelpBuilder userHelpBuilder;

	public UserHelpService(PublicMessageSender publicMessageSender, UserHelpBuilder userHelpBuilder) {
		this.publicMessageSender = publicMessageSender;
		this.userHelpBuilder = userHelpBuilder;
	}

	void displayListOfValidCommands(TextChannel textChannel) {
		EmbedBuilder embedBuilder = userHelpBuilder.buildHelpResponse();
		publicMessageSender.sendMessage(textChannel, embedBuilder);
	}
}
