package com.megalomaniac.pepperpotts.discord.help.admin;

import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

@Service
public class AdminHelpService {

	private final MessageSender messageSender;
	private final AdminHelpBuilder adminHelpBuilder;

	public AdminHelpService(MessageSender messageSender, AdminHelpBuilder adminHelpBuilder) {
		this.messageSender = messageSender;
		this.adminHelpBuilder = adminHelpBuilder;
	}

	public void displayListOfValidCommands(Message messageDetails) {
		EmbedBuilder message = adminHelpBuilder.buildHelpResponse();
		messageSender.sendMessage(messageDetails, message);
	}

}
