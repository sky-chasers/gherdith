package com.megalomaniac.pepperpotts.discord.bossbalita;

import com.megalomaniac.pepperpotts.db.character.Character;
import com.megalomaniac.pepperpotts.db.character.CharacterRepository;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.user.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Log4j2
@Service
public class BossBalitaKeyGenerator {

	private final CharacterRepository characterRepository;
	private final BossBalitaKeyGeneratorReplyBuilder replyBuilder;
	private final MessageSender messageSender;
	private final BossBalitaKeyGeneratorHelper processor;

	public BossBalitaKeyGenerator(CharacterRepository characterRepository,
								  BossBalitaKeyGeneratorReplyBuilder replyBuilder,
								  MessageSender messageSender,
								  BossBalitaKeyGeneratorHelper processor) {
		this.characterRepository = characterRepository;
		this.replyBuilder = replyBuilder;
		this.messageSender = messageSender;
		this.processor = processor;
	}

	public void generateKey(GenerateBossBalitaKeyCommand command) {
		String teamName = command.getTeamName();
		Optional<User> admin = command.getMessage().getUserAuthor();
		Optional<User> user = processor.fetchDiscordUser(command.getDiscordUserId());
		Optional<Character> characterResults = characterRepository.findByTeamName(teamName);

		if (user.isEmpty()) {
			processor.sendInvalidDiscordIdMessageToAdmin(command);
			return;
		}

		if (characterResults.isPresent() && admin.isPresent()) {
			processor.processKeyGeneration(command, user.get(), characterResults.get());
			return;
		}

		String failureMessage = replyBuilder.buildErrorMessage(teamName);
		messageSender.sendInfoMessage(command.getMessage(), failureMessage);
	}

}
