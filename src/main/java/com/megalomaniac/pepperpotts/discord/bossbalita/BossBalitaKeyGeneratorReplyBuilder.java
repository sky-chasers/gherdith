package com.megalomaniac.pepperpotts.discord.bossbalita;

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter;
import org.springframework.stereotype.Service;

@Service
public class BossBalitaKeyGeneratorReplyBuilder {

	public String buildSuccessMessage(BossBalitaReporter bossBalitaReporter) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Key has been successfully generated!");
		stringBuilder.append("\n\n **Account Details: **");
		stringBuilder.append("\n Team Name: ");
		stringBuilder.append(bossBalitaReporter.getTeamName());
		stringBuilder.append("\n Key: ||");
		stringBuilder.append(bossBalitaReporter.getKey());
		stringBuilder.append("|| \n\n");
		stringBuilder.append("Use the command below to apply the changes to the boss reporter addon. \n");
		stringBuilder.append("```");
		stringBuilder.append("/bossbalita init ");
		stringBuilder.append(bossBalitaReporter.getKey());
		stringBuilder.append("```");

		return stringBuilder.toString();
	}

	public String buildErrorMessage(String teamName) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Failed to generate key! We cannot find the character: ");
		stringBuilder.append(teamName);
		stringBuilder.append(" in our database.");

		return stringBuilder.toString();
	}

	public String buildInvalidDiscordIdMessage(Long discorUserId) {
		return "Failed to find discord user with id: " + discorUserId + ". The discord id might be invalid.";
	}
}
