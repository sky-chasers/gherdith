package com.megalomaniac.pepperpotts.discord.bossbalita;

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter;
import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporterRepository;
import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporterService;
import com.megalomaniac.pepperpotts.db.character.Character;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import com.megalomaniac.pepperpotts.discord.message.PrivateMessageSender;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.user.User;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.concurrent.CompletionException;

@Service
public class BossBalitaKeyGeneratorHelper {

	private final BossBalitaReporterRepository bossBalitaReporterRepository;
	private final BossBalitaKeyGeneratorReplyBuilder replyBuilder;
	private final MessageSender messageSender;
	private final BossBalitaReporterService bossBalitaReporterService;
	private final PrivateMessageSender privateMessageSender;
	private final DiscordApi pepperPotDiscordApi;

	public BossBalitaKeyGeneratorHelper(BossBalitaReporterRepository bossBalitaReporterRepository,
										BossBalitaKeyGeneratorReplyBuilder replyBuilder,
										MessageSender messageSender,
										BossBalitaReporterService bossBalitaReporterService,
										PrivateMessageSender privateMessageSender,
										DiscordApi pepperPotDiscordApi) {
		this.bossBalitaReporterRepository = bossBalitaReporterRepository;
		this.replyBuilder = replyBuilder;
		this.messageSender = messageSender;
		this.bossBalitaReporterService = bossBalitaReporterService;
		this.privateMessageSender = privateMessageSender;
		this.pepperPotDiscordApi = pepperPotDiscordApi;
	}

	public void sendInvalidDiscordIdMessageToAdmin(GenerateBossBalitaKeyCommand command) {
		String invalidDiscordUserIdMessage = replyBuilder.buildInvalidDiscordIdMessage(command.getDiscordUserId());
		messageSender.sendErrorMessage(command.getMessage(),invalidDiscordUserIdMessage);
	}

	public Optional<User> fetchDiscordUser(Long discordUserId) {
		try {
			User user = pepperPotDiscordApi.getUserById(discordUserId).join();
			return Optional.ofNullable(user);
		} catch (CompletionException e) {
			return Optional.empty();
		}
	}

	public void processKeyGeneration(GenerateBossBalitaKeyCommand command, User user, Character character) {
		String teamName = command.getTeamName();
		Optional<BossBalitaReporter> reporterResults = bossBalitaReporterRepository.findByTeamName(teamName);
		String inGameAccountId = character.getTosAccount().getInGameAccountId();

		BossBalitaReporter reporter;

		if (reporterResults.isPresent()) {
			reporter = bossBalitaReporterService.updateReporterKey(reporterResults.get(), command.getDiscordUserId());
		} else {
			reporter = bossBalitaReporterService.createReporter(teamName, inGameAccountId, command.getDiscordUserId());
		}

		sendSuccessMessageToUser(user, reporter);
		sendSuccessMessageToAdmin(command, reporter);
	}

	private void sendSuccessMessageToUser(User discordUser, BossBalitaReporter reporter) {
		String response = replyBuilder.buildSuccessMessage(reporter);
		privateMessageSender.sendSuccessMessageToUser(discordUser, response);
	}

	private void sendSuccessMessageToAdmin(GenerateBossBalitaKeyCommand command, BossBalitaReporter reporter) {
		String response = replyBuilder.buildSuccessMessage(reporter);
		messageSender.sendSuccessMessage(command.getMessage(), response);
	}
}
