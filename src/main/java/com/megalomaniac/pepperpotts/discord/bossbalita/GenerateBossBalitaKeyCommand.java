package com.megalomaniac.pepperpotts.discord.bossbalita;

import lombok.Getter;
import lombok.Setter;
import org.javacord.api.entity.message.Message;

public class GenerateBossBalitaKeyCommand {

	@Getter
	@Setter
	private String command;

	@Getter
	@Setter
	private String teamName;

	@Getter
	@Setter
	private Long discordUserId;

	@Getter
	@Setter
	private Message message;

	GenerateBossBalitaKeyCommand(Message message) {
		String wholeCommand = message.getContent();
		String[] splitCommand = wholeCommand.split(" ");

		this.command = splitCommand[0];
		this.teamName = splitCommand[1];
		this.discordUserId = Long.valueOf(splitCommand[2]);
		this.message = message;
	}
}
