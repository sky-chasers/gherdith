package com.megalomaniac.pepperpotts.discord.bossbalita;

import com.megalomaniac.pepperpotts.discord.command.AdminCommand;
import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

@Component
public class GenerateBossBalitaKeyListener implements MessageCreateListener {

	private final AdminCommand adminCommand = AdminCommand.GENERATE_BOSS_BALITA_KEYS;
	private final CommandValidator commandValidator;
	private final BossBalitaKeyGenerator bossBalitaKeyGenerator;

	public GenerateBossBalitaKeyListener(CommandValidator commandValidator,
										 BossBalitaKeyGenerator bossBalitaKeyGenerator) {
		this.commandValidator = commandValidator;
		this.bossBalitaKeyGenerator = bossBalitaKeyGenerator;
	}

	@Override
	public void onMessageCreate(MessageCreateEvent event) {

		if(!commandValidator.isValidCommand(event, adminCommand)) {
			return;
		}

		GenerateBossBalitaKeyCommand command = new GenerateBossBalitaKeyCommand(event.getMessage());
		bossBalitaKeyGenerator.generateKey(command);
	}
}
