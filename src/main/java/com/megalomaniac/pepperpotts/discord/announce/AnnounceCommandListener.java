package com.megalomaniac.pepperpotts.discord.announce;

import com.megalomaniac.pepperpotts.discord.command.AdminCommand;
import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Log4j2
@Component
public class AnnounceCommandListener implements MessageCreateListener {

	private final CommandValidator validator;
	private final AdminCommand command = AdminCommand.ANNOUNCE_ATTENDANCE;
	private final AnnounceService announceService;
	private final MessageSender messageSender;

	public AnnounceCommandListener(CommandValidator commandValidator, AnnounceService announceService, MessageSender messageSender) {
		this.validator = commandValidator;
		this.announceService = announceService;
		this.messageSender = messageSender;
	}


	@Override
	public void onMessageCreate(MessageCreateEvent event) {
		if (!validator.isValidCommand(event, command)) {
			return;
		}

		try {
			AnnouncementCommand announcementCommand = new AnnouncementCommand(event.getMessage());
			announceService.announceAttendance(announcementCommand);
		} catch (AnnouncementTypeNotFoundException e) {
			String validTypes = extractValidAnnouncementTypes();
			String errorMessage = "Invalid announcement type. Valid announcement types are: " + validTypes + ".";
			messageSender.sendErrorMessage(event.getMessage(), errorMessage);
		}
	}

	private String extractValidAnnouncementTypes() {
		return AnnouncementType.getValidTypes().stream()
						.map(type -> "`" + type.getName() + "` ")
						.collect(Collectors.toList())
						.toString();
	}
}
