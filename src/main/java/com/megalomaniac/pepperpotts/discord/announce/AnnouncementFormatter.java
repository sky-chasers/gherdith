package com.megalomaniac.pepperpotts.discord.announce;

import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class AnnouncementFormatter {

	private final AnnouncementChannels announcementChannels;

	public AnnouncementFormatter(AnnouncementChannels announcementChannels) {
		this.announcementChannels = announcementChannels;
	}

	public JSONObject formatAnnouncement(AnnouncementType type, String title, String message) {
		String channelId = announcementChannels.getChannelForType(type);

		JSONObject embedValue = new JSONObject();
		embedValue.put("color", "#4CAF50");
		embedValue.put("title", title);
		embedValue.put("description", message);

		JSONObject messageValue = new JSONObject();
		messageValue.put("embed", embedValue);

		JSONObject contents = new JSONObject();
		contents.put("channelId", channelId);
		contents.put("message", messageValue);

		return contents;
	}
}
