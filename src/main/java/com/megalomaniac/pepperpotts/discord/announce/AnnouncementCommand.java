package com.megalomaniac.pepperpotts.discord.announce;

import lombok.Getter;
import lombok.Setter;
import org.javacord.api.entity.message.Message;

import java.util.Optional;

public class AnnouncementCommand {

	@Getter
	@Setter
	private String command;

	@Getter
	@Setter
	private AnnouncementType announcementType;

	@Getter
	@Setter
	private String googleSheetId;

	@Getter
	@Setter
	private Message originalMessage;

	public AnnouncementCommand(Message message) throws AnnouncementTypeNotFoundException{
		String[] splitCommand = message.getContent().split(" ");
		Optional<AnnouncementType> foundType = AnnouncementType.findTypeByName(splitCommand[1]);

		if (!foundType.isPresent()) {
			throw new AnnouncementTypeNotFoundException();
		}

		this.command = splitCommand[0];
		this.announcementType = foundType.get();
		this.googleSheetId = splitCommand[2];
		this.originalMessage = message;
	}
}
