package com.megalomaniac.pepperpotts.discord.announce;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public enum AnnouncementType {

	BORUTA("boruta"),
	GILTINE("giltine"),
	GTW("gtw");

	@Getter
	private final String name;

	AnnouncementType(String name) {
		this.name = name;
	}

	public static Optional<AnnouncementType> findTypeByName(String name) {
		for(AnnouncementType type : values()){
			if(type.getName().equals(name)){
				return Optional.of(type);
			}
		}
		return Optional.empty();
	}

	public static List<AnnouncementType> getValidTypes() {
		return Arrays.asList(values());
	}
}
