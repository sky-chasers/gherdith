package com.megalomaniac.pepperpotts.discord.announce;

import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import com.megalomaniac.pepperpotts.ghervis.api.GhervisApi;
import com.megalomaniac.pepperpotts.google.sheets.SheetReader;
import com.megalomaniac.pepperpotts.google.sheets.SheetUtils;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Log4j2
@Service
public class AnnounceService {

	private final GhervisApi ghervisApi;
	private final AnnouncementFormatter formatter;
	private final SheetReader sheetReader;
	private final MessageSender messageSender;
	private final SheetUtils sheetUtils;

	public AnnounceService(GhervisApi ghervisApi,
						   AnnouncementFormatter formatter,
						   SheetReader sheetReader,
						   MessageSender messageSender,
						   SheetUtils sheetUtils) {
		this.ghervisApi = ghervisApi;
		this.formatter = formatter;
		this.sheetReader = sheetReader;
		this.messageSender = messageSender;
		this.sheetUtils = sheetUtils;
	}

	public void announceAttendance(AnnouncementCommand command) {
		try {
			AnnouncementType type = command.getAnnouncementType();
			SpreadsheetProperties properties = sheetReader.fetchGoogleSheetPropertiesWithId(command.getGoogleSheetId());
			String title = properties.getTitle();
			String url = sheetUtils.constructUrlWithId(command.getGoogleSheetId());

			JSONObject messageDetails = formatter.formatAnnouncement(type, title, url);
			ghervisApi.sendChannelMessage(messageDetails);
			messageSender.sendSuccessMessage(command.getOriginalMessage(),
					"Successfully announced message to " + type + " channel.");
		} catch (IOException e) {
			e.printStackTrace();
			String errorMessage = "Failed to fetch google sheets with id: " + command.getGoogleSheetId();
			messageSender.sendErrorMessage(command.getOriginalMessage(), errorMessage);
		}
	}

}
