package com.megalomaniac.pepperpotts.discord.announce;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Log4j2
@Service
public class AnnouncementChannels {

	@Value("${megalomaniac.pepperpotts.discord.channels.borutaAttendance}")
	private String borutaAttendanceChannel;

	@Value("${megalomaniac.pepperpotts.discord.channels.gtwAttendance}")
	private String gtwAttendanceChannel;

	@Value("${megalomaniac.pepperpotts.discord.channels.giltineAttendance}")
	private String giltineAttendanceChannel;

	private Map<AnnouncementType, String> channelsMap;

	public String getChannelForType(AnnouncementType type) {

		if (channelsMap == null) {
			channelsMap = new HashMap<>();
			channelsMap.put(AnnouncementType.BORUTA, borutaAttendanceChannel);
			channelsMap.put(AnnouncementType.GTW, gtwAttendanceChannel);
			channelsMap.put(AnnouncementType.GILTINE, giltineAttendanceChannel);
		}

		return channelsMap.get(type);
	}
}
