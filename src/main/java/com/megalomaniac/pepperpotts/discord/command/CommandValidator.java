package com.megalomaniac.pepperpotts.discord.command;

import com.megalomaniac.pepperpotts.db.generalcommand.CommandChannelPermissionRepository;
import com.megalomaniac.pepperpotts.discord.message.MessageDebugger;
import com.megalomaniac.pepperpotts.discord.user.UserService;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class CommandValidator {

	private final MessageDebugger logger;
	private final CommandHelper commandHelper;
	private final UserService userService;
	private final CommandChannelPermissionRepository allowedChannelRepository;

	public CommandValidator(MessageDebugger logger, CommandHelper commandHelper,
							UserService userService, CommandChannelPermissionRepository allowedChannelRepository) {
		this.logger = logger;
		this.commandHelper = commandHelper;
		this.userService = userService;
		this.allowedChannelRepository = allowedChannelRepository;
	}

	public boolean isValidCommand(MessageCreateEvent event, Command command) {
		Message message = event.getMessage();
		String rawCommand = message.getContent();
		Optional<User> user = message.getAuthor().asUser();
		TextChannel currentChannel = event.getChannel();

		if (!command.startsWithValidCommand(rawCommand)) {
			return false;
		}

		if (message.isPrivateMessage() && !command.isAllowedInPrivateChat()) {
			logger.debug(command.getCommandString() + " is not allowed in private chat.", message);
			return false;
		}

		if (!message.isPrivateMessage() && !command.isAllowedInPublicChat()) {
			logger.debug(command.getCommandString() + "is not allowed in public chat.", message);
			return false;
		}

		if (user.isEmpty()) {
			logger.debug("User is empty.", message);
			return false;
		}

		boolean isAdminCommand = AdminCommand
				.findCommandByCommandString(command.getCommandString())
				.isPresent();
		boolean isAdmin = userService.isAdmin(user.get());

		if (isAdminCommand && !isAdmin) {
			log.info("User: {} cannot use {} command. Only admins can use this command.",
					user.get().getDiscriminatedName(),
					command.getCommandString()
			);
			return false;
		}

		if (!command.hasCorrectArgumentCount(rawCommand)) {
			logger.debug("Command has invalid argument count.", message);
			commandHelper.sendSpecificInvalidCommandResponse(message, command);
			return false;
		}

		if (!message.isPrivateMessage() && !isAllowedInChannel(command, currentChannel)) {
			logger.debug(command.getCommandString() + " is not allowed in channel: " + currentChannel.getId(), message);
			return false;
		}

		logger.info(rawCommand, message.getId(), message.getAuthor().getId());
		return true;
	}

	private boolean isAllowedInChannel(Command command, TextChannel currentChannel) {
		List<Long> allowedChannels = allowedChannelRepository.findAllByCommand(command.getEnumString());

		if (allowedChannels.isEmpty()) {
			return false;
		}

		return allowedChannels.contains(currentChannel.getId());
	}

}
