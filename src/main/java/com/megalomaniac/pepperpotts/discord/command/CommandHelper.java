package com.megalomaniac.pepperpotts.discord.command;

import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import org.javacord.api.entity.message.Message;
import org.springframework.stereotype.Service;

@Service
public class CommandHelper {

	private final MessageSender messageSender;

	public CommandHelper(MessageSender messageSender) {
		this.messageSender = messageSender;
	}

	public void sendSpecificInvalidCommandResponse(Message message, Command command) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("The command you entered is invalid. Please follow this format: \n");
		stringBuilder.append("```");
		stringBuilder.append(command.getFormat());
		stringBuilder.append("```");
		stringBuilder.append("Example: \n");
		stringBuilder.append("```");
		stringBuilder.append(command.getExample());
		stringBuilder.append("```");

		messageSender.sendErrorMessage(message, stringBuilder.toString());
	}
}
