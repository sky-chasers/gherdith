package com.megalomaniac.pepperpotts.discord.command;

import java.util.Optional;

public enum AdminCommand implements Command {

	ANNOUNCE_ATTENDANCE() {
		@Override
		public String getDescription() {
			return "Announce attendance via Ghervis";
		}

		@Override
		public String getCommandString() {
			return "!announce";
		}

		@Override
		public String getExample() {
			return "!announce boruta my-google-sheet-id";
		}

		@Override
		public String getFormat() {
			return "!announce <type> <googleSheetId>";
		}

		@Override
		public String getEnumString() {
			return AdminCommand.ANNOUNCE_ATTENDANCE.getEnumString();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return false;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return true;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!announce");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 3;
		}
	},

	ADMIN_HELP() {
		@Override
		public String getDescription() {
			return "Returns a list of admin commands and their sample usages.";
		}

		@Override
		public String getCommandString() {
			return "!adminHelp";
		}

		@Override
		public String getExample() {
			return "!adminHelp";
		}

		@Override
		public String getFormat() {
			return "!adminHelp";
		}

		@Override
		public String getEnumString() {
			return AdminCommand.ADMIN_HELP.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return true;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return true;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!adminHelp");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 1;
		}
	},

	CALCULATE_GTW_SALARY() {
		@Override
		public String getDescription() {
			return "Calculates GTW Salary given a googleSheetId and total funds.";
		}

		@Override
		public String getCommandString() {
			return "!calculateGtwSalary";
		}

		@Override
		public String getExample() {
			return "!calculateGtwSalary ArvmZqLbEj4H4g2IAIlN7Nbro 12345678910";
		}

		@Override
		public String getFormat() {
			return "!calculateGtwSalary <googleSheetsId> <totalFunds>";
		}

		@Override
		public String getEnumString() {
			return AdminCommand.CALCULATE_GTW_SALARY.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return true;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return true;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!calculateGtwSalary");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 3;
		}
	},

	FILL_RAID_ATTENDANCE() {
		@Override
		public String getDescription() {
			return "Fills up raid attendance sheet.";
		}

		@Override
		public String getCommandString() {
			return "!attendance";
		}

		@Override
		public String getExample() {
			return "!attendance giltine 1IoA5y6_OjoGRRXZYntVZ9v2BBJ2u5eF-23ji9du kkaio99dje8440aaiid-ssjuh";
		}

		@Override
		public String getFormat() {
			return "!attendance <type> <googleSheetIdInput> <googleSheetIdOutput>";
		}

		@Override
		public String getEnumString() {
			return AdminCommand.FILL_RAID_ATTENDANCE.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return false;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return true;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!attendance");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 4;
		}
	},

	FILL_BORUTA_BONUS_POINTS() {
		@Override
		public String getDescription() {
			return "Fill boruta bonus points in google sheet.";
		}

		@Override
		public String getCommandString() {
			return "!fillbonus";
		}

		@Override
		public String getExample() {
			return "!fillbonus mucndpoq0w92nnsx";
		}

		@Override
		public String getFormat() {
			return "!fillbonus <borutaAttendanceSheetId>";
		}

		@Override
		public String getEnumString() {
			return AdminCommand.FILL_BORUTA_BONUS_POINTS.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return true;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return true;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!fillbonus");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 2;
		}
	},

	GENERATE_BOSS_BALITA_KEYS() {
		@Override
		public String getDescription() {
			return "Generate new keys for boss balita reporter.";
		}

		@Override
		public String getCommandString() {
			return "!generateKey";
		}

		@Override
		public String getExample() {
			return "!generateKey Ghervis 123456789";
		}

		@Override
		public String getFormat() {
			return "!generateKey <teamName> <discordId>";
		}

		@Override
		public String getEnumString() {
			return AdminCommand.GENERATE_BOSS_BALITA_KEYS.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return true;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return false;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!generateKey");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 3;
		}
	};

	public static Optional<AdminCommand> findCommandByCommandString(String name) {
		for(AdminCommand command : values()){
			if(command.getCommandString().equals(name)){
				return Optional.of(command);
			}
		}
		return Optional.empty();
	}
}
