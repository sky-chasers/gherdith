package com.megalomaniac.pepperpotts.discord.command;

public enum GeneralCommand implements Command {

	HELP() {
		@Override
		public String getDescription() {
			return "Shows the list of available commands.";
		}

		@Override
		public String getCommandString() {
			return "!help";
		}

		@Override
		public String getExample() {
			return "!help";
		}

		@Override
		public String getFormat() {
			return "!help";
		}

		@Override
		public String getEnumString() {
			return GeneralCommand.HELP.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return true;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return false;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!help");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 1;
		}
	},

	NEWS() {
		@Override
		public String getDescription() {
			return "Shows the latest news from Tree of Savior page.";
		}

		@Override
		public String getCommandString() {
			return "!news";
		}

		@Override
		public String getExample() {
			return "!news";
		}

		@Override
		public String getFormat() {
			return "!news";
		}

		@Override
		public String getEnumString() {
			return GeneralCommand.NEWS.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return true;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return false;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!news");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 1;
		}
	},

	WHO_IS_ACCOUNT_ID() {
		@Override
		public String getDescription() {
			return "Shows tosAccount info given an in game account id.";
		}

		@Override
		public String getCommandString() {
			return "!whoIsAccountId";
		}

		@Override
		public String getExample() {
			return "!whoIsAccountId 1234567";
		}

		@Override
		public String getFormat() {
			return "!whoIsAccountId <accountId>";
		}

		@Override
		public String getEnumString() {
			return GeneralCommand.WHO_IS_ACCOUNT_ID.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return true;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return false;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!whoIsAccountId");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 2;
		}
	},

	WHO_IS_TEAM_NAME() {
		@Override
		public String getDescription() {
			return "Shows tosAccount info given a team skill.";
		}

		@Override
		public String getCommandString() {
			return "!whoIs";
		}

		@Override
		public String getExample() {
			return "!whoIs Ghervis";
		}

		@Override
		public String getFormat() {
			return "!whoIs <teamName>";
		}

		@Override
		public String getEnumString() {
			return GeneralCommand.WHO_IS_TEAM_NAME.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return true;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return false;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!whoIs");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return command.split(" ").length == 2;
		}
	},

	TOME_FINDER() {
		@Override
		public String getDescription() {
			return "Returns the mystic tome book number of a given key word.";
		}

		@Override
		public String getCommandString() {
			return "!tome";
		}

		@Override
		public String getExample() {
			return "!tome Teleportation";
		}

		@Override
		public String getFormat() {
			return "!tome <keyword>";
		}

		@Override
		public String getEnumString() {
			return GeneralCommand.TOME_FINDER.name();
		}

		@Override
		public boolean isAllowedInPublicChat() {
			return true;
		}

		@Override
		public boolean isAllowedInPrivateChat() {
			return false;
		}

		@Override
		public boolean startsWithValidCommand(String command) {
			return command.split(" ")[0].equals("!tome");
		}

		@Override
		public boolean hasCorrectArgumentCount(String command) {
			return true;
		}
	}
}
