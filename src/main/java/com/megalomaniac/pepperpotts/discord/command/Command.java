package com.megalomaniac.pepperpotts.discord.command;

public interface Command {

	String getDescription();

	String getCommandString();

	String getExample();

	String getFormat();

	String getEnumString();

	boolean isAllowedInPublicChat();

	boolean isAllowedInPrivateChat();

	boolean startsWithValidCommand(String command);

	boolean hasCorrectArgumentCount(String command);

}
