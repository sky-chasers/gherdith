package com.megalomaniac.pepperpotts.discord.tos.autonews.ktest;

import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsFormatter;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.ImageSelector;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.NewsImage;
import com.megalomaniac.pepperpotts.discord.tos.news.NewsReplyFormatter;
import com.megalomaniac.pepperpotts.naver.papago.PapagoLanguage;
import com.megalomaniac.pepperpotts.naver.papago.PapagoTranslationApi;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import com.megalomaniac.pepperpotts.utils.PostRequestException;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;

@Service
@Log4j2
public class KtestAutoNewsFormatter implements AutoNewsFormatter {

	private final NewsReplyFormatter newsReplyFormatter;
	private final String ktestNewsBaseUrl;
	private final String ktestServerHomePageUrl;
	private final String ktestGeneralBulletinBoardUrl;
	private final String ktosDevBlogUrl;
	private final String tosIconUrl;
	private final ImageSelector imageSelector;
	private final PapagoTranslationApi papagoTranslationApi;

	public KtestAutoNewsFormatter(NewsReplyFormatter newsReplyFormatter, String ktestNewsBaseUrl,
								  String ktestServerHomePageUrl, String ktestGeneralBulletinBoardUrl,
								  String ktosDevBlogUrl, String tosIconUrl, ImageSelector imageSelector,
								  PapagoTranslationApi papagoTranslationApi) {
		this.newsReplyFormatter = newsReplyFormatter;
		this.ktestNewsBaseUrl = ktestNewsBaseUrl;
		this.ktestServerHomePageUrl = ktestServerHomePageUrl;
		this.ktestGeneralBulletinBoardUrl = ktestGeneralBulletinBoardUrl;
		this.ktosDevBlogUrl = ktosDevBlogUrl;
		this.tosIconUrl = tosIconUrl;
		this.imageSelector = imageSelector;
		this.papagoTranslationApi = papagoTranslationApi;
	}

	public EmbedBuilder format(List<TosNews> tosNewsList) {
		NewsImage newsImage = imageSelector.getRandomImageUrl();

		EmbedBuilder formattedNews = new EmbedBuilder()
				.setColor(Color.RED)
				.setDescription(createDescription())
				.setAuthor("KTest Latest News")
				.setUrl(ktestNewsBaseUrl)
				.setFooter("IMC Games")
				.setImage(newsImage.getImgurSrc())
				.setThumbnail(tosIconUrl);

		StringBuilder fieldContent = new StringBuilder();

		for (TosNews news : tosNewsList) {
			fieldContent.append(translateTitleWithUrl(news));
			fieldContent.append("\n");
		}

		fieldContent.append("\n **[Image Source](" + newsImage.getActualImgSrc() + ")**");
		formattedNews.addField("What's :new:", fieldContent.toString());

		return formattedNews;
	}

	private String translateTitleWithUrl(TosNews news) {
		String finalTitle = news.getTitle();

		try {
			finalTitle = papagoTranslationApi.translate(PapagoLanguage.KOREAN, PapagoLanguage.ENGLISH, news.getTitle());
		} catch (PostRequestException e) {
			log.info("Failed to translate: {}. The news will not be translated.", news.getTitle());
		}

		return "• " + newsReplyFormatter.createLinkWithLabel(finalTitle, news.getUrl());
	}

	private String createDescription() {
		String[] urls = {
			newsReplyFormatter.createLinkWithLabel("Ktest Homepage", ktestServerHomePageUrl),
			newsReplyFormatter.createLinkWithLabel("General Bulletin Board", ktestGeneralBulletinBoardUrl),
			newsReplyFormatter.createLinkWithLabel("Notice Board", ktestNewsBaseUrl),
			newsReplyFormatter.createLinkWithLabel("Dev Blog", ktosDevBlogUrl)
		};

		return String.join(" | ", urls);
	}
}
