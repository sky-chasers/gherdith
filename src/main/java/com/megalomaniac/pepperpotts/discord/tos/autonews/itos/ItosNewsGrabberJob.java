package com.megalomaniac.pepperpotts.discord.tos.autonews.itos;


import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNews;
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNewsRepository;
import com.megalomaniac.pepperpotts.db.archivednews.NewsSource;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsService;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi;
import com.megalomaniac.pepperpotts.utils.GetRequestException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Component
public class ItosNewsGrabberJob {

	@Value("${megalomaniac.pepperpotts.discord.channels.news}")
	private String NEWS_CHANNEL_ID;

	private final static String EVERY_MINUTE = "0 * * * * *";

	private final TosNewsApi tosNewsApi;
	private final ArchivedNewsRepository archivedNewsRepository;
	private final AutoNewsService itosAutoNewsService;

	public ItosNewsGrabberJob(TosNewsApi tosNewsApi, ArchivedNewsRepository archivedNewsRepository, AutoNewsService itosAutoNewsService) {
		this.tosNewsApi = tosNewsApi;
		this.archivedNewsRepository = archivedNewsRepository;
		this.itosAutoNewsService = itosAutoNewsService;
	}

	@Scheduled(cron = EVERY_MINUTE)
	protected void fetchAndPublishLatestNews() {
		try {
			List<TosNews> frontPageNews = tosNewsApi.fetchAllItosNews();
			List<TosNews> newsToPublish = new ArrayList<>();

			frontPageNews.forEach(news -> {
				Optional<ArchivedNews> lastWeeksNews = archivedNewsRepository.findByUrlAndNewsSource(news.getUrl(), NewsSource.ITOS);

				if (lastWeeksNews.isEmpty()) {
					newsToPublish.add(news);
				}
			});

			if (!newsToPublish.isEmpty()) {
				itosAutoNewsService.publishLatestNews(newsToPublish, NewsSource.ITOS, NEWS_CHANNEL_ID);
				itosAutoNewsService.updateNewsArchive(frontPageNews, NewsSource.ITOS);
			}

			itosAutoNewsService.deleteStaleNews(frontPageNews, NewsSource.ITOS);

		} catch (GetRequestException e) {
			log.error("Unable to fetch latest news and updates from iTOS news page. Ended with error: {}", e.getMessage());
		}
	}
}
