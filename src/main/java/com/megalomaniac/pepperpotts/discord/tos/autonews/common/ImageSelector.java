package com.megalomaniac.pepperpotts.discord.tos.autonews.common;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImageSelector {

	private final List<NewsImage> imageList;

	public ImageSelector() {
		this.imageList = new ArrayList<>();
		this.imageList.add(new NewsImage("https://i.imgur.com/Zc8J49z.png", "http://www.inven.co.kr/board/tos/4186/1445?p=3"));
		this.imageList.add(new NewsImage("https://i.imgur.com/UlJ0tNy.png", "http://www.inven.co.kr/board/tos/4186/1043?p=17"));
		this.imageList.add(new NewsImage("https://i.imgur.com/ezF5Rdl.png", "http://www.inven.co.kr/board/tos/4186/1053?p=17"));
		this.imageList.add(new NewsImage("https://i.imgur.com/Cdv7SW7.png", "http://www.inven.co.kr/board/tos/4186/656?p=31"));
		this.imageList.add(new NewsImage("https://i.imgur.com/TlMWWKm.png", "http://www.inven.co.kr/board/tos/4186/348?p=44"));
		this.imageList.add(new NewsImage("https://i.imgur.com/7RXy3W3.png", "http://www.inven.co.kr/board/tos/4186/342?p=44"));
		this.imageList.add(new NewsImage("https://i.imgur.com/aUu9zRn.png", "https://www.pixiv.net/en/artworks/58877183"));
		this.imageList.add(new NewsImage("https://i.imgur.com/tfHewph.png", "https://www.pixiv.net/en/artworks/89782406"));
		this.imageList.add(new NewsImage("https://i.imgur.com/1ZrVYDW.png", "https://www.pixiv.net/en/artworks/82268633"));
		this.imageList.add(new NewsImage("https://i.imgur.com/lapvGnH.png", "https://www.pixiv.net/en/artworks/66945272"));
		this.imageList.add(new NewsImage("https://i.imgur.com/eIRQyBh.png", "https://www.pixiv.net/en/artworks/66945248"));
		this.imageList.add(new NewsImage("https://i.imgur.com/BNQhunV.png", "https://www.pixiv.net/en/artworks/65801820"));
		this.imageList.add(new NewsImage("https://i.imgur.com/bcXcpYf.png", "https://www.pixiv.net/en/artworks/58462283"));
		this.imageList.add(new NewsImage("https://i.imgur.com/20Xbg3A.png", "https://www.pixiv.net/en/artworks/59175452"));
		this.imageList.add(new NewsImage("https://i.imgur.com/iWSV2pU.png", "https://www.pixiv.net/en/artworks/65052569"));
		this.imageList.add(new NewsImage("https://i.imgur.com/fZuO7Hq.png", "https://www.pixiv.net/en/artworks/73004899"));
		this.imageList.add(new NewsImage("https://i.imgur.com/LU1X085.png", "https://www.pixiv.net/en/artworks/69752103"));
		this.imageList.add(new NewsImage("https://i.imgur.com/qAHkTrk.png", "https://www.pixiv.net/en/artworks/65801400"));
		this.imageList.add(new NewsImage("https://i.imgur.com/cimx9ho.png", "https://www.pixiv.net/en/artworks/64859510"));
		this.imageList.add(new NewsImage("https://i.imgur.com/9tlARTq.png", "https://www.pixiv.net/en/artworks/79488306"));
		this.imageList.add(new NewsImage("https://i.imgur.com/eqqvxES.png", "https://www.pixiv.net/en/artworks/71466119"));
	}

	public NewsImage getRandomImageUrl() {
		return imageList.get(generateRandomNumber());
	}

	private int generateRandomNumber() {
		int min = 0;
		int max = imageList.size() - 1;

		return (int)Math.floor(Math.random()*(max-min+1)+min);
	}
}
