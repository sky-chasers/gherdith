package com.megalomaniac.pepperpotts.discord.tos.autonews.common;

import com.megalomaniac.pepperpotts.db.archivednews.NewsSource;
import com.megalomaniac.pepperpotts.discord.tos.autonews.itos.ItosAutoNewsFormatter;
import com.megalomaniac.pepperpotts.discord.tos.autonews.ktest.KtestAutoNewsFormatter;
import com.megalomaniac.pepperpotts.discord.tos.autonews.ktos.KtosAutoNewsFormatter;
import com.megalomaniac.pepperpotts.discord.tos.autonews.ktosdevblog.KtosDevBlogFormatter;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class FormatPicker {

	private final Map<NewsSource, AutoNewsFormatter> map;

	public FormatPicker(KtosAutoNewsFormatter ktosAutoNewsFormatter,
						ItosAutoNewsFormatter itosAutoNewsFormatter,
						KtestAutoNewsFormatter ktestAutoNewsFormatter,
						KtosDevBlogFormatter devBlogFormatter) {
		map = new HashMap<>();
		map.put(NewsSource.KTOS, ktosAutoNewsFormatter);
		map.put(NewsSource.KTEST, ktestAutoNewsFormatter);
		map.put(NewsSource.KTOS_DEVBLOG, devBlogFormatter);
		map.put(NewsSource.ITOS, itosAutoNewsFormatter);
	}

	public AutoNewsFormatter fetchFormatterForNewsSource(NewsSource newsSource) {
		return map.get(newsSource);
	}
}
