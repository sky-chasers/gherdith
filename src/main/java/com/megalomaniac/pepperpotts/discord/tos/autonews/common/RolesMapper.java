package com.megalomaniac.pepperpotts.discord.tos.autonews.common;

import com.megalomaniac.pepperpotts.db.archivednews.NewsSource;
import org.hibernate.validator.internal.util.privilegedactions.NewSchema;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class RolesMapper {

    private Map<NewsSource,String> map;

//    private final String itosRoleId;
//    private final String ktosRoleId;
//    private final String ktosdevblogRoleId;
//    private final String ktestRoleId;


    public RolesMapper(String itosRoleId, String ktosRoleId, String ktosdevblogRoleId, String ktestRoleId) {
        this.map = new HashMap<>();

        this.map.put(NewsSource.ITOS, itosRoleId);
        this.map.put(NewsSource.KTOS, ktosRoleId);
        this.map.put(NewsSource.KTOS_DEVBLOG, ktosdevblogRoleId);
        this.map.put(NewsSource.KTEST, ktestRoleId);
    }

    public String getRoleId(NewsSource newsSource) {
        return this.map.get(newsSource);
    }
}
