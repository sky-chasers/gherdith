package com.megalomaniac.pepperpotts.discord.tos.autonews.common;

import lombok.Getter;

public class NewsImage {

	@Getter
	private String imgurSrc;

	@Getter
	private String actualImgSrc;

	public NewsImage(String imgurSrc, String actualImgSrc) {
		this.imgurSrc = imgurSrc;
		this.actualImgSrc = actualImgSrc;
	}
}
