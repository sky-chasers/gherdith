package com.megalomaniac.pepperpotts.discord.tos.autonews.ktos;

import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsFormatter;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.ImageSelector;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.NewsImage;
import com.megalomaniac.pepperpotts.discord.tos.news.NewsReplyFormatter;
import com.megalomaniac.pepperpotts.naver.papago.PapagoLanguage;
import com.megalomaniac.pepperpotts.naver.papago.PapagoTranslationApi;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import com.megalomaniac.pepperpotts.utils.PostRequestException;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;

@Log4j2
@Service
public class KtosAutoNewsFormatter implements AutoNewsFormatter {

	private final String ktosNewsPageUrl;
	private final String tosIconUrl;
	private final String ktosUpdatesUrl;
	private final String ktosEventPageUrl;
	private final String ktosHomePageUrl;
	private final String ktosDevBlogUrl;
	private final NewsReplyFormatter newsReplyFormatter;
	private final ImageSelector imageSelector;
	private final PapagoTranslationApi papagoTranslationApi;

	public KtosAutoNewsFormatter(String ktosNewsPageUrl, String tosIconUrl, String ktosUpdatesUrl, String ktosEventPageUrl,
								 String ktosHomePageUrl, String ktosDevBlogUrl, NewsReplyFormatter newsReplyFormatter,
								 ImageSelector imageSelector, PapagoTranslationApi papagoTranslationApi) {
		this.ktosNewsPageUrl = ktosNewsPageUrl;
		this.tosIconUrl = tosIconUrl;
		this.ktosUpdatesUrl = ktosUpdatesUrl;
		this.ktosEventPageUrl = ktosEventPageUrl;
		this.ktosHomePageUrl = ktosHomePageUrl;
		this.ktosDevBlogUrl = ktosDevBlogUrl;
		this.newsReplyFormatter = newsReplyFormatter;
		this.imageSelector = imageSelector;
		this.papagoTranslationApi = papagoTranslationApi;
	}

	public EmbedBuilder format(List<TosNews> tosNewsList) {
		NewsImage newsImage = imageSelector.getRandomImageUrl();

		EmbedBuilder formattedNews = new EmbedBuilder()
				.setColor(Color.PINK)
				.setDescription(createDescription())
				.setAuthor("KTOS Latest News")
				.setUrl(ktosNewsPageUrl)
				.setFooter("IMC Games")
				.setImage(newsImage.getImgurSrc())
				.setThumbnail(tosIconUrl);

		StringBuilder fieldContent = new StringBuilder();

		for (TosNews news : tosNewsList) {
			fieldContent.append(createTitleWithUrl(news));
			fieldContent.append("\n");
		}

		fieldContent.append("\n **[Image Source](" + newsImage.getActualImgSrc() + ")**");

		formattedNews.addField("What's :new: ", fieldContent.toString());

		return formattedNews;
	}

	private String createTitleWithUrl(TosNews news) {
		String finalTitle = news.getTitle();

		try {
			finalTitle = papagoTranslationApi.translate(PapagoLanguage.KOREAN, PapagoLanguage.ENGLISH, news.getTitle());
		} catch (PostRequestException e) {
			log.info("Failed to translate: {}. The news will not be translated.", news.getTitle());
		}

		return "• " + newsReplyFormatter.createLinkWithLabel(finalTitle, news.getUrl());
	}

	private String createDescription() {
		String[] urls = {
				newsReplyFormatter.createLinkWithLabel("Home", ktosHomePageUrl),
				newsReplyFormatter.createLinkWithLabel("Notice", ktosNewsPageUrl),
				newsReplyFormatter.createLinkWithLabel("Updates", ktosUpdatesUrl),
				newsReplyFormatter.createLinkWithLabel("Events", ktosEventPageUrl),
				newsReplyFormatter.createLinkWithLabel("Dev Blog", ktosDevBlogUrl)
		};

		return String.join(" | ", urls);
	}
}
