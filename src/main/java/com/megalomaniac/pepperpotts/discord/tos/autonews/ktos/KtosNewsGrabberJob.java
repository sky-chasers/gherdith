package com.megalomaniac.pepperpotts.discord.tos.autonews.ktos;

import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNews;
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNewsRepository;
import com.megalomaniac.pepperpotts.db.archivednews.NewsSource;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsService;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi;
import com.megalomaniac.pepperpotts.utils.GetRequestException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Log4j2
public class KtosNewsGrabberJob {

	@Value("${megalomaniac.pepperpotts.discord.channels.ktosnews}")
	private String NEWS_CHANNEL_ID;

	private final static String EVERY_MINUTE = "0 * * * * *";

	private final TosNewsApi tosNewsApi;
	private final ArchivedNewsRepository archivedNewsRepository;
	private final AutoNewsService autoNewsService;

	public KtosNewsGrabberJob(TosNewsApi tosNewsApi,
							  ArchivedNewsRepository archivedNewsRepository,
							  AutoNewsService autoNewsService) {
		this.tosNewsApi = tosNewsApi;
		this.archivedNewsRepository = archivedNewsRepository;
		this.autoNewsService = autoNewsService;
	}

	@Scheduled(cron = EVERY_MINUTE)
	protected void fetchAndPublishLatestNews() {
		try {
			List<TosNews> frontPageNewsList = tosNewsApi.fetchAllKtosNews();
			List<TosNews> newsToPublish = new ArrayList<>();

			for (TosNews frontPageNews : frontPageNewsList) {
				Optional<ArchivedNews> lastWeekNews = archivedNewsRepository.findByUrlAndNewsSource(frontPageNews.getUrl(), NewsSource.KTOS);

				if (lastWeekNews.isEmpty()) {
					newsToPublish.add(frontPageNews);
				}
			}

			if (!newsToPublish.isEmpty()) {
				autoNewsService.publishLatestNews(newsToPublish, NewsSource.KTOS, NEWS_CHANNEL_ID);
				autoNewsService.updateNewsArchive(newsToPublish, NewsSource.KTOS);
			}

			autoNewsService.deleteStaleNews(frontPageNewsList,NewsSource.KTOS);
		} catch (GetRequestException e) {
			log.error("Unable to fetch latest news and updates from Ktos news page. Ended with error: {}", e.getMessage());
		}
	}
}
