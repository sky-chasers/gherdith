package com.megalomaniac.pepperpotts.discord.tos.news;

import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import com.megalomaniac.pepperpotts.discord.command.GeneralCommand;
import org.javacord.api.entity.message.Message;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

@Component
public class NewsGrabberListener implements MessageCreateListener {

	private final GeneralCommand command = GeneralCommand.NEWS;
	private final CommandValidator validator;
	private final NewsGrabber newsGrabber;

	public NewsGrabberListener(CommandValidator validator, NewsGrabber newsGrabber) {
		this.validator = validator;
		this.newsGrabber = newsGrabber;
	}

	@Override
	public void onMessageCreate(MessageCreateEvent event) {
		Message message = event.getMessage();

		if(!validator.isValidCommand(event, command)) {
			return;
		}

		newsGrabber.respondWithLatestNews(message);
	}

}
