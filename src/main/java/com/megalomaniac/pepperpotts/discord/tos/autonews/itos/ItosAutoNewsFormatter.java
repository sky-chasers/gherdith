package com.megalomaniac.pepperpotts.discord.tos.autonews.itos;

import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsFormatter;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.ImageSelector;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.NewsImage;
import com.megalomaniac.pepperpotts.discord.tos.news.NewsReplyFormatter;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;

@Log4j2
@Service
public class ItosAutoNewsFormatter implements AutoNewsFormatter {

	private final NewsReplyFormatter newsReplyFormatter;
	private final String tosIconUrl;
	private final ImageSelector imageSelector;
	private final String tosAllNewsBaseUrl;

	public ItosAutoNewsFormatter(NewsReplyFormatter newsReplyFormatter, String tosIconUrl, ImageSelector imageSelector,
								 String tosAllNewsBaseUrl) {
		this.newsReplyFormatter = newsReplyFormatter;
		this.tosIconUrl = tosIconUrl;
		this.imageSelector = imageSelector;
		this.tosAllNewsBaseUrl = tosAllNewsBaseUrl;
	}

	public EmbedBuilder format(List<TosNews> tosNewsList) {
		NewsImage newsImage = imageSelector.getRandomImageUrl();

		EmbedBuilder embedBuilder = new EmbedBuilder()
				.setColor(Color.CYAN)
				.setDescription(newsReplyFormatter.createDescription())
				.setAuthor("Tree of Savior Latest News")
				.setUrl(tosAllNewsBaseUrl)
				.setFooter("IMC Games")
				.setImage(newsImage.getImgurSrc())
				.setThumbnail(tosIconUrl);

		StringBuilder fieldContent = new StringBuilder();

		tosNewsList.forEach(news -> {
			String title = "• " + newsReplyFormatter.createLinkWithLabel(news.getTitle(), news.getUrl());
			fieldContent.append(title);
			fieldContent.append("\n");
		});

		fieldContent.append("\n **[Image Source](" + newsImage.getActualImgSrc() + ")**");

		embedBuilder.addField("What's :new:", fieldContent.toString());

		return embedBuilder;
	}

}
