package com.megalomaniac.pepperpotts.discord.tos.news;

import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi;
import com.megalomaniac.pepperpotts.utils.GetRequestException;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j2
@Service
public class NewsGrabber {

	private final MessageSender messageSender;
	private final NewsReplyFormatter newsReplyFormatter;
	private final TosNewsApi tosNewsApi;

	public NewsGrabber(MessageSender messageSender,
					   NewsReplyFormatter newsReplyFormatter,
					   TosNewsApi tosNewsApi) {
		this.messageSender = messageSender;
		this.newsReplyFormatter = newsReplyFormatter;
		this.tosNewsApi = tosNewsApi;
	}

	void respondWithLatestNews(Message messageFromUser) {

		try {
			List<TosNews> tosNews = tosNewsApi.fetchAllItosNews();
			EmbedBuilder response = newsReplyFormatter.format(tosNews);
			messageSender.sendMessage(messageFromUser, response);
		} catch (GetRequestException e) {
			messageSender.sendErrorMessage(messageFromUser, "Sorry, I encountered an error while fetching the news. Please try again later.");
		}

	}
}
