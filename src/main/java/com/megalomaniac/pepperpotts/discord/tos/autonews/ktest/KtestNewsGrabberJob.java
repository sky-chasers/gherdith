package com.megalomaniac.pepperpotts.discord.tos.autonews.ktest;

import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNews;
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNewsRepository;
import com.megalomaniac.pepperpotts.db.archivednews.NewsSource;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsService;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi;
import com.megalomaniac.pepperpotts.utils.GetRequestException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Component
public class KtestNewsGrabberJob {

	@Value("${megalomaniac.pepperpotts.discord.channels.ktestnews}")
	private String NEWS_CHANNEL_ID;

	private final static String EVERY_MINUTE = "0 * * * * *";

	private final TosNewsApi tosNewsApi;
	private final AutoNewsService autoNewsService;
	private final ArchivedNewsRepository archivedNewsRepository;

	public KtestNewsGrabberJob(TosNewsApi tosNewsApi, AutoNewsService autoNewsService, ArchivedNewsRepository archivedNewsRepository) {
		this.tosNewsApi = tosNewsApi;
		this.autoNewsService = autoNewsService;
		this.archivedNewsRepository = archivedNewsRepository;
	}

	@Scheduled(cron = EVERY_MINUTE)
	protected void fetchAndPublishLatestNews() {
		try {
			List<TosNews> frontPageNews = tosNewsApi.fetchAllKtestNews();
			List<TosNews> newsToPublish = new ArrayList<>();

			frontPageNews.forEach(news -> {
				Optional<ArchivedNews> lastWeeksNews = archivedNewsRepository.findByUrlAndNewsSource(news.getUrl(), NewsSource.KTEST);

				if (lastWeeksNews.isEmpty()) {
					newsToPublish.add(news);
				}
			});

			if (!newsToPublish.isEmpty()) {
				autoNewsService.publishLatestNews(newsToPublish, NewsSource.KTEST, NEWS_CHANNEL_ID);
				autoNewsService.updateNewsArchive(frontPageNews, NewsSource.KTEST);
			}

			autoNewsService.deleteStaleNews(frontPageNews, NewsSource.KTEST);

		} catch (GetRequestException e) {
			log.error("Unable to fetch latest news and updates from kTest news page. Ended with error: {}", e.getMessage());
		}
	}
}
