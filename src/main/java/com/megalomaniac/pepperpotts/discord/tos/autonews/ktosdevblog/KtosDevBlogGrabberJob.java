package com.megalomaniac.pepperpotts.discord.tos.autonews.ktosdevblog;

import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNews;
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNewsRepository;
import com.megalomaniac.pepperpotts.db.archivednews.NewsSource;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsService;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import com.megalomaniac.pepperpotts.tos.news.TosNewsApi;
import com.megalomaniac.pepperpotts.utils.GetRequestException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Component
public class KtosDevBlogGrabberJob {

	@Value("${megalomaniac.pepperpotts.discord.channels.ktosnews}")
	private String NEWS_CHANNEL_ID;

	private final static String EVERY_MINUTE = "0 * * * * *";

	private final TosNewsApi tosNewsApi;
	private final AutoNewsService ktosAutoNewsService;
	private final ArchivedNewsRepository archivedNewsRepository;

	public KtosDevBlogGrabberJob(TosNewsApi tosNewsApi,
								 AutoNewsService ktosAutoNewsService,
								 ArchivedNewsRepository archivedNewsRepository) {
		this.tosNewsApi = tosNewsApi;
		this.ktosAutoNewsService = ktosAutoNewsService;
		this.archivedNewsRepository = archivedNewsRepository;
	}

	@Scheduled(cron = EVERY_MINUTE)
	protected void fetchAndPublishLatestDevBlogs() {
		try {
			List<TosNews> frontPageNewsList = tosNewsApi.fetchAllDevBlogs();
			List<TosNews> newsToPublish = new ArrayList<>();

			for (TosNews frontPageNews : frontPageNewsList) {
				Optional<ArchivedNews> lastWeekNews = archivedNewsRepository.findByUrlAndNewsSource(frontPageNews.getUrl(), NewsSource.KTOS_DEVBLOG);

				if (lastWeekNews.isEmpty()) {
					newsToPublish.add(frontPageNews);
				}
			}

			if (!newsToPublish.isEmpty()) {
				ktosAutoNewsService.publishLatestNews(newsToPublish, NewsSource.KTOS_DEVBLOG, NEWS_CHANNEL_ID);
				ktosAutoNewsService.updateNewsArchive(newsToPublish, NewsSource.KTOS_DEVBLOG);
			}

			ktosAutoNewsService.deleteStaleNews(frontPageNewsList, NewsSource.KTOS_DEVBLOG);
		} catch (GetRequestException e) {
			e.printStackTrace();
		}
	}
}
