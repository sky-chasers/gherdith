package com.megalomaniac.pepperpotts.discord.tos.autonews.ktosdevblog;

import com.megalomaniac.pepperpotts.discord.tos.autonews.common.AutoNewsFormatter;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.ImageSelector;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.NewsImage;
import com.megalomaniac.pepperpotts.discord.tos.news.NewsReplyFormatter;
import com.megalomaniac.pepperpotts.naver.papago.PapagoLanguage;
import com.megalomaniac.pepperpotts.naver.papago.PapagoTranslationApi;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import com.megalomaniac.pepperpotts.utils.PostRequestException;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;

@Service
@Log4j2
public class KtosDevBlogFormatter implements AutoNewsFormatter {

	private final NewsReplyFormatter newsReplyFormatter;
	private final String ktosNewsPageUrl;
	private final String tosIconUrl;
	private final String ktosCommunityBoard;
	private final String ktosFeatureUrl;
	private final String ktosDevBlogUrl;
	private final ImageSelector imageSelector;
	private final PapagoTranslationApi papagoTranslationApi;

	public KtosDevBlogFormatter(NewsReplyFormatter newsReplyFormatter, String ktosNewsPageUrl, String tosIconUrl,
								String ktosCommunityBoard, String ktosFeatureUrl,
								String ktosDevBlogUrl, ImageSelector imageSelector,
								PapagoTranslationApi papagoTranslationApi) {
		this.newsReplyFormatter = newsReplyFormatter;
		this.ktosNewsPageUrl = ktosNewsPageUrl;
		this.tosIconUrl = tosIconUrl;
		this.ktosCommunityBoard = ktosCommunityBoard;
		this.ktosFeatureUrl = ktosFeatureUrl;
		this.ktosDevBlogUrl = ktosDevBlogUrl;
		this.imageSelector = imageSelector;
		this.papagoTranslationApi = papagoTranslationApi;
	}

	public EmbedBuilder format(List<TosNews> tosNewsList) {
		NewsImage newsImage = imageSelector.getRandomImageUrl();

		EmbedBuilder formattedNews = new EmbedBuilder()
				.setColor(Color.GREEN)
				.setDescription(createDescription())
				.setAuthor("KTOS DevBlog")
				.setUrl(ktosNewsPageUrl)
				.setFooter("IMC Games")
				.setImage(newsImage.getImgurSrc())
				.setThumbnail(tosIconUrl);

		StringBuilder fieldContent = new StringBuilder();

		for (TosNews news : tosNewsList) {
			fieldContent.append(createTitleWithUrl(news));
			fieldContent.append("\n");
		}

		fieldContent.append("\n **[Image Source](" + newsImage.getActualImgSrc() + ")**");

		formattedNews.addField("What's :new:", fieldContent.toString());

		return formattedNews;
	}

	private String createTitleWithUrl(TosNews news) {
		String finalTitle = news.getTitle();

		try {
			finalTitle = papagoTranslationApi.translate(PapagoLanguage.KOREAN, PapagoLanguage.ENGLISH, news.getTitle());
		} catch (PostRequestException e) {
			log.info("Failed to translate: {}. The news will not be translated.", news.getTitle());
		}

		return "• " + newsReplyFormatter.createLinkWithLabel(finalTitle, news.getUrl());
	}

	private String createDescription() {
		String[] urls = {
				newsReplyFormatter.createLinkWithLabel("Notice", ktosNewsPageUrl),
				newsReplyFormatter.createLinkWithLabel("Feature", ktosFeatureUrl),
				newsReplyFormatter.createLinkWithLabel("Community Board", ktosCommunityBoard),
				newsReplyFormatter.createLinkWithLabel("Dev Blog", ktosDevBlogUrl)
		};

		return String.join(" | ", urls);
	}
}
