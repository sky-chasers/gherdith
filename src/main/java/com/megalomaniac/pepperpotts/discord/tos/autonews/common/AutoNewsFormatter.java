package com.megalomaniac.pepperpotts.discord.tos.autonews.common;

import com.megalomaniac.pepperpotts.tos.news.TosNews;
import org.javacord.api.entity.message.embed.EmbedBuilder;

import java.util.List;

public interface AutoNewsFormatter {

	public EmbedBuilder format(List<TosNews> tosNewsList);

}
