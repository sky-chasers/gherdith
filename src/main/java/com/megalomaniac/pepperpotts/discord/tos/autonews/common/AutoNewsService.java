package com.megalomaniac.pepperpotts.discord.tos.autonews.common;

import com.google.common.collect.Lists;
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNews;
import com.megalomaniac.pepperpotts.db.archivednews.ArchivedNewsRepository;
import com.megalomaniac.pepperpotts.db.archivednews.NewsSource;
import com.megalomaniac.pepperpotts.discord.message.PublicMessageSender;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.channel.TextChannel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service
public class AutoNewsService {

	private final DiscordApi discordApi;
	private final PublicMessageSender publicMessageSender;
	private final FormatPicker formatPicker;
	private final ArchivedNewsRepository archivedNewsRepository;
	private final RolesMapper rolesMapper;


	public AutoNewsService(DiscordApi discordApi,
						   PublicMessageSender publicMessageSender,
						   FormatPicker formatPicker,
						   ArchivedNewsRepository archivedNewsRepository,
						   RolesMapper rolesMapper) {
		this.discordApi = discordApi;
		this.publicMessageSender = publicMessageSender;
		this.formatPicker = formatPicker;
		this.archivedNewsRepository = archivedNewsRepository;
		this.rolesMapper = rolesMapper;
	}

	public void publishLatestNews(List<TosNews> newsToPublish, NewsSource newsSource, String channelId) {
		AutoNewsFormatter formatter = formatPicker.fetchFormatterForNewsSource(newsSource);
		List<List<TosNews>> formattedNewsList =  Lists.partition(newsToPublish, 5);
		Optional<TextChannel> newsChannel = discordApi.getTextChannelById(channelId);

		if (newsChannel.isPresent()) {
			for (List<TosNews> formattedNews : formattedNewsList) {
				publicMessageSender.sendMessage(newsChannel.get(), getNewsContent(newsSource), formatter.format(formattedNews));
			}
		}
	}

	public void updateNewsArchive(List<TosNews> latestNews, NewsSource newsSource) {
		for (TosNews latestNew : latestNews) {
			Optional<ArchivedNews> existingNews = archivedNewsRepository.findByUrlAndNewsSource(latestNew.getUrl(), newsSource);

			if (existingNews.isEmpty()) {
				ArchivedNews archivedNews = new ArchivedNews();
				archivedNews.setUrl(latestNew.getUrl());
				archivedNews.setDatePublishedByImc(latestNew.getDatePublished());
				archivedNews.setNewsSource(newsSource);

				archivedNewsRepository.save(archivedNews);
			}
		}
	}

	public void deleteStaleNews(List<TosNews> frontPageNews, NewsSource newsSource) {
		List<String> excludedUrls = frontPageNews.stream().map(TosNews::getUrl).collect(Collectors.toList());
		List<ArchivedNews> newsToDelete = archivedNewsRepository.findAllByNewsSourceAndUrlNotIn(newsSource, excludedUrls);

		log.info("Deleting {} archived news...", newsToDelete.size());
		archivedNewsRepository.deleteAll(newsToDelete);
	}

	private String getNewsContent(final NewsSource newsSource) {
		return String.format("<@&%s>", rolesMapper.getRoleId(newsSource));
	}
}
