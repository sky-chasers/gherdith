package com.megalomaniac.pepperpotts.discord.tos.news;

import com.megalomaniac.pepperpotts.discord.tos.autonews.common.ImageSelector;
import com.megalomaniac.pepperpotts.discord.tos.autonews.common.NewsImage;
import com.megalomaniac.pepperpotts.tos.news.TosNews;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.util.List;

@Log4j2
@Service
public class NewsReplyFormatter {

	private final String tosAnnouncementsUrl;
	private final String tosAllNewsBaseUrl;
	private final String tosEventsUrl;
	private final String tosPatchUrl;
	private final String tosBlogUrl;
	private final String tosIssuesUrl;
	private final String tosIconUrl;
	private final ImageSelector imageSelector;

	public NewsReplyFormatter(String tosAnnouncementsUrl,
							  String tosAllNewsBaseUrl,
							  String tosEventsUrl,
							  String tosPatchUrl,
							  String tosBlogUrl,
							  String tosIssuesUrl,
							  String tosIconUrl,
							  ImageSelector imageSelector) {
		this.tosAnnouncementsUrl = tosAnnouncementsUrl;
		this.tosAllNewsBaseUrl = tosAllNewsBaseUrl;
		this.tosEventsUrl = tosEventsUrl;
		this.tosPatchUrl = tosPatchUrl;
		this.tosBlogUrl = tosBlogUrl;
		this.tosIssuesUrl = tosIssuesUrl;
		this.tosIconUrl = tosIconUrl;
		this.imageSelector = imageSelector;
	}

	public EmbedBuilder format(List<TosNews> appNews) {
		NewsImage newsImage = imageSelector.getRandomImageUrl();

		EmbedBuilder embedBuilder = new EmbedBuilder()
				.setColor(Color.PINK)
				.setDescription(createDescription())
				.setAuthor("Tree of Savior News")
				.setUrl(tosAllNewsBaseUrl)
				.setFooter("IMC Games")
				.setImage(newsImage.getImgurSrc())
				.setThumbnail(tosIconUrl);

		StringBuilder description = new StringBuilder();

		appNews.forEach(news -> {
			String title = "• "+createLinkWithLabel(news.getTitle(), news.getUrl());
			description.append(title);
			description.append("\n");
		});

		description.append("\n **[Image Source](" + newsImage.getActualImgSrc() + ")**");

		embedBuilder.addField("Patch Notes And News", description.toString());
		return embedBuilder;
	}

	public String createDescription() {
		String[] urls = {
				createLinkWithLabel("All", tosAllNewsBaseUrl),
				createLinkWithLabel("Announcements", tosAnnouncementsUrl),
				createLinkWithLabel("Events", tosEventsUrl),
				createLinkWithLabel("Patch", tosPatchUrl),
				createLinkWithLabel("Blog", tosBlogUrl),
				createLinkWithLabel("Issues", tosIssuesUrl)
		};

		return String.join(" | ", urls);
	}

	public String createLinkWithLabel(String label, String link) {
		return String.format("[%s](%s)", label, link);
	}
}
