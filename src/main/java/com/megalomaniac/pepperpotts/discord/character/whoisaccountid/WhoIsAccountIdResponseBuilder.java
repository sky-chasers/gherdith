package com.megalomaniac.pepperpotts.discord.character.whoisaccountid;

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount;
import com.megalomaniac.pepperpotts.db.character.Character;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WhoIsAccountIdResponseBuilder {

	String buildFoundCharacterResponse(TosAccount tosAccount) {
		List<Character> characters = tosAccount.getCharacters();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(buildFoundCharacterResponse(characters.size(), tosAccount.getInGameAccountId()));
		stringBuilder.append("\n ");

		characters.forEach(teamName ->  {
			stringBuilder.append("- ");
			stringBuilder.append(teamName.getTeamName());
			stringBuilder.append("\n");
		});

		return stringBuilder.toString();
	}

	String buildFoundCharacterResponse(int teamNamesCount, String accountId) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Found **");
		stringBuilder.append(teamNamesCount);
		stringBuilder.append(" team names** for account id: **");
		stringBuilder.append(accountId);
		stringBuilder.append("**\n");

		return stringBuilder.toString();
	}
}
