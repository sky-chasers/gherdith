package com.megalomaniac.pepperpotts.discord.character.whois;

import lombok.Getter;
import lombok.Setter;

public class WhoIsCommand {

	@Getter
	@Setter
	private String command;

	@Getter
	@Setter
	private String teamName;

	WhoIsCommand(String wholeCommand) {
		String[] splitCommand = wholeCommand.split(" ");

		this.command = splitCommand[0];
		this.teamName = splitCommand[1];
	}
}
