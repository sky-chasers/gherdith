package com.megalomaniac.pepperpotts.discord.character.whoisaccountid;

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount;
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccountRepository;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WhoIsAccountIdService {

	private final TosAccountRepository tosAccountRepository;
	private final WhoIsAccountIdResponseBuilder responseBuilder;
	private final MessageSender messageSender;

	public WhoIsAccountIdService(TosAccountRepository tosAccountRepository,
								 WhoIsAccountIdResponseBuilder responseBuilder,
								 MessageSender messageSender) {
		this.tosAccountRepository = tosAccountRepository;
		this.responseBuilder = responseBuilder;
		this.messageSender = messageSender;
	}

	void respondWithAccountInfoPrivately(WhoIsAccountIdCommand command) {
		Optional<TosAccount> characterOptional = tosAccountRepository.findByInGameAccountId(command.getAccountId());

		if (characterOptional.isPresent()) {
			String response = responseBuilder.buildFoundCharacterResponse(characterOptional.get());
			messageSender.sendSuccessMessage(command.getMessage(), response);
		} else {
			String response = responseBuilder.buildFoundCharacterResponse(0, command.getAccountId());
			messageSender.sendInfoMessage(command.getMessage(), response);
		}
	}
}
