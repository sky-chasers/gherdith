package com.megalomaniac.pepperpotts.discord.character.whois;

import com.megalomaniac.pepperpotts.discord.command.GeneralCommand;
import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import org.javacord.api.entity.message.Message;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

@Component
public class WhoIsListener implements MessageCreateListener {

	private final GeneralCommand command = GeneralCommand.WHO_IS_TEAM_NAME;
	private final CommandValidator commandValidator;
	private final WhoIsService whoIsService;

	public WhoIsListener(CommandValidator commandValidator, WhoIsService whoIsService) {
		this.commandValidator = commandValidator;
		this.whoIsService = whoIsService;
	}

	@Override
	public void onMessageCreate(MessageCreateEvent event) {
		Message message = event.getMessage();
		String rawCommand = message.getContent();

		if (!commandValidator.isValidCommand(event, command)) {
			return;
		}

		WhoIsCommand whoIsCommand = new WhoIsCommand(rawCommand);
		whoIsService.respond(message, whoIsCommand);
	}

}
