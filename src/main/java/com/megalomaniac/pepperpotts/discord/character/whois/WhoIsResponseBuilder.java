package com.megalomaniac.pepperpotts.discord.character.whois;

import com.megalomaniac.pepperpotts.db.character.Character;
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WhoIsResponseBuilder {

	String buildFailedResponse(String teamName) {
		return "Unable to find any details of team skill: **" + teamName + "**";
	}

	String buildSuccessResponse(TosAccount tosAccount, String searchedTeamName) {
		List<Character> characters = tosAccount.getCharacters();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(buildFoundCharacterResponse(characters.size(), tosAccount.getInGameAccountId(), searchedTeamName));

		characters.forEach(teamName ->  {
			stringBuilder.append("> ");
			stringBuilder.append(teamName.getTeamName());
			stringBuilder.append("\n");
		});

		return stringBuilder.toString();
	}

	private String buildFoundCharacterResponse(int teamNamesCount, String accountId, String searchedTeamName) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Found **");
		stringBuilder.append(teamNamesCount);
		stringBuilder.append(" team names** associated with: **");
		stringBuilder.append(searchedTeamName);
		stringBuilder.append("**\n");
		stringBuilder.append("Account ID: **");
		stringBuilder.append(accountId);
		stringBuilder.append("**\n");

		return stringBuilder.toString();
	}
}
