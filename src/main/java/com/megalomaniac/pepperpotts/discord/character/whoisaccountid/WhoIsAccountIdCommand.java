package com.megalomaniac.pepperpotts.discord.character.whoisaccountid;

import lombok.Getter;
import lombok.Setter;
import org.javacord.api.entity.message.Message;

class WhoIsAccountIdCommand {

	@Getter
	@Setter
	private String command;

	@Getter
	@Setter
	private Message message;

	@Getter
	@Setter
	private String accountId;

	WhoIsAccountIdCommand(Message message) {
		String wholeCommand = message.getContent();
		String[] splitCommand = wholeCommand.split(" ");

		this.command = splitCommand[0];
		this.accountId = splitCommand[1];
		this.message = message;
	}
}
