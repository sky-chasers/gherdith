package com.megalomaniac.pepperpotts.discord.character.whois;

import com.megalomaniac.pepperpotts.db.character.Character;
import com.megalomaniac.pepperpotts.db.character.CharacterRepository;
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import org.javacord.api.entity.message.Message;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class WhoIsService {

	private final MessageSender messageSender;
	private final CharacterRepository characterRepository;
	private final WhoIsResponseBuilder whoIsResponseBuilder;

	public WhoIsService(MessageSender messageSender,
						CharacterRepository characterRepository,
						WhoIsResponseBuilder whoIsResponseBuilder) {
		this.messageSender = messageSender;
		this.characterRepository = characterRepository;
		this.whoIsResponseBuilder = whoIsResponseBuilder;
	}

	void respond(Message message, WhoIsCommand whoIsCommand) {
		String teamName = whoIsCommand.getTeamName();

		Optional<Character> characterOptional = characterRepository.findByTeamName(teamName);
		String response;

		if (characterOptional.isEmpty()) {
			response = whoIsResponseBuilder.buildFailedResponse(teamName);
			messageSender.sendInfoMessage(message, response);
		} else {
			TosAccount account = characterOptional.get().getTosAccount();
			response = whoIsResponseBuilder.buildSuccessResponse(account, teamName);
			messageSender.sendSuccessMessage(message, response);
		}
	}
}
