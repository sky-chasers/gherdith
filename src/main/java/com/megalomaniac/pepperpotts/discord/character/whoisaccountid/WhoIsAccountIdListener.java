package com.megalomaniac.pepperpotts.discord.character.whoisaccountid;

import com.megalomaniac.pepperpotts.discord.command.GeneralCommand;
import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Log4j2
@Component
public class WhoIsAccountIdListener implements MessageCreateListener {

	private final GeneralCommand command = GeneralCommand.WHO_IS_ACCOUNT_ID;
	private final WhoIsAccountIdService whoIsAccountIdService;
	private final CommandValidator validator;

	public WhoIsAccountIdListener(WhoIsAccountIdService whoIsAccountIdService,
								  CommandValidator validator) {
		this.whoIsAccountIdService = whoIsAccountIdService;
		this.validator = validator;
	}

	@Override
	public void onMessageCreate(MessageCreateEvent event) {
		Message message = event.getMessage();
		Optional<User> user = message.getAuthor().asUser();

		if (!validator.isValidCommand(event, command)) {
			return;
		}

		WhoIsAccountIdCommand whoIsAccountIdCommand = new WhoIsAccountIdCommand(message);
		whoIsAccountIdService.respondWithAccountInfoPrivately(whoIsAccountIdCommand);
	}

}
