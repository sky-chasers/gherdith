package com.megalomaniac.pepperpotts.discord.user;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class UserConfig {

	private List<String> adminIds;

	public UserConfig() {
		log.info("initializing admin ids...");
		this.adminIds = new ArrayList<>();
		this.adminIds.add("192535219008438272");
		this.adminIds.add("186807036061810688");
		this.adminIds.add("198006249411969025");
	}

	public List<String> getMegalomaniacAdminIds() {
		return adminIds;
	}

}
