package com.megalomaniac.pepperpotts.discord.user;

import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.user.User;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class UserService {

	private final UserConfig userConfig;

	public UserService(UserConfig userConfig) {
		this.userConfig = userConfig;
	}

	public boolean isAdmin(User user) {
		return userConfig.getMegalomaniacAdminIds().contains(user.getIdAsString());
	}
}
