package com.megalomaniac.pepperpotts.discord.gtw.salary;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;

@Service
public class CalculateGtwSalaryFormatter {

	String formatGtwSalaryEntryMessage(String title,
									   BigDecimal totalGtwFunds,
									   List<GtwSalaryOutput> salaryOutputs) {

		StringBuilder stringBuilder = new StringBuilder()
				.append("**")
				.append(title)
				.append("**\n\n")
				.append("Total GTW Fund: **")
				.append(formatNumberWithCommas(totalGtwFunds))
				.append(" silver ** \n");

		for (GtwSalaryOutput salaryOutput : salaryOutputs) {

			if (!salaryOutput.getSetting().isDividedByParticipant()) {
				String fundLabel = salaryOutput.getSetting().getLabel();
				BigDecimal amount = salaryOutput.getSalaryValue();
				BigDecimal percentage = salaryOutput.getSetting().getPercentage();

				stringBuilder.append(buildSalaryLine(fundLabel, amount, percentage));
			}
		}

		return stringBuilder.toString();
	}

	String formatParticipantSalary(List<GtwSalaryOutput> salaryOutputs, int participantSize) {
		StringBuilder stringBuilder = new StringBuilder();

		salaryOutputs.forEach(salaryOutput -> {
			if (salaryOutput.getSetting().isDividedByParticipant()) {
				stringBuilder.append(buildSalaryLine(salaryOutput.getSetting().getLabel(), salaryOutput.getSalaryValue(), salaryOutput.getSetting().getPercentage()));
				stringBuilder.append("Total number of participants: ");
				stringBuilder.append(participantSize);
				stringBuilder.append("\n");
				stringBuilder.append("Salary per participant: amount/ **");
				stringBuilder.append(participantSize);
				stringBuilder.append(" = ");
				stringBuilder.append(formatNumberWithCommas(salaryOutput.getSalaryPerParticipant()));
				stringBuilder.append("** silver \n");
				stringBuilder.append("----------------------------------- \n");
				stringBuilder.append("Everyone who participated in the previous war has received the full value except the following members: \n");

				for (GtwAttendanceSalary gtwAttendanceSalary : salaryOutput.getNonPerfectParticipantSalary()) {
					stringBuilder.append("* ");
					stringBuilder.append(gtwAttendanceSalary.getGtwAttendanceRow().getName());
					stringBuilder.append(" (");
					stringBuilder.append(gtwAttendanceSalary.getGtwAttendanceRow().getScoreInPercentage());
					stringBuilder.append("% participation time) = ** ");
					stringBuilder.append(formatNumberWithCommas(gtwAttendanceSalary.getFinalSalary()));
					stringBuilder.append("** silver \n");
				}

				stringBuilder.append("----------------------------------- \n");
			}
		});

		return stringBuilder.toString();
	}

	String formatRemainingSilver(BigDecimal remainingSilver) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Remaining Victory Benefit: **" );
		stringBuilder.append(formatNumberWithCommas(remainingSilver));
		stringBuilder.append("** silver \n");
		stringBuilder.append("This will be used for war supplies and other necessities.");

		return stringBuilder.toString();
	}

	private String buildSalaryLine(String label, BigDecimal amount, BigDecimal percentage) {
		return label +  " : **" + formatNumberWithCommas(amount) + "** silver ( " + percentage + "% of total GTW Fund ) \n";
	}

	private String formatNumberWithCommas(BigDecimal number) {
		NumberFormat numberFormatter = NumberFormat.getInstance();
		numberFormatter.setGroupingUsed(true);

		return numberFormatter.format(number);
	}
}
