package com.megalomaniac.pepperpotts.discord.gtw.attendance;

import lombok.Getter;
import lombok.Setter;

public class GtwAttendanceRow {

	@Getter
	@Setter
	private String name;

	@Getter
	@Setter
	private Integer scoreInPercentage;

	public GtwAttendanceRow(String name, Integer scoreInPercentage) {
		this.name = name;
		this.scoreInPercentage = scoreInPercentage;
	}
}
