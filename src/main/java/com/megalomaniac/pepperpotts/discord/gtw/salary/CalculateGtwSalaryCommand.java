package com.megalomaniac.pepperpotts.discord.gtw.salary;

import lombok.Getter;
import lombok.Setter;
import org.javacord.api.entity.message.Message;

import java.math.BigDecimal;
import java.math.BigInteger;

class CalculateGtwSalaryCommand {

	@Getter
	@Setter
	private String command;

	@Getter
	@Setter
	private Message message;

	@Setter
	@Getter
	private String googleSheetsId;

	@Getter
	@Setter
	private BigDecimal totalFunds;

	public CalculateGtwSalaryCommand(Message message) {
		String wholeCommand = message.getContent();
		String[] splitCommand = wholeCommand.split(" ");

		this.command = splitCommand[0];
		this.googleSheetsId = splitCommand[1];
		this.totalFunds = new BigDecimal(splitCommand[2]);
		this.message = message;
	}
}
