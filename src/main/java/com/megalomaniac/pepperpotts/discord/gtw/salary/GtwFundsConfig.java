package com.megalomaniac.pepperpotts.discord.gtw.salary;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GtwFundsConfig {

	@Value("${megalomaniac.pepperpotts.funds.gtwAttendanceScoreThresholdPercentage}")
	private int gtwAttendanceScoreThresholdPercentage;

	@Bean(name = "gtwAttendanceScoreThresholdPercentage")
	public int getGtwAttendanceScoreThresholdPercentage() {
		return this.gtwAttendanceScoreThresholdPercentage;
	}

	@Value("${megalomaniac.pepperpotts.funds.gtwSalarySettingsSheetId}")
	private String gtwSettingsSheetId;

	@Bean(name = "gtwSettingsSheetId")
	public String getGtwSettingsSheetId() {
		return this.gtwSettingsSheetId;
	}
}
