package com.megalomaniac.pepperpotts.discord.gtw.attendance;

import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import com.megalomaniac.pepperpotts.google.sheets.SheetReader;
import com.megalomaniac.pepperpotts.discord.gtw.salary.InvalidGtwSheetException;
import com.megalomaniac.pepperpotts.discord.gtw.salary.SheetNotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class GtwAttendanceService {

	private final SheetReader sheetReader;
	private final String gtwAttendanceRange;
	private final String gtwAttendanceIdentifier;

	public GtwAttendanceService(SheetReader sheetReader, String gtwAttendanceRange, String gtwAttendanceIdentifier) {
		this.sheetReader = sheetReader;
		this.gtwAttendanceRange = gtwAttendanceRange;
		this.gtwAttendanceIdentifier = gtwAttendanceIdentifier;
	}

	public List<GtwAttendanceRow> fetchAttendanceWithId(String googleSheetId) throws SheetNotFoundException {
		List<GtwAttendanceRow> gtwAttendanceList = new ArrayList<>();

		try {
			List<List<Object>> response = sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, gtwAttendanceRange);

			for (List row : response) {
				String name = row.get(0).toString();
				String rawScore = row.get(1).toString().replaceAll("%", "");
				Integer score = Integer.parseInt(rawScore);

				GtwAttendanceRow gtwAttendanceRow = new GtwAttendanceRow(name, score);
				gtwAttendanceList.add(gtwAttendanceRow);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new SheetNotFoundException();
		}

		return gtwAttendanceList;
	}

	public GtwAttendanceSheet fetchGoogleSheetPropertiesWithId(String googleSheetId) throws SheetNotFoundException, InvalidGtwSheetException {
		try {
			SpreadsheetProperties properties = sheetReader.fetchGoogleSheetPropertiesWithId(googleSheetId);

			if (!properties.getTitle().startsWith(gtwAttendanceIdentifier)) {
				throw new InvalidGtwSheetException();
			}

			return new GtwAttendanceSheet(googleSheetId, properties.getTitle());
		} catch (IOException e) {
			e.printStackTrace();
			throw new SheetNotFoundException();
		}
	}

	public List<GtwAttendanceRow> filterParticipantsWithScoreHigherThan(List<GtwAttendanceRow> participants, Integer score) {
		return participants
				.stream()
				.filter(participant -> participant.getScoreInPercentage() > score)
				.collect(Collectors.toList());
	}
}
