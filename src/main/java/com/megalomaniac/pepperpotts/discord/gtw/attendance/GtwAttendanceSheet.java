package com.megalomaniac.pepperpotts.discord.gtw.attendance;

import lombok.Getter;
import lombok.Setter;

public class GtwAttendanceSheet {

	@Getter
	@Setter
	private String googleSheetId;

	@Getter
	@Setter
	private String title;

	GtwAttendanceSheet(String googleSheetId, String title) {
		this.googleSheetId = googleSheetId;
		this.title = title;
	}
}
