package com.megalomaniac.pepperpotts.discord.gtw.salary;

import com.megalomaniac.pepperpotts.discord.gtw.attendance.GtwAttendanceRow;
import com.megalomaniac.pepperpotts.discord.gtw.attendance.GtwAttendanceService;
import com.megalomaniac.pepperpotts.discord.gtw.attendance.GtwAttendanceSheet;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.Message;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

@Log4j2
@Service
public class GtwSalaryCalculator {

	private final GtwAttendanceService attendance;
	private final GtwSalaryPartsCalculator partsCalculator;
	private final CalculateGtwSalaryFormatter messageFormatter;
	private final GtwSalarySettingsService gtwSalarySettingsService;
	private final MessageSender messageSender;

	public GtwSalaryCalculator(GtwAttendanceService attendance,
							   GtwSalaryPartsCalculator partsCalculator,
							   CalculateGtwSalaryFormatter messageFormatter,
							   GtwSalarySettingsService gtwSalarySettingsService,
							   MessageSender messageSender) {

		this.attendance = attendance;
		this.partsCalculator = partsCalculator;
		this.messageFormatter = messageFormatter;
		this.gtwSalarySettingsService = gtwSalarySettingsService;
		this.messageSender = messageSender;
	}

	public void calculateSalary(CalculateGtwSalaryCommand command) {
		String googleSheetsId = command.getGoogleSheetsId();
		BigDecimal totalGtwFunds = command.getTotalFunds();
		Message message = command.getMessage();

		try  {
			List<GtwSalarySetting> salarySettings = gtwSalarySettingsService.fetchGtwSettings();
			GtwAttendanceSheet sheetProperties = attendance.fetchGoogleSheetPropertiesWithId(googleSheetsId);
			List<GtwAttendanceRow> gtwAttendanceList = attendance.fetchAttendanceWithId(googleSheetsId);
			List<GtwAttendanceRow> participants = attendance.filterParticipantsWithScoreHigherThan(gtwAttendanceList, 0);

			String googleSheetTitle = sheetProperties.getTitle();
			List<GtwSalaryOutput> outputList = partsCalculator.calculateBasedOnSettings(totalGtwFunds, salarySettings, participants);
			BigDecimal remainingSilver = partsCalculator.calculateRemainingFunds(outputList);

			String entryMessage = messageFormatter.formatGtwSalaryEntryMessage(googleSheetTitle, totalGtwFunds, outputList);
			String participantSalaryMessage = messageFormatter.formatParticipantSalary(outputList, participants.size());
			String endMessage = messageFormatter.formatRemainingSilver(remainingSilver);

			messageSender.sendMessage(message, entryMessage);
			messageSender.sendMessage(message, participantSalaryMessage);
			messageSender.sendMessage(message, endMessage);

		} catch (SheetNotFoundException e) {
			messageSender.sendErrorMessage(message, "I'm sorry, I can't find the sheet with id: " +  googleSheetsId + ".");
		} catch (InvalidGtwSheetException e) {
			messageSender.sendErrorMessage(message, "The sheet you're trying to access is invalid. Make sure it's a valid GTW attendance sheet.");
		} catch (InvalidGtwSalarySettingsException e) {
			messageSender.sendErrorMessage(message, e.getMessage());
		}

	}

	public boolean hasValidTotalFunds(String command) {
		try {
			String totalFundAsString = command.split(" ")[2];
			BigInteger fund = new BigInteger(totalFundAsString);
			return fund.compareTo(BigInteger.ZERO) > 0;
		} catch (NumberFormatException | ArrayIndexOutOfBoundsException e ) {
			return false;
		}
	}

}
