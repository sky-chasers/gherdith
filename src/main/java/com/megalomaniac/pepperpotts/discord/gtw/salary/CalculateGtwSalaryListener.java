package com.megalomaniac.pepperpotts.discord.gtw.salary;

import com.megalomaniac.pepperpotts.discord.command.AdminCommand;
import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.Message;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class CalculateGtwSalaryListener implements MessageCreateListener {

	private final GtwSalaryCalculator gtwSalaryCalculator;
	private final MessageSender messageSender;
	private final AdminCommand adminCommand = AdminCommand.CALCULATE_GTW_SALARY;
	private final CommandValidator validator;

	public CalculateGtwSalaryListener(GtwSalaryCalculator responder,
									  MessageSender messageSender,
									  CommandValidator validator) {
		this.gtwSalaryCalculator = responder;
		this.messageSender = messageSender;
		this.validator = validator;
	}

	@Override
	public void onMessageCreate(MessageCreateEvent event) {
		Message message = event.getMessage();
		String rawCommand = message.getContent();

		if(!validator.isValidCommand(event, adminCommand)) {
			return;
		}

		if (!gtwSalaryCalculator.hasValidTotalFunds(rawCommand)){
			messageSender.sendErrorMessage(message, "The `totalFunds` must be a valid number greater than zero.");
			return;
		}

		CalculateGtwSalaryCommand calculateGtwSalaryCommand = new CalculateGtwSalaryCommand(message);

		gtwSalaryCalculator.calculateSalary(calculateGtwSalaryCommand);
	}
}
