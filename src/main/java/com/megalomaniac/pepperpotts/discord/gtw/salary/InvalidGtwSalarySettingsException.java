package com.megalomaniac.pepperpotts.discord.gtw.salary;

import lombok.Getter;
import lombok.Setter;

public class InvalidGtwSalarySettingsException extends Exception {

	@Getter
	@Setter
	private String message;

	public InvalidGtwSalarySettingsException(String message) {
		this.message = message;
	}
}
