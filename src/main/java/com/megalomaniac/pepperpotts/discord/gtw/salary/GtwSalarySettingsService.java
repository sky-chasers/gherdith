package com.megalomaniac.pepperpotts.discord.gtw.salary;

import com.megalomaniac.pepperpotts.google.sheets.SheetReader;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@Service
public class GtwSalarySettingsService {

	private final SheetReader sheetReader;

	@Value("${megalomaniac.pepperpotts.funds.gtwSalarySettingsSheetId}")
	private String googleSheetId;

	public GtwSalarySettingsService(SheetReader sheetReader) {
		this.sheetReader = sheetReader;
	}

	public List<GtwSalarySetting> fetchGtwSettings() throws SheetNotFoundException, InvalidGtwSalarySettingsException {
		List<GtwSalarySetting> settings = new ArrayList<>();
		String range = "settings!A:C";

		String labelName = "Label";
		String percentageName = "Percentage";
		String divideByParticipantName = "Divide by participant";

		try {
			List<List<Object>> response = sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, range);
			List<String> labels = response.get(0).stream()
					.map(object -> Objects.toString(object, null))
					.collect(Collectors.toList());

			int labelIndex = findIndexOfLabel(labels, labelName);
			int percentageIndex = findIndexOfLabel(labels, percentageName);
			int divideByParticipantIndex = findIndexOfLabel(labels, divideByParticipantName);

			for (List row : response.subList(1, response.size())) {
				String label = row.get(labelIndex).toString();
				BigDecimal percentage = convertStringToNumber(percentageName, row.get(percentageIndex).toString());
				boolean divideByParticipants = convertStringToBoolean(label, row.get(divideByParticipantIndex).toString());

				GtwSalarySetting setting = new GtwSalarySetting();
				setting.setLabel(label);
				setting.setPercentage(percentage);
				setting.setDividedByParticipant(divideByParticipants);

				settings.add(setting);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new SheetNotFoundException();
		}

		return settings;
	}

	private int findIndexOfLabel(List<String> labels, String label) throws InvalidGtwSalarySettingsException {
		int labelIndex = labels.indexOf(label);

		if (labelIndex == -1) {
			String errorMessage = "Failed to find column "+ label +" in google sheet.";
			throw new InvalidGtwSalarySettingsException(errorMessage);
		}

		return labelIndex;
	}

	private boolean convertStringToBoolean(String label, String booleanString) throws InvalidGtwSalarySettingsException {

		if (booleanString.equals("true")) {
			return true;
		}

		if (booleanString.equals("false")) {
			return false;
		}

		String errorMessage = "Failed to convert `" + label + "` : `" + booleanString + "` to a boolean. Value must be true or false.";
		throw new InvalidGtwSalarySettingsException(errorMessage);
	}

	private BigDecimal convertStringToNumber(String label, String numberString) throws InvalidGtwSalarySettingsException {
		try {
			return new BigDecimal(numberString);
		} catch (NumberFormatException e) {
			String errorMessage = "Failed to convert `" + label + "`: " + numberString + " to a number.";
			throw new InvalidGtwSalarySettingsException(errorMessage);
		}
	}
}
