package com.megalomaniac.pepperpotts.discord.gtw.salary;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class GtwSalarySetting {

	private String label;
	private BigDecimal percentage;
	private boolean dividedByParticipant;

}
