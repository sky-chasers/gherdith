package com.megalomaniac.pepperpotts.discord.gtw.attendance;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GtwAttendanceSheetConfig {

	@Value("${megalomaniac.pepperpotts.googleSheets.gtwAttendanceRange}")
	private String gtwAttendanceRange;

	@Bean(name = "gtwAttendanceRange")
	public String getGtwAttendanceRange() {
		return gtwAttendanceRange;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.gtwAttendanceIdentifier}")
	private String gtwAttendanceSheetIdentifier;

	@Bean(name = "gtwAttendanceIdentifier")
	public String getGtwAttendanceSheetIdentifier() {
		return gtwAttendanceSheetIdentifier;
	}
}
