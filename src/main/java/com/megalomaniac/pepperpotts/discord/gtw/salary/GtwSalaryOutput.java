package com.megalomaniac.pepperpotts.discord.gtw.salary;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
public class GtwSalaryOutput {

	GtwSalarySetting setting;
	BigDecimal salaryValue;
	BigDecimal salaryPerParticipant;
	List<GtwAttendanceSalary> nonPerfectParticipantSalary;

}
