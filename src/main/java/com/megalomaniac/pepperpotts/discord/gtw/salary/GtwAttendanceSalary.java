package com.megalomaniac.pepperpotts.discord.gtw.salary;

import com.megalomaniac.pepperpotts.discord.gtw.attendance.GtwAttendanceRow;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

class GtwAttendanceSalary {

	@Setter
	@Getter
	private GtwAttendanceRow gtwAttendanceRow;

	@Getter
	@Setter
	private BigDecimal finalSalary;

	GtwAttendanceSalary(GtwAttendanceRow row, BigDecimal finalSalary) {
		this.gtwAttendanceRow = row;
		this.finalSalary = finalSalary;
	}
}
