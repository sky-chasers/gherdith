package com.megalomaniac.pepperpotts.discord.gtw.salary;

import com.megalomaniac.pepperpotts.discord.gtw.attendance.GtwAttendanceRow;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Service
public class GtwSalaryPartsCalculator {

	private final int gtwAttendanceScoreThresholdPercentage;

	public GtwSalaryPartsCalculator(int gtwAttendanceScoreThresholdPercentage) {
		this.gtwAttendanceScoreThresholdPercentage = gtwAttendanceScoreThresholdPercentage;
	}

	public List<GtwSalaryOutput> calculateBasedOnSettings(BigDecimal totalGtwFunds, List<GtwSalarySetting> settings,
														  List<GtwAttendanceRow> participants) throws InvalidGtwSalarySettingsException {
		List<GtwSalaryOutput> outputList = new ArrayList<>();
		validateSettingTotalIs100Percent(settings);

		settings.forEach(setting -> {
			BigDecimal salary = getPercentageOfBigDecimals(setting.getPercentage(), totalGtwFunds);

			GtwSalaryOutput output = new GtwSalaryOutput();
			output.setSetting(setting);
			output.setSalaryValue(salary);

			if (setting.isDividedByParticipant()) {
				BigDecimal participantSize = BigDecimal.valueOf(participants.size());
				BigDecimal salaryPerParticipant = salary.divide(participantSize, RoundingMode.DOWN);
				List<GtwAttendanceSalary> nonPerfectSalaries = calculateSalaryOfParticipantsBelowOrEqualToThreshold2(
						participants, gtwAttendanceScoreThresholdPercentage, salaryPerParticipant);

				output.setSalaryPerParticipant(salaryPerParticipant);
				output.setNonPerfectParticipantSalary(nonPerfectSalaries);
			}

			outputList.add(output);
		});

		return outputList;
	}

	public BigDecimal calculateRemainingFunds(List<GtwSalaryOutput> outputs) {
		BigDecimal remainingPerParticipantFund = new BigDecimal(0);

		for (GtwSalaryOutput output: outputs) {
			if(output.getSetting().isDividedByParticipant()) {
				BigDecimal nonPerfectParticipantSize = new BigDecimal(output.getNonPerfectParticipantSalary().size());

				BigDecimal salaryPerParticipant = output.getSalaryPerParticipant();
				BigDecimal totalNonPerfectSalary = salaryPerParticipant.multiply(nonPerfectParticipantSize);
				BigDecimal combinedSalaryOfNonPerfectParticipants = output.getNonPerfectParticipantSalary()
						.stream()
						.map(GtwAttendanceSalary::getFinalSalary)
						.reduce(BigDecimal.ZERO, BigDecimal::add);

				BigDecimal remainingFunds = totalNonPerfectSalary.subtract(combinedSalaryOfNonPerfectParticipants);
				remainingPerParticipantFund = remainingPerParticipantFund.add(remainingFunds);
			}
		}

		return remainingPerParticipantFund;
	}

	private List<GtwAttendanceSalary> calculateSalaryOfParticipantsBelowOrEqualToThreshold2(
			List<GtwAttendanceRow> participants, int threshold, BigDecimal salaryPerParticipant) {

		List<GtwAttendanceRow> validParticipants = participants
				.stream()
				.filter(participant -> participant.getScoreInPercentage() <= threshold)
				.collect(Collectors.toList());

		List<GtwAttendanceSalary> result = new ArrayList<>();

		validParticipants.forEach(participant -> {
			BigDecimal percentage = new BigDecimal(participant.getScoreInPercentage());
			BigDecimal finalSalary = getPercentageOfBigDecimals(percentage, salaryPerParticipant);
			result.add(new GtwAttendanceSalary(participant, finalSalary));
		});

		return result;
	}

	private void validateSettingTotalIs100Percent(List<GtwSalarySetting> settings) throws InvalidGtwSalarySettingsException {
		BigDecimal sum = settings.stream()
				.map(GtwSalarySetting::getPercentage)
				.reduce(BigDecimal.ZERO, BigDecimal::add);

		if (!sum.equals(BigDecimal.valueOf(100))) {
			throw new InvalidGtwSalarySettingsException("The sum of all percentages must be 100.");
		}
	}

	private BigDecimal getPercentageOfBigDecimals(BigDecimal percent, BigDecimal number) {
		return percent.multiply(number).divide(BigDecimal.valueOf(100), RoundingMode.DOWN);
	}

}
