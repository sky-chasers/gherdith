package com.megalomaniac.pepperpotts.discord.message;

import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.javacord.api.entity.user.User;
import org.springframework.stereotype.Service;

@Service
public class PrivateMessageSender {

	private final MessageBuilder messageBuilder;

	public PrivateMessageSender(MessageBuilder messageBuilder) {
		this.messageBuilder = messageBuilder;
	}

	public void sendMessageToUser(User user, String message) {
		if (!user.isBot() && !user.isYourself()) {
			user.sendMessage(message);
		}
	}

	public void sendMessageToUser(User user, EmbedBuilder embedBuilder) {
		if (!user.isBot() && !user.isYourself()) {
			user.sendMessage(embedBuilder);
		}
	}

	public void sendSuccessMessageToUser(User user, String message) {
		EmbedBuilder embedBuilder = messageBuilder.buildSuccessMessage(message);

		if (!user.isBot() && !user.isYourself()) {
			user.sendMessage(embedBuilder);
		}
	}
}
