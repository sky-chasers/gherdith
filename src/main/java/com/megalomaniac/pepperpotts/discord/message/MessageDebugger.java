package com.megalomaniac.pepperpotts.discord.message;

import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.Message;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class MessageDebugger {

	public void debug(String debugMessage, Message message) {
		log.debug("\n messageID: {} \n {}", message.getId(), debugMessage);
	}

	public void info(String infoMessage, Long messageId, Long authorId) {
		log.info("\n messageId: {} \n authorId: {} \n message: \n {}", messageId, authorId, infoMessage);
	}
}
