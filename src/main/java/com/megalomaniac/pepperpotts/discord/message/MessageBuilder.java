package com.megalomaniac.pepperpotts.discord.message;

import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

import java.awt.*;

@Service
public class MessageBuilder {

	EmbedBuilder buildErrorMessage(String message) {
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setColor(Color.RED);
		embedBuilder.setTitle(":warning: Yikes! An unexpected error occurred.");
		embedBuilder.setDescription(message);

		return embedBuilder;
	}

	EmbedBuilder buildSuccessMessage(String message) {
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setColor(Color.GREEN);
		embedBuilder.setTitle(":white_check_mark: Success!");
		embedBuilder.setDescription(message);

		return embedBuilder;
	}

	EmbedBuilder buildInfoMessage(String message) {
		EmbedBuilder embedBuilder = new EmbedBuilder();
		embedBuilder.setColor(Color.CYAN);
		embedBuilder.setDescription(":information_source:  " + message);

		return embedBuilder;
	}
}
