package com.megalomaniac.pepperpotts.discord.message;

import org.javacord.api.entity.channel.TextChannel;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

@Service
public class PublicMessageSender {

	public void sendMessage(TextChannel channel, String message) {
		channel.sendMessage(message);
	}

	public void sendMessage(TextChannel channel, EmbedBuilder message) {
		channel.sendMessage(message);
	}

	public void sendMessage(TextChannel channel, String content, EmbedBuilder embed) {
		channel.sendMessage(content, embed);
	}
}
