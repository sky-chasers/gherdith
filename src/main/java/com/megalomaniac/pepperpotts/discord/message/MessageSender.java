package com.megalomaniac.pepperpotts.discord.message;

import org.javacord.api.entity.message.Message;
import org.javacord.api.entity.message.embed.EmbedBuilder;
import org.springframework.stereotype.Service;

@Service
public class MessageSender {

	private final PrivateMessageSender privateMessageSender;
	private final PublicMessageSender publicMessageSender;
	private final MessageBuilder messageBuilder;

	public MessageSender(PrivateMessageSender privateMessageSender,
						 PublicMessageSender publicMessageSender,
						 MessageBuilder messageBuilder) {
		this.privateMessageSender = privateMessageSender;
		this.publicMessageSender = publicMessageSender;
		this.messageBuilder = messageBuilder;
	}

	public void sendErrorMessage(Message messageDetailsFromUser, String errorMessage) {
		EmbedBuilder embedBuilder = messageBuilder.buildErrorMessage(errorMessage);
		sendMessage(messageDetailsFromUser, embedBuilder);
	}

	public void sendSuccessMessage(Message messageDetailsFromUser, String successMessage) {
		EmbedBuilder embedBuilder = messageBuilder.buildSuccessMessage(successMessage);
		sendMessage(messageDetailsFromUser, embedBuilder);
	}

	public void sendInfoMessage(Message messageDetailsFromUser, String infoMessage) {
		EmbedBuilder embedBuilder = messageBuilder.buildInfoMessage(infoMessage);
		sendMessage(messageDetailsFromUser, embedBuilder);
	}

	public void sendMessage(Message messageDetailsFromUser, String response) {
		if (messageDetailsFromUser.isPrivateMessage() && messageDetailsFromUser.getUserAuthor().isPresent()) {
			privateMessageSender.sendMessageToUser(messageDetailsFromUser.getUserAuthor().get(), response);
		}

		if(!messageDetailsFromUser.isPrivateMessage()) {
			publicMessageSender.sendMessage(messageDetailsFromUser.getChannel(), response);
		}
	}

	public void sendMessage(Message messageDetailsFromUser, EmbedBuilder response) {
		if (messageDetailsFromUser.isPrivateMessage() && messageDetailsFromUser.getUserAuthor().isPresent()) {
			privateMessageSender.sendMessageToUser(messageDetailsFromUser.getUserAuthor().get(), response);
		}

		if(!messageDetailsFromUser.isPrivateMessage()) {
			publicMessageSender.sendMessage(messageDetailsFromUser.getChannel(), response);
		}
	}
}
