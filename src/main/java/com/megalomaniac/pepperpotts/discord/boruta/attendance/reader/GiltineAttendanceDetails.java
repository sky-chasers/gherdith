package com.megalomaniac.pepperpotts.discord.boruta.attendance.reader;

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.GiltineLocation;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class GiltineAttendanceDetails {

	@Getter
	@Setter
	private String teamName;

	@Getter
	@Setter
	private Integer score;

	@Setter
	@Getter
	private List<GiltineLocation> locations;
}
