package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import com.google.api.services.sheets.v4.model.ValueRange;
import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.BiddingPointDetail;
import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.BiddingPointsTabReader;
import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.FailedToReadBiddingPointsException;
import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.LabelNotFoundException;
import com.megalomaniac.pepperpotts.google.sheets.SheetWriter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Log4j2
@Service
public class BonusPointsWriter {

	private final SheetWriter sheetWriter;
	private final BiddingPointsTabReader biddingPointsTabReader;

	public BonusPointsWriter(SheetWriter sheetWriter, BiddingPointsTabReader biddingPointsTabReader) {
		this.sheetWriter = sheetWriter;
		this.biddingPointsTabReader = biddingPointsTabReader;
	}

	public void writeBonusPointsToGoogleSheetWithId(String googleSheetId, List<BonusPointsOutput> outputList) throws
			FailedToReadBiddingPointsException, FailedToWriteBonusPointsException, LabelNotFoundException {
		List<BiddingPointDetail> biddingPointDetails = biddingPointsTabReader.fetchBiddingPointDetails(googleSheetId);
		List<ValueRange> valuesToWrite = new ArrayList<>();

		for (BiddingPointDetail detail : biddingPointDetails) {
			Optional<BonusPointsOutput> bonusPointsOutputSearchResult = outputList
					.stream()
					.filter(output -> output.getTeamName().equals(detail.getTeamName()))
					.findFirst();

			ValueRange valueRange = new ValueRange();
			List<List<Object>> bonusPoint = new ArrayList<>();
			String cell = detail.getBorutaBonusPointsColumnLetter() + detail.getRowNumber();
			String range = "Bidding Points!" + cell;

			if (bonusPointsOutputSearchResult.isPresent()) {
				BonusPointsOutput bonusPointsOutput = bonusPointsOutputSearchResult.get();
				bonusPoint.add(Collections.singletonList(bonusPointsOutput.getBonusPoints()));
			} else {
				bonusPoint.add(Collections.singletonList(0));
			}

			valueRange.setValues(bonusPoint);
			valueRange.setRange(range);
			valuesToWrite.add(valueRange);
		}

		try {
			sheetWriter.update(googleSheetId, valuesToWrite);
		} catch (IOException e) {
			log.error("Failed to write bonus bidding points to google sheet id: {}", googleSheetId);
			log.error(e.getMessage());
			throw new FailedToWriteBonusPointsException();
		}
	}

}
