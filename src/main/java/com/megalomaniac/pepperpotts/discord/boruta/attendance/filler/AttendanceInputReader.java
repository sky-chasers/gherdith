package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import com.megalomaniac.pepperpotts.google.sheets.SheetReader;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Log4j2
@Service
public class AttendanceInputReader {

	private final SheetReader sheetReader;
	private final RaidTypeInputRangeMapper inputRangeMapper;
	private final RaidInputContentsReader inputContentsReader;

	public AttendanceInputReader(SheetReader sheetReader, RaidTypeInputRangeMapper inputRangeMapper,
								 RaidInputContentsReader inputContentsReader) {
		this.sheetReader = sheetReader;
		this.inputRangeMapper = inputRangeMapper;
		this.inputContentsReader = inputContentsReader;
	}

	public List<InputContents> fetchContentsOfGoogleSheetId(String googleSheetId, RaidType raidType) throws AttendanceInputReaderIOException, EmptyAttendanceInputException {
		try {
			String inputRange = inputRangeMapper.getInputRangeForRaidType(raidType);
			List<List<Object>> response = sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, inputRange);

			log.info("Fetching input for raid: {} with range: {}", raidType, inputRange);

			if (response == null || response.isEmpty()) {
				throw new EmptyAttendanceInputException();
			}

			return inputContentsReader.readLocationsWithRaidType(raidType, response);
		} catch (IOException e) {
			log.error("Failed to fetch contents of google sheet id: {}", googleSheetId);
			log.error(e.getMessage());
			throw new AttendanceInputReaderIOException();
		}
	}
}
