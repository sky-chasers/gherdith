package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

public class RaidBonusPoints {

	@Setter
	@Getter
	private String teamName;

	@Setter
	@Getter
	private BigDecimal maxPossibleBonusPoints;
}
