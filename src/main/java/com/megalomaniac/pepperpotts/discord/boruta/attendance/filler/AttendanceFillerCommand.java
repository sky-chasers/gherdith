package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import lombok.Getter;
import lombok.Setter;
import org.javacord.api.entity.message.Message;

public class AttendanceFillerCommand {

	@Getter
	@Setter
	private String command;

	@Getter
	@Setter
	private Message message;

	@Setter
	@Getter
	private String inputGoogleSheetId;

	@Getter
	@Setter
	private String outputGoogleSheetId;

	@Getter
	@Setter
	private RaidType raidType;

	public AttendanceFillerCommand(Message message) {
		String rawCommand = message.getContent();
		String[] splitCommand = rawCommand.split(" ");

		String rawRaidType = splitCommand[1].toUpperCase();

		this.message = message;
		this.command = splitCommand[0];
		this.raidType = RaidType.valueOf(rawRaidType);
		this.inputGoogleSheetId = splitCommand[2];
		this.outputGoogleSheetId = splitCommand[3];
	}
}
