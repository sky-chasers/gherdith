package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import com.megalomaniac.pepperpotts.google.sheets.SheetReader;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class AttendanceOutputReader {

	private final SheetReader sheetReader;
	private final RaidTypeOutputRangeMapper raidTypeOutputRangeMapper;

	public AttendanceOutputReader(SheetReader sheetReader, RaidTypeOutputRangeMapper raidTypeOutputRangeMapper) {
		this.sheetReader = sheetReader;
		this.raidTypeOutputRangeMapper = raidTypeOutputRangeMapper;
	}

	public List<OutputContents> fetchGoogleSheetWithId(String googleSheetId, RaidType raidType) throws IOException {
		int currentRowIndex = 2;

		String outputRange = raidTypeOutputRangeMapper.getOutputRangeForRaidType(raidType);
		List<List<Object>> response = sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, outputRange);
		List<OutputContents> outputContents = new ArrayList<>();

		for (List row : response) {
			String name = row.get(0).toString();
			int columnIndex = row.size();
			String columnLetter = getLetterWithIndex(columnIndex);
			currentRowIndex = currentRowIndex + 1;

			OutputContents outputContent = new OutputContents();
			outputContent.setName(name);
			outputContent.setNextEmptyRange(columnLetter + currentRowIndex);

			outputContents.add(outputContent);
		}

		return outputContents;
	}

	private String getLetterWithIndex(int index) {
		return Character.toString("ABCDEFGHIJKLMNOPQRSTUVWXYZ".charAt(index));
	}
}
