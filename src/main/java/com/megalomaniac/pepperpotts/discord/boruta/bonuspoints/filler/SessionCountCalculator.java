package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.BorutaAttendanceDetails;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.GiltineAttendanceDetails;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.TreeSet;


@Log4j2
@Service
public class SessionCountCalculator {

	public int calculateBorutaSessionCount(List<BorutaAttendanceDetails> borutaAttendanceList) {
		TreeSet<SessionCountMapper> sessionCounts = new TreeSet<>(new SessionCountMapper());

		for (BorutaAttendanceDetails detail : borutaAttendanceList) {
			int locationSize = detail.getLocations().size();
			addSessionCountDetails(locationSize, sessionCounts);
		}

		return sessionCounts.last().getSessionCount();
	}

	public int calculateGiltineSessionCount(List<GiltineAttendanceDetails> giltineAttendanceList) {
		TreeSet<SessionCountMapper> sessionCounts = new TreeSet<>(new SessionCountMapper());

		for (GiltineAttendanceDetails detail : giltineAttendanceList) {
			int locationSize = detail.getLocations().size();
			addSessionCountDetails(locationSize, sessionCounts);
		}

		return sessionCounts.last().getSessionCount();
	}

	private void addSessionCountDetails(int locationSize, TreeSet<SessionCountMapper> sessionCounts ) {
		Optional<SessionCountMapper> result = sessionCounts.stream()
				.filter(sessionCountMapper -> sessionCountMapper.getSessionCount() == locationSize)
				.findFirst();

		if (result.isEmpty()) {
			SessionCountMapper sessionCountMapper = new SessionCountMapper();
			sessionCountMapper.setParticipantCount(1);
			sessionCountMapper.setSessionCount(locationSize);

			sessionCounts.add(sessionCountMapper);
		} else {
			SessionCountMapper sessionCountMapper = result.get();
			int participantCount = sessionCountMapper.getParticipantCount() + 1;
			sessionCountMapper.setParticipantCount(participantCount);
		}
	}
}
