package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.message.Message;
import org.springframework.stereotype.Service;

import java.util.List;

@Log4j2
@Service
public class AttendanceFillerService {

	private final AttendanceInputReader attendanceInputReader;
	private final MessageSender messageSender;
	private final AttendanceSheetWriter attendanceSheetWriter;

	public AttendanceFillerService(AttendanceInputReader attendanceInputReader,
								   MessageSender privateMessageSender,
								   AttendanceSheetWriter attendanceSheetWriter) {
		this.attendanceInputReader = attendanceInputReader;
		this.messageSender = privateMessageSender;
		this.attendanceSheetWriter = attendanceSheetWriter;
	}

	void fillAttendance(AttendanceFillerCommand command) {
		Message messageDetails = command.getMessage();

		try {
			List<InputContents> inputContents = attendanceInputReader
					.fetchContentsOfGoogleSheetId(command.getInputGoogleSheetId(), command.getRaidType());

			messageSender.sendInfoMessage(messageDetails, "Please wait while I start filling the attendance sheet...");
			attendanceSheetWriter.recordAttendanceToGoogleSheetWithId(command.getOutputGoogleSheetId(), inputContents, command.getRaidType());
			messageSender.sendSuccessMessage(messageDetails, "Thank you for waiting. Attendance is successfully recorded.");

		} catch (AttendanceInputReaderIOException e) {
			messageSender.sendErrorMessage(messageDetails, "Failed to fetch input sheet with id: " + command.getInputGoogleSheetId());
		} catch (AttendanceSheetWriterException e) {
			messageSender.sendErrorMessage(messageDetails, "Failed to write output to google sheet with id: " + command.getOutputGoogleSheetId());
		} catch (EmptyAttendanceInputException e) {
			messageSender.sendErrorMessage(messageDetails, "The input google sheet "+ command.getInputGoogleSheetId() + " is empty.");
		}
	}
}
