package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class RaidTypeOutputRangeMapper {

	private Map<RaidType, String> tabNamesMap;
	private Map<RaidType, String> outputRangeMap;

	public RaidTypeOutputRangeMapper(String borutaAttendanceTabName, String giltineAttendanceTabName) {
		this.tabNamesMap = new HashMap<>();
		this.tabNamesMap.put(RaidType.BORUTA, borutaAttendanceTabName);
		this.tabNamesMap.put(RaidType.GILTINE, giltineAttendanceTabName);

		String giltineAttendanceOutputRange = giltineAttendanceTabName + "!A3:BU";
		String borutaAttendanceOutputRange = borutaAttendanceTabName + "!A3:BU";
		this.outputRangeMap = new HashMap<>();
		this.outputRangeMap.put(RaidType.BORUTA, borutaAttendanceOutputRange);
		this.outputRangeMap.put(RaidType.GILTINE, giltineAttendanceOutputRange);
	}

	public String getTabNameForRaidType(RaidType raidType) {
		return tabNamesMap.get(raidType);
	}

	public String getOutputRangeForRaidType(RaidType raidType) {
		return outputRangeMap.get(raidType);
	}
}
