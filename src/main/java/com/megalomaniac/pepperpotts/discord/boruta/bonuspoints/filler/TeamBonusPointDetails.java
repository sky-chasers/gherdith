package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.BorutaAttendanceDetails;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.GiltineAttendanceDetails;
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPoints;
import lombok.Getter;
import lombok.Setter;

public class TeamBonusPointDetails {

	@Getter
	@Setter
	private String teamName;

	@Getter
	@Setter
	private BorutaAttendanceDetails borutaAttendanceDetails;

	@Getter
	@Setter
	private GiltineAttendanceDetails giltineAttendanceDetails;

	@Getter
	@Setter
	private RaidBonusPoints raidBonusPoints;

}
