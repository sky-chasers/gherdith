package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import java.util.List;
import java.util.Optional;

public enum BorutaLocation {

	SUB {
		@Override
		public String getLocationName() {
			return "Sub";
		}
	},
	SULIVINAS_LAIR {
		@Override
		public String getLocationName() {
			return "Sulivinas Lair (4952)";
		}
	},
	NOT_APPLICABLE {
		@Override
		public String getLocationName() {
			return "N/A";
		}
	};

	public abstract String getLocationName();

	public static List<BorutaLocation> getValidBorutaLocations() {
		return List.of(SULIVINAS_LAIR, SUB);
	}

	public static Optional<BorutaLocation> findLocationByName(String name) {
		for(BorutaLocation details : values()){
			if(details.getLocationName().equals(name)){
				return Optional.of(details);
			}
		}
		return Optional.empty();
	}
}
