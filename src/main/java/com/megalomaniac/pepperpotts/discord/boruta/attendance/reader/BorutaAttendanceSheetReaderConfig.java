package com.megalomaniac.pepperpotts.discord.boruta.attendance.reader;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BorutaAttendanceSheetReaderConfig {

	@Value("${megalomaniac.pepperpotts.googleSheets.consolidatedAttendance.borutaAttendance.range}")
	private String borutaAttendanceRange;

	@Bean(name = "borutaAttendanceRange")
	public String getBorutaAttendanceRange() {
		return borutaAttendanceRange;
	}
}
