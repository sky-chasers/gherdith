package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import lombok.Getter;
import lombok.Setter;

public class BonusPointsOutput {

	@Getter
	@Setter
	private String teamName;

	@Getter
	@Setter
	private int bonusPoints;
}
