package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import lombok.Getter;
import lombok.Setter;

import java.util.Comparator;

public class SessionCountMapper implements Comparator<SessionCountMapper> {

	@Getter
	@Setter
	private Integer sessionCount;

	@Getter
	@Setter
	private Integer participantCount;

	@Override
	public int compare(SessionCountMapper o1, SessionCountMapper o2) {
		if (o1.getSessionCount() > o2.getSessionCount()) {
			return 1;
		}
		if (o1.getSessionCount() < o2.getSessionCount()) {
			return -1;
		}
		return 0;
	}
}
