package com.megalomaniac.pepperpotts.discord.boruta.attendance.reader;

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.BorutaLocation;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class BorutaAttendanceDetails {

	@Getter
	@Setter
	private String teamName;

	@Setter
	@Getter
	private Integer score;

	@Getter
	@Setter
	private List<BorutaLocation> locations;
}
