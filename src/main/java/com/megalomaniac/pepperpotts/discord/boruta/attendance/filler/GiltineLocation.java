package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import java.util.List;
import java.util.Optional;

public enum GiltineLocation {
	DEMONIC_SANCTUARY {
		@Override
		public String getLocationName() {
			return "Demonic Sanctuary (11230)";
		}
	},
	SUB {
		@Override
		public String getLocationName() {
			return "Sub";
		}
	},
	NOT_APPLICABLE {
		@Override
		public String getLocationName() {
			return "N/A";
		}
	};

	public abstract String getLocationName();

	public static List<GiltineLocation> getValidGiltineRaidLocations() {
		return List.of(DEMONIC_SANCTUARY, SUB);
	}

	public static Optional<GiltineLocation> findLocationByName(String name) {
		for(GiltineLocation details : values()){
			if(details.getLocationName().equals(name)){
				return Optional.of(details);
			}
		}
		return Optional.empty();
	}
}
