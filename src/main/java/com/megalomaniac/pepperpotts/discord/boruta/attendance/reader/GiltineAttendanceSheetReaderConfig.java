package com.megalomaniac.pepperpotts.discord.boruta.attendance.reader;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GiltineAttendanceSheetReaderConfig {
	@Value("${megalomaniac.pepperpotts.googleSheets.consolidatedAttendance.giltineAttendance.range}")
	private String giltineAttendanceRange;

	@Bean(name = "giltineAttendanceRange")
	public String getGiltineAttendanceRange() {
		return giltineAttendanceRange;
	}
}
