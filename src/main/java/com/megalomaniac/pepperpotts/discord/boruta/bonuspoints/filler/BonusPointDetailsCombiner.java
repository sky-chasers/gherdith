package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.BorutaLocation;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.GiltineLocation;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.BorutaAttendanceDetails;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.GiltineAttendanceDetails;
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPoints;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@Service
public class BonusPointDetailsCombiner {

	public List<TeamBonusPointDetails> combineDetails(List<BorutaAttendanceDetails> borutaAttendanceDetails,
													  List<GiltineAttendanceDetails> giltineAttendanceDetails,
													  List<RaidBonusPoints> raidBonusPointsList,
													  int borutaSessionCount,
													  int giltineSessionCount) {

		List<TeamBonusPointDetails> output = new ArrayList<>();
		List<String> allBorutaTeamNames = borutaAttendanceDetails.stream()
				.map(BorutaAttendanceDetails::getTeamName)
				.collect(Collectors.toList());

		List<String> allGiltineTeamNames = giltineAttendanceDetails.stream()
				.map(GiltineAttendanceDetails::getTeamName)
				.collect(Collectors.toList());

		Set<String> teamNames = Stream.of(allBorutaTeamNames, allGiltineTeamNames)
				.flatMap(List::stream)
				.collect(Collectors.toSet());

		for (String teamName: teamNames) {
			BorutaAttendanceDetails borutaAttendance = findBorutaAttendanceOfTeamName(borutaAttendanceDetails, teamName, borutaSessionCount);
			GiltineAttendanceDetails giltineAttendance = findGiltineAttendanceOfTeamName(giltineAttendanceDetails, teamName, giltineSessionCount);
			RaidBonusPoints raidBonusPoints = findHighestBonusPointsOfPlayer(raidBonusPointsList, teamName);

			TeamBonusPointDetails combinedDetails = new TeamBonusPointDetails();
			combinedDetails.setTeamName(teamName);
			combinedDetails.setBorutaAttendanceDetails(borutaAttendance);
			combinedDetails.setGiltineAttendanceDetails(giltineAttendance);
			combinedDetails.setRaidBonusPoints(raidBonusPoints);

			output.add(combinedDetails);
		}

		return output;
	}

	private BorutaAttendanceDetails findBorutaAttendanceOfTeamName(List<BorutaAttendanceDetails> borutaAttendanceDetails,
																   String teamName,
																   int borutaSessionCount) {

		Optional<BorutaAttendanceDetails> result = borutaAttendanceDetails.stream()
				.filter(details -> details.getTeamName().equals(teamName))
				.findFirst();

		if (result.isEmpty()) {
			BorutaAttendanceDetails blankDetails = new BorutaAttendanceDetails();
			blankDetails.setTeamName(teamName);
			blankDetails.setScore(0);
			blankDetails.setLocations(createBlankBorutaLocations(borutaSessionCount));

			return blankDetails;
		}

		return result.get();
	}

	private GiltineAttendanceDetails findGiltineAttendanceOfTeamName(List<GiltineAttendanceDetails> giltineAttendanceDetails,
																	 String teamName,
																	 int giltineSessionCount) {
		Optional<GiltineAttendanceDetails> result = giltineAttendanceDetails.stream()
				.filter(details -> details.getTeamName().equals(teamName))
				.findFirst();

		if (result.isEmpty()) {
			GiltineAttendanceDetails blankDetails = new GiltineAttendanceDetails();
			blankDetails.setTeamName(teamName);
			blankDetails.setScore(0);
			blankDetails.setLocations(createBlankGiltineLocations(giltineSessionCount));

			return blankDetails;
		}

		return result.get();
	}

	private List<BorutaLocation> createBlankBorutaLocations(int borutaSessionCount) {
		List<BorutaLocation> blankLocations = new ArrayList<>();

		for (int index = 0; index < borutaSessionCount + 1; index++) {
			blankLocations.add(BorutaLocation.NOT_APPLICABLE);
		}

		return blankLocations;
	}

	private List<GiltineLocation> createBlankGiltineLocations(int giltineSessionCount) {
		List<GiltineLocation> blankLocations = new ArrayList<>();

		for (int index = 0; index < giltineSessionCount + 1; index++) {
			blankLocations.add(GiltineLocation.NOT_APPLICABLE);
		}

		return blankLocations;
	}

	private RaidBonusPoints findHighestBonusPointsOfPlayer(List<RaidBonusPoints> raidBonusPointsList, String teamName) {
		try {

			List<RaidBonusPoints> sortedBonusPoints = raidBonusPointsList.stream()
					.filter(borutaBonusPoint -> borutaBonusPoint.getTeamName().equals(teamName))
					.sorted(Comparator.comparing(RaidBonusPoints::getMaxPossibleBonusPoints).reversed())
					.collect(Collectors.toList());

			List<BigDecimal> bonusPoints = sortedBonusPoints.stream()
					.map(RaidBonusPoints::getMaxPossibleBonusPoints)
					.collect(Collectors.toList());

			return sortedBonusPoints.get(0);
		} catch (IndexOutOfBoundsException e) {
			RaidBonusPoints raidBonusPoints = new RaidBonusPoints();
			raidBonusPoints.setTeamName(teamName);
			raidBonusPoints.setMaxPossibleBonusPoints(BigDecimal.ZERO);
			return raidBonusPoints;
		}

	}
}
