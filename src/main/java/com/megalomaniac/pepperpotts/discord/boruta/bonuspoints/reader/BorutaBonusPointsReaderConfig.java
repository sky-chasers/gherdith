package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BorutaBonusPointsReaderConfig {

	@Value("${megalomaniac.pepperpotts.googleSheets.bonusPointsSheets.sheetId}")
	private String bonusPointsSheetId;

	@Bean(name = "bonusPointsSheetId")
	public String getBonusPointsSheetId() {
		return bonusPointsSheetId;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.bonusPointsSheets.borutaCharactersRange}")
	private String bonusPointsCharactersRange;

	@Bean(name = "bonusPointsCharactersRange")
	public String getBonusPointsCharactersRange() {
		return bonusPointsCharactersRange;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.bonusPointsSheets.bonusPointsBiddingPointsLabel}")
	private String bonusPointsBiddingPointsLabel;

	@Bean(name = "bonusPointsBiddingPointsLabel")
	public String getBonusPointsBiddingPointsLabel() {
		return bonusPointsBiddingPointsLabel;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.bonusPointsSheets.teamNameLabel}")
	private String bonusPointsTeamNameLabel;

	@Bean(name = "bonusPointsTeamNameLabel")
	public String getBonusPointsTeamNameLabel() {
		return bonusPointsTeamNameLabel;
	}
}
