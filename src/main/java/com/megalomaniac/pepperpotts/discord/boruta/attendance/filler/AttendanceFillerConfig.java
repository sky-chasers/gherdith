package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AttendanceFillerConfig {

	@Value("${megalomaniac.pepperpotts.googleSheets.borutaAttendanceTabName}")
	private String borutaAttendanceTabName;

	@Bean(name = "borutaAttendanceTabName")
	public String getBorutaAttendanceTabName() {
		return borutaAttendanceTabName;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.giltineAttendanceTabName}")
	private String giltineAttendanceTabName;

	@Bean(name = "giltineAttendanceTabName")
	public String getGiltineAttendanceTabName() {
		return giltineAttendanceTabName;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.borutaAttendanceInputRange}")
	private String borutaAttendanceInputRange;

	@Bean("borutaAttendanceInputRange")
	public String getBorutaAttendanceInputRange() {
		return borutaAttendanceInputRange;
	}

	@Value("${megalomaniac.pepperpotts.googleSheets.giltineAttendanceInputRange}")
	private String giltineAttendanceInputRange;

	@Bean("giltineAttendanceInputRange")
	public String getGiltineAttendanceInputRange() {
		return giltineAttendanceInputRange;
	}

}
