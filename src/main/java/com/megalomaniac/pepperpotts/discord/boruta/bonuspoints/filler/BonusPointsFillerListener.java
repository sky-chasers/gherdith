package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import com.megalomaniac.pepperpotts.discord.command.AdminCommand;
import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

@Component
public class BonusPointsFillerListener implements MessageCreateListener {

	private final CommandValidator validator;
	private final BonusPointsFillerService bonusPointsFillerService;
	private final AdminCommand command = AdminCommand.FILL_BORUTA_BONUS_POINTS;

	public BonusPointsFillerListener(CommandValidator validator,
									 BonusPointsFillerService bonusPointsFillerService) {
		this.validator = validator;
		this.bonusPointsFillerService = bonusPointsFillerService;
	}

	@Override
	public void onMessageCreate(MessageCreateEvent messageCreateEvent) {

		if (!validator.isValidCommand(messageCreateEvent, command)) {
			return;
		}

		FillBonusPointsCommand fillBonusPointsCommand = new FillBonusPointsCommand(messageCreateEvent.getMessage());
		bonusPointsFillerService.fillBonusPoints(fillBonusPointsCommand);
	}
}
