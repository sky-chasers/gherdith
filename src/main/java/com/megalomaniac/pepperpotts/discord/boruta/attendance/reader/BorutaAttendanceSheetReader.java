package com.megalomaniac.pepperpotts.discord.boruta.attendance.reader;

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.BorutaLocation;
import com.megalomaniac.pepperpotts.google.sheets.SheetReader;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class BorutaAttendanceSheetReader {

	private final SheetReader sheetReader;
	private final String borutaAttendanceRange;

	public BorutaAttendanceSheetReader(SheetReader sheetReader, String borutaAttendanceRange) {
		this.sheetReader = sheetReader;
		this.borutaAttendanceRange = borutaAttendanceRange;
	}

	public List<BorutaAttendanceDetails> fetchBorutaAttendance(String googleSheetId) throws FailedToFetchBorutaAttendanceException {
		try {
			List<BorutaAttendanceDetails> details = new ArrayList<>();
			List<List<Object>> rawOutput = sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, borutaAttendanceRange);

			for (List row : rawOutput) {
				String teamName = row.get(0).toString();
				Integer score = Integer.valueOf(row.get(1).toString());
				int maxColumnCount = row.size();
				List<BorutaLocation> locations = new ArrayList<>();

				for (Object rawLocation : row.subList(2, maxColumnCount)) {
					Optional<BorutaLocation> borutaLocation = BorutaLocation.findLocationByName(rawLocation.toString());

					if (borutaLocation.isPresent()) {
						locations.add(borutaLocation.get());
					} else {
						locations.add(BorutaLocation.NOT_APPLICABLE);
					}

				}

				BorutaAttendanceDetails detail = new BorutaAttendanceDetails();
				detail.setTeamName(teamName);
				detail.setScore(score);
				detail.setLocations(locations);

				details.add(detail);
			}

			return details;
		} catch (IOException e) {
			log.error("Failed to fetch boruta attendance with id: {} and range: {}", googleSheetId, borutaAttendanceRange);
			log.error(e.getMessage());
			throw new FailedToFetchBorutaAttendanceException();
		}
	}
}
