package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class RaidTypeInputRangeMapper {

	private Map<RaidType, String> inputRangeMap;

	public RaidTypeInputRangeMapper(String borutaAttendanceInputRange, String giltineAttendanceInputRange) {
		this.inputRangeMap = new HashMap<>();
		this.inputRangeMap.put(RaidType.BORUTA, borutaAttendanceInputRange);
		this.inputRangeMap.put(RaidType.GILTINE, giltineAttendanceInputRange);
	}

	public String getInputRangeForRaidType(RaidType raidType) {
		return inputRangeMap.get(raidType);
	}

}
