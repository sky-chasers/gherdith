package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader;

import com.megalomaniac.pepperpotts.google.sheets.SheetReader;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Log4j2
@Service
public class RaidBonusPointsReader {

	private final SheetReader sheetReader;
	private final String bonusPointsSheetId;
	private final String bonusPointsCharactersRange;
	private final String bonusPointsBiddingPointsLabel;
	private final String bonusPointsTeamNameLabel;

	public RaidBonusPointsReader(SheetReader sheetReader,
								 String bonusPointsSheetId,
								 String bonusPointsCharactersRange,
								 String bonusPointsBiddingPointsLabel,
								 String bonusPointsTeamNameLabel) {
		this.sheetReader = sheetReader;
		this.bonusPointsSheetId = bonusPointsSheetId;
		this.bonusPointsCharactersRange = bonusPointsCharactersRange;
		this.bonusPointsBiddingPointsLabel = bonusPointsBiddingPointsLabel;
		this.bonusPointsTeamNameLabel = bonusPointsTeamNameLabel;
	}

	public List<RaidBonusPoints> fetchBonusPointsDetails() throws FailedToFetchBonusPointsSheetException {

		try {
			List<List<Object>> rawOutput = sheetReader.fetchGoogleSheetWithIdAndRange(bonusPointsSheetId, bonusPointsCharactersRange);
			List<String> labels = rawOutput.get(0).stream()
					.map(object -> Objects.toString(object, null))
					.collect(Collectors.toList());

			int bonusPointsIndex = labels.indexOf(bonusPointsBiddingPointsLabel);
			int teamNameIndex = labels.indexOf(bonusPointsTeamNameLabel);
			List<RaidBonusPoints> output = new ArrayList<>();

			for (List <Object> row : rawOutput.subList(1, rawOutput.size())) {
				String teamName = row.get(teamNameIndex).toString();
				BigDecimal bonusPoints = getBonusPointsFromRow(row, bonusPointsIndex);

				if (teamName.length() > 0) {
					RaidBonusPoints raidBonusPoints = new RaidBonusPoints();
					raidBonusPoints.setTeamName(teamName);
					raidBonusPoints.setMaxPossibleBonusPoints(bonusPoints);

					output.add(raidBonusPoints);
				}
			}

			return output;
		} catch (IOException e) {
			log.error("Failed to fetch Boruta bonus points sheet with id: {} due to the following error: ", bonusPointsSheetId);
			log.error(e.getMessage());
			throw new FailedToFetchBonusPointsSheetException();
		}
	}

	private BigDecimal getBonusPointsFromRow(List<Object> row, int bonusPointsIndex) {
		try {
			return new BigDecimal(row.get(bonusPointsIndex).toString());
		} catch (NumberFormatException | IndexOutOfBoundsException | NullPointerException e) {
			return BigDecimal.ZERO;
		}
	}
}
