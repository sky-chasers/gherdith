package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import lombok.Getter;
import lombok.Setter;
import org.javacord.api.entity.message.Message;

public class FillBonusPointsCommand {

	@Getter
	@Setter
	private String command;

	@Getter
	@Setter
	private Message message;

	@Setter
	@Getter
	private String attendanceSheetId;

	public FillBonusPointsCommand(Message message) {
		String rawCommand = message.getContent();
		String[] splitCommand = rawCommand.split(" ");

		this.message = message;
		this.command = splitCommand[0];
		this.attendanceSheetId = splitCommand[1];
	}
}
