package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.FailedToReadBiddingPointsException;
import com.megalomaniac.pepperpotts.discord.biddingpoints.reader.LabelNotFoundException;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.*;
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPoints;
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPointsReader;
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.FailedToFetchBonusPointsSheetException;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class BonusPointsFillerService {

	private final BorutaAttendanceSheetReader borutaAttendanceSheetReader;
	private final MessageSender messageSender;
	private final BonusPointsCalculator bonusPointsCalculator;
	private final RaidBonusPointsReader raidBonusPointsReader;
	private final BonusPointDetailsCombiner detailsCombiner;
	private final BonusPointsWriter bonusPointsWriter;
	private final GiltineAttendanceSheetReader giltineAttendanceSheetReader;
	private final SessionCountCalculator sessionCountCalculator;

	public BonusPointsFillerService(BorutaAttendanceSheetReader borutaAttendanceSheetReader,
									MessageSender messageSender,
									BonusPointsCalculator bonusPointsCalculator,
									RaidBonusPointsReader raidBonusPointsReader,
									BonusPointDetailsCombiner detailsCombiner,
									BonusPointsWriter bonusPointsWriter,
									GiltineAttendanceSheetReader giltineAttendanceSheetReader,
									SessionCountCalculator sessionCountCalculator) {
		this.borutaAttendanceSheetReader = borutaAttendanceSheetReader;
		this.messageSender = messageSender;
		this.bonusPointsCalculator = bonusPointsCalculator;
		this.raidBonusPointsReader = raidBonusPointsReader;
		this.detailsCombiner = detailsCombiner;
		this.bonusPointsWriter = bonusPointsWriter;
		this.giltineAttendanceSheetReader = giltineAttendanceSheetReader;
		this.sessionCountCalculator = sessionCountCalculator;
	}

	public void fillBonusPoints(FillBonusPointsCommand command) {
		String googleSheetId = command.getAttendanceSheetId();

		try {
			List<BorutaAttendanceDetails> borutaAttendanceList = borutaAttendanceSheetReader.fetchBorutaAttendance(googleSheetId);
			List<GiltineAttendanceDetails> giltineAttendanceList = giltineAttendanceSheetReader.fetchGiltineAttendance(googleSheetId);
			List<RaidBonusPoints> raidBonusPoints = raidBonusPointsReader.fetchBonusPointsDetails();

			List<BonusPointsOutput> output = new ArrayList<>();
			int borutaSessionCount = sessionCountCalculator.calculateBorutaSessionCount(borutaAttendanceList);
			int giltineSessionCount = sessionCountCalculator.calculateGiltineSessionCount(giltineAttendanceList);

			List<TeamBonusPointDetails> teamBonusPointDetails = detailsCombiner
					.combineDetails(borutaAttendanceList, giltineAttendanceList, raidBonusPoints, giltineSessionCount, borutaSessionCount);

			for (TeamBonusPointDetails userDetail : teamBonusPointDetails) {
				BonusPointsOutput bonusPointsOutput = new BonusPointsOutput();
				bonusPointsOutput.setTeamName(userDetail.getTeamName());
				bonusPointsOutput.setBonusPoints(bonusPointsCalculator.calculateBonusPoints(userDetail, borutaSessionCount, giltineSessionCount));

				output.add(bonusPointsOutput);
			}

			bonusPointsWriter.writeBonusPointsToGoogleSheetWithId(googleSheetId, output);
			messageSender.sendSuccessMessage(command.getMessage(), generateSuccessMessage(googleSheetId, borutaSessionCount, giltineSessionCount));

		} catch (FailedToFetchBorutaAttendanceException e) {
			messageSender.sendErrorMessage(command.getMessage(), "Failed to fetch Boruta attendance sheet with id: " + googleSheetId);
		} catch (FailedToFetchBonusPointsSheetException e) {
			messageSender.sendErrorMessage(command.getMessage(), "Failed to fetch Boruta bonus points sheet.");
		} catch (FailedToReadBiddingPointsException e) {
			messageSender.sendErrorMessage(command.getMessage(), "Failed to read bidding points sheet with id: " + googleSheetId + " Please make sure it's in the correct format.");
		} catch (FailedToWriteBonusPointsException e) {
			messageSender.sendErrorMessage(command.getMessage(), "Failed to write bonus bidding points to sheet with id: " + googleSheetId);
		} catch (LabelNotFoundException e) {
			messageSender.sendErrorMessage(command.getMessage(), "Failed to read google sheet with id: " + googleSheetId + ". Label: **" + e.getOffendingLabel() + "** does not exist.");
		} catch (FailedToFetchGiltineAttendanceException e) {
			messageSender.sendErrorMessage(command.getMessage(), "Failed to fetch Giltine sheet with id: " + googleSheetId);
		}
	}

	private String generateSuccessMessage(String googleSheetId, int borutaSessionCount, int giltineSessionCount) {
		String link = generateLink(googleSheetId);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Successfully updated the bonus points. Click [here]("+ link +") to view the results. \n");
		stringBuilder.append("Details: \n");
		stringBuilder.append("Boruta Session Count: " + borutaSessionCount + "\n");
		stringBuilder.append("Giltine Session Count: "  + giltineSessionCount);
		return stringBuilder.toString();
	}

	private String generateLink(String googleSheetId) {
		return "https://docs.google.com/spreadsheets/d/"+ googleSheetId +"/edit?usp=sharing";
	}
}
