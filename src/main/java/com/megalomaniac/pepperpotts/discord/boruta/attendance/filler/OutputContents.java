package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import lombok.Getter;
import lombok.Setter;

public class OutputContents {

	@Getter
	@Setter
	private String name;

	@Getter
	@Setter
	private String nextEmptyRange;
}
