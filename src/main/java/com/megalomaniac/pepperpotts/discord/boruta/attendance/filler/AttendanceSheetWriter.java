package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import com.google.api.services.sheets.v4.model.ValueRange;
import com.megalomaniac.pepperpotts.google.sheets.SheetWriter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Log4j2
@Service
public class AttendanceSheetWriter {

	private final AttendanceOutputReader outputReader;
	private final SheetWriter sheetWriter;
	private final RaidTypeOutputRangeMapper raidTypeOutputRangeMapper;

	public AttendanceSheetWriter(AttendanceOutputReader outputReader, SheetWriter sheetWriter, RaidTypeOutputRangeMapper raidTypeOutputRangeMapper) {
		this.outputReader = outputReader;
		this.sheetWriter = sheetWriter;
		this.raidTypeOutputRangeMapper = raidTypeOutputRangeMapper;
	}

	public void recordAttendanceToGoogleSheetWithId(String outputGoogleSheetId, List<InputContents> inputContents, RaidType raidType) throws AttendanceSheetWriterException {
		try {
			List<OutputContents> outputContents = outputReader.fetchGoogleSheetWithId(outputGoogleSheetId, raidType);
			List<ValueRange> valuesToWrite = new ArrayList<>();

			for (OutputContents outputContent : outputContents) {
				Optional<InputContents> inputContent = inputContents
						.stream()
						.filter(content -> content.getName().equals(outputContent.getName()))
						.findFirst();

				String DEFAULT_VALUE = "N/A";
				String value = inputContent.isPresent() ? inputContent.get().getLocation() : DEFAULT_VALUE;

				List<List<Object>> body = Arrays.asList(
						Collections.singletonList((Object) value)
				);

				String tabName = raidTypeOutputRangeMapper.getTabNameForRaidType(raidType);
				String range = tabName + "!" + outputContent.getNextEmptyRange();

				ValueRange valueRange = new ValueRange();
				valueRange.setRange(range);
				valueRange.setValues(body);
				valuesToWrite.add(valueRange);
			}

			sheetWriter.update(outputGoogleSheetId, valuesToWrite);
		} catch (IOException e) {
			log.error("Failed to write attendance sheet on google sheet with id: {}", outputGoogleSheetId);
			throw new AttendanceSheetWriterException();
		}
	}

}
