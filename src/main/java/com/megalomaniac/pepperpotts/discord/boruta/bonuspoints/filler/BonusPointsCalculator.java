package com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler;

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.BorutaLocation;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.GiltineLocation;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.BorutaAttendanceDetails;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.reader.GiltineAttendanceDetails;
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.reader.RaidBonusPoints;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Log4j2
@Service
public class BonusPointsCalculator {

	public int calculateBonusPoints(TeamBonusPointDetails bonusPointDetails, int borutaSessionCount, int giltineSessionCount) {
		BorutaAttendanceDetails borutaAttendanceDetails = bonusPointDetails.getBorutaAttendanceDetails();
		GiltineAttendanceDetails giltineAttendanceDetails = bonusPointDetails.getGiltineAttendanceDetails();
		RaidBonusPoints raidBonusPoints = bonusPointDetails.getRaidBonusPoints();

		int borutaBonusPoints = calculateBorutaAttendanceBonusPoints(borutaAttendanceDetails, raidBonusPoints, borutaSessionCount);
		int giltineBonusPoints = calculateGiltineAttendanceBonusPoints(giltineAttendanceDetails, raidBonusPoints, giltineSessionCount);

		if (borutaBonusPoints > giltineBonusPoints) {
			return borutaBonusPoints;
		}

		return giltineBonusPoints;
	}

	private int calculateGiltineAttendanceBonusPoints(GiltineAttendanceDetails attendanceDetails,
													  RaidBonusPoints raidBonusPoints,
													  int numberOfSessions) {

		BigDecimal percentageScore = BigDecimal.ZERO;
		BigDecimal bonusPoints = BigDecimal.ZERO;
		List<GiltineLocation> validGiltineRaidLocations = GiltineLocation.getValidGiltineRaidLocations();
		List<GiltineLocation> userLocations = attendanceDetails.getLocations();

		if (!userLocations.isEmpty() && raidBonusPoints.getMaxPossibleBonusPoints() != null) {
			BigDecimal percentagePerDay = new BigDecimal(100/numberOfSessions);

			for (GiltineLocation location: userLocations) {
				if (validGiltineRaidLocations.contains(location)) {
					percentageScore = percentageScore.add(percentagePerDay);
				}
			}

			bonusPoints = calculateTentativeBonusPoints(raidBonusPoints.getMaxPossibleBonusPoints(), percentageScore);
		}

		return bonusPoints.setScale(0, RoundingMode.UP).intValue();
	}

	private int calculateBorutaAttendanceBonusPoints(BorutaAttendanceDetails attendanceDetails,
													 RaidBonusPoints raidBonusPoints,
													 int numberOfSessions) {
		BigDecimal percentageScore = BigDecimal.ZERO;
		BigDecimal bonusPoints = BigDecimal.ZERO;
		List<BorutaLocation> validBorutaLocations = BorutaLocation.getValidBorutaLocations();
		List<BorutaLocation> userLocations = attendanceDetails.getLocations();

		if (!userLocations.isEmpty() && raidBonusPoints.getMaxPossibleBonusPoints() != null) {
			BigDecimal percentagePerDay = new BigDecimal(100/numberOfSessions);

			for (BorutaLocation location: userLocations) {
				if (validBorutaLocations.contains(location)) {
					percentageScore = percentageScore.add(percentagePerDay);
				}
			}

			bonusPoints = calculateTentativeBonusPoints(raidBonusPoints.getMaxPossibleBonusPoints(), percentageScore);
		}

		return bonusPoints.setScale(0, RoundingMode.UP).intValue();
	}

	private BigDecimal calculateTentativeBonusPoints(BigDecimal maxPossibleBonusPoints, BigDecimal percentageScore) {
		return ((percentageScore.divide(new BigDecimal(100))).multiply(maxPossibleBonusPoints));
	}

}
