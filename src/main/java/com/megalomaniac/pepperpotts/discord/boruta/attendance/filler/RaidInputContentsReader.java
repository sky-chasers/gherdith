package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RaidInputContentsReader {

	public List<InputContents> readLocationsWithRaidType(RaidType raidType, List<List<Object>> response) {
		if (raidType.equals(RaidType.BORUTA)) {
			return readBorutaLocations(response);
		}

		if (raidType.equals(RaidType.GILTINE)) {
			return readGiltineLocations(response);
		}

		return new ArrayList<>();
	}

	private List<InputContents> readBorutaLocations(List<List<Object>> response) {
		List<InputContents> transformedContents = new ArrayList<>();

		for (List row : response) {
			String name = row.get(0).toString();
			InputContents inputContent = initializeInputContents(name);
			List<BorutaLocation> locations = new ArrayList<>();

			for(Object locationObject : row) {
				Optional<BorutaLocation> temporaryLocation = BorutaLocation.findLocationByName(locationObject.toString());
				temporaryLocation.ifPresent(locations::add);
			}

			if (locations.contains(BorutaLocation.SULIVINAS_LAIR)){
				inputContent.setLocation(BorutaLocation.SULIVINAS_LAIR.getLocationName());
			}

			transformedContents.add(inputContent);
		}

		return transformedContents;
	}

	private List<InputContents> readGiltineLocations(List<List<Object>> response) {
		List<InputContents> transformedContents = new ArrayList<>();

		for (List row : response) {
			String name = row.get(0).toString();
			InputContents inputContent = initializeInputContents(name);
			List<GiltineLocation> locations = new ArrayList<>();

			for(Object locationObject : row) {
				Optional<GiltineLocation> temporaryLocation = GiltineLocation.findLocationByName(locationObject.toString());
				temporaryLocation.ifPresent(locations::add);
			}

			if (locations.contains(GiltineLocation.DEMONIC_SANCTUARY)) {
				inputContent.setLocation(GiltineLocation.DEMONIC_SANCTUARY.getLocationName());
			}

			transformedContents.add(inputContent);
		}

		return transformedContents;
	}

	private InputContents initializeInputContents(String name) {
		InputContents inputContent = new InputContents();
		inputContent.setName(name);
		inputContent.setLocation("N/A");

		return inputContent;
	}
}
