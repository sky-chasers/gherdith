package com.megalomaniac.pepperpotts.discord.boruta.attendance.filler;

import com.megalomaniac.pepperpotts.discord.command.AdminCommand;
import com.megalomaniac.pepperpotts.discord.command.CommandValidator;
import com.megalomaniac.pepperpotts.discord.message.MessageSender;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.user.User;
import org.javacord.api.event.message.MessageCreateEvent;
import org.javacord.api.listener.message.MessageCreateListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Log4j2
@Component
public class AttendanceFillerListener implements MessageCreateListener {

	private final CommandValidator validator;
	private final AttendanceFillerService attendanceFillerService;
	private final MessageSender messageSender;
	private final AdminCommand command = AdminCommand.FILL_RAID_ATTENDANCE;

	public AttendanceFillerListener(CommandValidator validator,
									AttendanceFillerService attendanceFillerService, MessageSender messageSender) {
		this.validator = validator;
		this.attendanceFillerService = attendanceFillerService;
		this.messageSender = messageSender;
	}

	@Override
	public void onMessageCreate(MessageCreateEvent messageCreateEvent) {
		if (!validator.isValidCommand(messageCreateEvent, command)) {
			return;
		}

		Optional<User> recipient = messageCreateEvent.getMessageAuthor().asUser();

		if (recipient.isPresent()) {

			try {
				AttendanceFillerCommand attendanceFillerCommand = new AttendanceFillerCommand(messageCreateEvent.getMessage());
				attendanceFillerService.fillAttendance(attendanceFillerCommand);
			} catch (IllegalArgumentException e) {
				String errorMessage = "Invalid raid type. Valid raid types are `giltine` and `boruta`.";
				messageSender.sendErrorMessage(messageCreateEvent.getMessage(), errorMessage);
			}
		}
	}

}
