package com.megalomaniac.pepperpotts.discord.boruta.attendance.reader;

import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.GiltineLocation;
import com.megalomaniac.pepperpotts.google.sheets.SheetReader;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
public class GiltineAttendanceSheetReader {

	private final SheetReader sheetReader;
	private final String giltineAttendanceRange;


	public GiltineAttendanceSheetReader(SheetReader sheetReader, String giltineAttendanceRange) {
		this.sheetReader = sheetReader;
		this.giltineAttendanceRange = giltineAttendanceRange;
	}

	public List<GiltineAttendanceDetails> fetchGiltineAttendance(String googleSheetId) throws FailedToFetchGiltineAttendanceException {

		try {
			List<GiltineAttendanceDetails> attendanceDetails = new ArrayList<>();
			List<List<Object>> rawOutput = sheetReader.fetchGoogleSheetWithIdAndRange(googleSheetId, giltineAttendanceRange);

			for (List row : rawOutput) {
				Integer score = Integer.valueOf(row.get(1).toString());
				String teamName = row.get(0).toString();
				int maxColumnCount = row.size();
				List<GiltineLocation> locations = new ArrayList<>();

				for (Object rawLocation : row.subList(2, maxColumnCount)) {
					Optional<GiltineLocation> giltineLocation = GiltineLocation.findLocationByName(rawLocation.toString());

					if (giltineLocation.isPresent()) {
						locations.add(giltineLocation.get());
					} else {
						locations.add(GiltineLocation.NOT_APPLICABLE);
					}

					GiltineAttendanceDetails giltineAttendanceDetails = new GiltineAttendanceDetails();
					giltineAttendanceDetails.setScore(score);
					giltineAttendanceDetails.setTeamName(teamName);
					giltineAttendanceDetails.setLocations(locations);

					attendanceDetails.add(giltineAttendanceDetails);
				}
			}

			return attendanceDetails;
		} catch (IOException e) {
			log.error("Failed to fetch giltine attendance with sheetId: {}.", googleSheetId);
			log.error(e.getMessage());
			throw new FailedToFetchGiltineAttendanceException();
		}
	}
}
