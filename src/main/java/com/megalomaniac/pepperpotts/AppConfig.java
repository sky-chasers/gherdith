package com.megalomaniac.pepperpotts;

import lombok.extern.log4j.Log4j2;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.permission.PermissionType;
import org.javacord.api.entity.permission.Permissions;
import org.javacord.api.entity.permission.PermissionsBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

@Log4j2
@EnableTransactionManagement
@Configuration
public class AppConfig {

	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Value("${megalomaniac.pepperpotts.discord.api.token}")
	private String pepperpottsDiscordApiToken;

	@Bean("pepperpottsDiscordApiToken")
	public String getPepperpottsDiscordApiToken() {
		return pepperpottsDiscordApiToken;
	}

	@Bean("pepperPotDiscordApi")
	public DiscordApi getDiscordApi() {
		Permissions permissions = new PermissionsBuilder()
				.setAllowed(PermissionType.SEND_MESSAGES)
				.build();

		DiscordApi discordApi = new DiscordApiBuilder()
				.setToken(pepperpottsDiscordApiToken)
				.login()
				.join();

		log.info("Pepper Potts is running...");
		log.info("Invitation link: {}", discordApi.createBotInvite(permissions));

		return discordApi;
	}
}
