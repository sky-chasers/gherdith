package com.megalomaniac.pepperpotts;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Log4j2
@Configuration
public class RolesConfig {

	@Value("${megalomaniac.pepperpotts.discord.roles.itos}")
	private String itos;

	@Value("${megalomaniac.pepperpotts.discord.roles.ktos}")
	private String ktos;

	@Value("${megalomaniac.pepperpotts.discord.roles.ktosdevblog}")
	private String ktosdevblog;

	@Value("${megalomaniac.pepperpotts.discord.roles.ktest}")
	private String ktest;

	@Bean("itosRoleId")
	public String getItos() {
		return itos;
	}

	@Bean("ktosRoleId")
	public String getKtos() {
		return ktos;
	}

	@Bean("ktosdevblogRoleId")
	public String getKtosDevBlog() {
		return ktosdevblog;
	}

	@Bean("ktestRoleId")
	public String getKtest() {
		return ktest;
	}
}
