package com.megalomaniac.pepperpotts.db.character;

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount;
import com.megalomaniac.pepperpotts.utils.DateUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CharacterService {

	private final DateUtils dateUtils;
	private final CharacterRepository characterRepository;

	public CharacterService(DateUtils dateUtils,
							CharacterRepository characterRepository) {
		this.dateUtils = dateUtils;
		this.characterRepository = characterRepository;
	}

	public void updateTeamNames(TosAccount tosAccount, List<String> teamNames) {

		teamNames.forEach(name -> {
			Optional<Character> optionalCharacter = characterRepository.findByTosAccountAndTeamName(tosAccount, name);

			if (optionalCharacter.isEmpty()) {
				Character character = new Character();
				character.setTeamName(name);
				character.setDateUpdated(dateUtils.getCurrentDateTime());
				character.setTosAccount(tosAccount);

				characterRepository.save(character);
			}
		});

	}

}
