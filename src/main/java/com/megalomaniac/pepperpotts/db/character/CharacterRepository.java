package com.megalomaniac.pepperpotts.db.character;

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
	Optional<Character> findByTosAccountAndTeamName(TosAccount tosAccount, String teamName);
	Optional<Character> findByTeamName(String teamName);
}
