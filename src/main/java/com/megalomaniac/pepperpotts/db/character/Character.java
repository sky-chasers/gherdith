package com.megalomaniac.pepperpotts.db.character;

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@EqualsAndHashCode(of = "id")
public class Character {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "character_id_seq")
	@SequenceGenerator(name = "character_id_seq", sequenceName = "character_id_seq", allocationSize = 1)
	private Long id;

	@ManyToOne(targetEntity = TosAccount.class)
	private TosAccount tosAccount;

	private LocalDateTime dateUpdated;

	@NotNull
	@NotBlank
	private String teamName;
}
