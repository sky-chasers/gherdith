package com.megalomaniac.pepperpotts.db.demonlord;

import com.megalomaniac.pepperpotts.ghervis.api.DemonLord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DemonLordRepository extends JpaRepository<DemonLordSpawnDetails, Long> {

	Optional<DemonLordSpawnDetails> findByName(DemonLord name);

}
