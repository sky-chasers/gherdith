package com.megalomaniac.pepperpotts.db.demonlord;

import com.megalomaniac.pepperpotts.ghervis.api.DemonLord;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Transactional
@Service
public class DemonLordService {

	private final DemonLordRepository demonLordRepository;

	public DemonLordService(DemonLordRepository demonLordRepository) {
		this.demonLordRepository = demonLordRepository;
	}

	public DemonLordSpawnDetails fetchOrCreateDemonLord(DemonLord bossName, LocalDateTime dateTimeSpawned) {
		Optional<DemonLordSpawnDetails> demonLord = demonLordRepository.findByName(bossName);

		if (demonLord.isEmpty()) {
			DemonLordSpawnDetails newDemonLordSpawnDetails = new DemonLordSpawnDetails();
			newDemonLordSpawnDetails.setDateAppeared(dateTimeSpawned);
			newDemonLordSpawnDetails.setName(bossName);
			return newDemonLordSpawnDetails;
		}

		return demonLord.get();
	}

}
