package com.megalomaniac.pepperpotts.db.demonlord;

import com.megalomaniac.pepperpotts.ghervis.api.DemonLord;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Entity
@EqualsAndHashCode(of = "id")
public class DemonLordSpawnDetails {

	@Id
	@Getter
	@Setter
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "demon_lord_spawn_details_id_seq")
	@SequenceGenerator(name = "demon_lord_spawn_details_id_seq", sequenceName = "demon_lord_spawn_details_id_seq", allocationSize = 1)
	private Long id;

	@Getter
	@Setter
	@Enumerated(EnumType.STRING)
	private DemonLord name;

	@Getter
	@Setter
	private LocalDateTime dateAppeared;

	@Getter
	@Setter
	private Long discordTagId;

	public String getServerTimeDate() {
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MMM dd yyyy h:mm:ss a");
		ZonedDateTime zonedDateTime = ZonedDateTime.of(this.dateAppeared, ZoneId.of("Singapore"));
		return zonedDateTime.format(dateFormat);
	}
}
