package com.megalomaniac.pepperpotts.db.bossbalitareporter;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface BossBalitaReporterRepository extends CrudRepository<BossBalitaReporter, Long> {

	public Optional<BossBalitaReporter> findByInGameAccountIdAndKey(String inGameAccountId, String key);
	public Optional<BossBalitaReporter> findByTeamName(String teamName);
}
