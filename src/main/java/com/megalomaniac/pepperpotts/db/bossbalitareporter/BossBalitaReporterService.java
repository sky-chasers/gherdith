package com.megalomaniac.pepperpotts.db.bossbalitareporter;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
public class BossBalitaReporterService {

	private final BossBalitaReporterRepository bossBalitaReporterRepository;

	public BossBalitaReporterService(BossBalitaReporterRepository bossBalitaReporterRepository) {
		this.bossBalitaReporterRepository = bossBalitaReporterRepository;
	}

	public BossBalitaReporter createReporter(String teamName, String inGameAccountId, Long discordUserId) {
		BossBalitaReporter reporter = new BossBalitaReporter();
		reporter.setTeamName(teamName);
		reporter.setKey(generateKey());
		reporter.setInGameAccountId(inGameAccountId);
		reporter.setDiscordUserId(discordUserId);

		bossBalitaReporterRepository.save(reporter);
		return reporter;
	}

	public BossBalitaReporter updateReporterKey(BossBalitaReporter reporter, Long discordUserId) {
		reporter.setKey(generateKey());
		reporter.setDiscordUserId(discordUserId);
		bossBalitaReporterRepository.save(reporter);
		return reporter;
	}

	private String generateKey() {
		return RandomStringUtils.random(20, true, true);
	}
}
