package com.megalomaniac.pepperpotts.db.bossbalitareporter;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@EqualsAndHashCode(of = "id")
public class BossBalitaReporter {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "boss_balita_reporter_id_seq")
	@SequenceGenerator(name = "boss_balita_reporter_id_seq", sequenceName = "boss_balita_reporter_id_seq", allocationSize = 1)
	private Long id;

	private String key;
	private String inGameAccountId;
	private String teamName;
	private Long discordUserId;

}
