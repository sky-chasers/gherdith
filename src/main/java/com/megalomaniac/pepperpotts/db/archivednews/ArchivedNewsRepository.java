package com.megalomaniac.pepperpotts.db.archivednews;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ArchivedNewsRepository extends CrudRepository<ArchivedNews, Long> {

	Optional<ArchivedNews> findByUrlAndNewsSource(String url, NewsSource newsSource);
	List<ArchivedNews> findAllByUrlNotIn(List<String> excludedUrls);
	List<ArchivedNews> findAllByNewsSourceAndUrlNotIn(NewsSource newsSource, List<String> excludedUrls);

}
