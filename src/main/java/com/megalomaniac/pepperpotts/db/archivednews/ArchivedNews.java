package com.megalomaniac.pepperpotts.db.archivednews;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;


@Getter
@Setter
@Entity
@EqualsAndHashCode(of = "id")
public class ArchivedNews {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "archived_news_id_seq")
	@SequenceGenerator(name = "archived_news_id_seq", sequenceName = "archived_news_id_seq", allocationSize = 1)
	private Long id;

	private String url;

	private LocalDate datePublishedByImc;

	@Enumerated(EnumType.STRING)
	@Column(name = "news_source")
	private NewsSource newsSource;
}
