package com.megalomaniac.pepperpotts.db.archivednews;

public enum NewsSource {
	ITOS,
	KTOS,
	KTOS_DEVBLOG,
	KTEST;
}
