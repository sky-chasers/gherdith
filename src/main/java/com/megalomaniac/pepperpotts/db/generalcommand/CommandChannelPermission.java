package com.megalomaniac.pepperpotts.db.generalcommand;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@EqualsAndHashCode(of = "id")
public class CommandChannelPermission {

	@Id
	@Getter
	@Setter
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "command_channel_permission_id_seq")
	@SequenceGenerator(name = "command_channel_permission_id_seq", sequenceName = "command_channel_permission_id_seq", allocationSize = 1)
	private Long id;

	@Getter
	@Setter
	private String command;

	@Getter
	@Setter
	private Long allowedChannel;

}
