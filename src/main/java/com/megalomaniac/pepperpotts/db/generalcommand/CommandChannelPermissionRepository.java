package com.megalomaniac.pepperpotts.db.generalcommand;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommandChannelPermissionRepository extends JpaRepository<CommandChannelPermission, Long> {

	@Query("SELECT permission.allowedChannel FROM CommandChannelPermission permission WHERE permission.command = :command")
	List<Long> findAllByCommand(String command);
}
