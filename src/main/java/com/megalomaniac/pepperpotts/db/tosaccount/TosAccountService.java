package com.megalomaniac.pepperpotts.db.tosaccount;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Log4j2
@Service
public class TosAccountService {

	private final TosAccountRepository tosAccountRepository;

	public TosAccountService(TosAccountRepository tosAccountRepository) {
		this.tosAccountRepository = tosAccountRepository;
	}

	public TosAccount createTosAccount(String accountId) {
		TosAccount tosAccount = new TosAccount();
		tosAccount.setInGameAccountId(accountId);
		tosAccount.setCharacters(new ArrayList<>());
		return tosAccountRepository.save(tosAccount);
	}
}
