package com.megalomaniac.pepperpotts.db.tosaccount;

import com.megalomaniac.pepperpotts.db.character.Character;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Entity
public class TosAccount {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tos_account_id_seq")
	@SequenceGenerator(name = "tos_account_id_seq", sequenceName = "tos_account_id_seq", allocationSize = 1)
	private Long id;

	private String inGameAccountId;

	@OneToMany(targetEntity = Character.class, mappedBy = "tosAccount", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	List<Character> characters;
}
