package com.megalomaniac.pepperpotts.db.tosaccount;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TosAccountRepository extends JpaRepository<TosAccount, Long> {

	Optional<TosAccount> findByInGameAccountId(String inGameAccountId);
}
