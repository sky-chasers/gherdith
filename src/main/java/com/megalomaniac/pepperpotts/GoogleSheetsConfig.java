package com.megalomaniac.pepperpotts;


import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class GoogleSheetsConfig {

	@Value("${megalomaniac.pepperpotts.appname}")
	private String applicationName;

	private static final String CREDENTIALS_FILE_PATH = "/googlesheet-credentials.json";
	private static final String TOKENS_DIRECTORY_PATH = "tokens";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		List<String> SCOPES = new ArrayList<>();
		SCOPES.add(SheetsScopes.DRIVE);
		SCOPES.add(SheetsScopes.DRIVE_FILE);
		SCOPES.add(SheetsScopes.SPREADSHEETS);

		InputStream inputStream = GoogleSheetsConfig.class.getResourceAsStream(CREDENTIALS_FILE_PATH);

		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(inputStream));

		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
				.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
				.setAccessType("offline")
				.build();

		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8080).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

	@Bean(name = "googleSheetsService")
	public Sheets configureGoogleSheetsService() throws GeneralSecurityException, IOException {
		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		return new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
				.setApplicationName(applicationName)
				.build();
	}
}
