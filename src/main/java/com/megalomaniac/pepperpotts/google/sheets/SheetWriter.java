package com.megalomaniac.pepperpotts.google.sheets;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.*;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class SheetWriter {

	private final Sheets googleSheetsService;

	public SheetWriter(Sheets googleSheetsService) {
		this.googleSheetsService = googleSheetsService;
	}

	public AppendValuesResponse write(String spreadSheetId, String range, List<List<Object>> values) throws IOException {
		ValueRange body = new ValueRange();
		body.setValues(values);

		return googleSheetsService
				.spreadsheets()
				.values()
				.append(spreadSheetId, range, body)
				.setValueInputOption("USER_ENTERED")
				.execute();

	}

	public BatchUpdateValuesResponse update(String spreadSheetId, List<ValueRange> values) throws IOException {
		BatchUpdateValuesRequest body = new BatchUpdateValuesRequest()
				.setValueInputOption("USER_ENTERED")
				.setData(values);

		return googleSheetsService
				.spreadsheets()
				.values()
				.batchUpdate(spreadSheetId, body)
				.execute();
	}

	public ClearValuesResponse clear(String spreadSheetId, String range) throws IOException {
		ClearValuesRequest requestBody = new ClearValuesRequest();
		return googleSheetsService
				.spreadsheets()
				.values()
				.clear(spreadSheetId, range, requestBody).execute();
	}
}
