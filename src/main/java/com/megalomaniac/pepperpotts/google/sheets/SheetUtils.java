package com.megalomaniac.pepperpotts.google.sheets;

import org.springframework.stereotype.Service;

@Service
public class SheetUtils {

	public String constructUrlWithId(String sheetId) {
		return "https://docs.google.com/spreadsheets/d/" + sheetId + "/edit?usp=sharing";
	}
}
