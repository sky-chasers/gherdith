package com.megalomaniac.pepperpotts.google.sheets;

import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.model.SpreadsheetProperties;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class SheetReader {

	private final Sheets googleSheetsService;

	public SheetReader(Sheets googleSheetsService) {
		this.googleSheetsService = googleSheetsService;
	}

	public List<List<Object>> fetchGoogleSheetWithIdAndRange(String spreadSheetId, String range) throws IOException {
		return googleSheetsService
				.spreadsheets()
				.values()
				.get(spreadSheetId, range)
				.execute()
				.getValues();
	}

	public SpreadsheetProperties fetchGoogleSheetPropertiesWithId(String spreadSheetId) throws IOException {
		return googleSheetsService
				.spreadsheets()
				.get(spreadSheetId)
				.execute()
				.getProperties();
	}
}
