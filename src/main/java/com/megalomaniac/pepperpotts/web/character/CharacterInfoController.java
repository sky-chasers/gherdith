package com.megalomaniac.pepperpotts.web.character;

import com.megalomaniac.pepperpotts.db.tosaccount.TosAccount;
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccountRepository;
import com.megalomaniac.pepperpotts.db.tosaccount.TosAccountService;
import com.megalomaniac.pepperpotts.db.character.CharacterService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class CharacterInfoController {

	private final TosAccountService tosAccountService;
	private final TosAccountRepository tosAccountRepository;
	private final CharacterService characterService;

	public CharacterInfoController(TosAccountService characterInfoService,
								   TosAccountRepository tosAccountRepository,
								   CharacterService characterService) {
		this.tosAccountService = characterInfoService;
		this.tosAccountRepository = tosAccountRepository;
		this.characterService = characterService;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@PostMapping("/api/v1/character-info")
	public CharacterInfoRequest saveTeamNames(@Valid @RequestBody CharacterInfoRequest request) {
		Optional<TosAccount> characterOptional = tosAccountRepository.findByInGameAccountId(request.getAccountId());
		TosAccount character = characterOptional
				.orElseGet(() -> tosAccountService.createTosAccount(request.getAccountId()));

		characterService.updateTeamNames(character, request.getTeamNames());

		return request;
	}

}
