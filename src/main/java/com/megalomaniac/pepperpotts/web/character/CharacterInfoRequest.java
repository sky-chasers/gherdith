package com.megalomaniac.pepperpotts.web.character;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@JsonSerialize(using = CharacterInfoSerializer.class)
public class CharacterInfoRequest {

	@Getter
	@Setter
	@NotNull
	private String accountId;

	@Getter
	@Setter
	@NotNull
	private List<String> teamNames;

}
