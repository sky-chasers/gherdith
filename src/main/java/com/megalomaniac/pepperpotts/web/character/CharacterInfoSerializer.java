package com.megalomaniac.pepperpotts.web.character;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CharacterInfoSerializer extends JsonSerializer<CharacterInfoRequest> {

	@Override
	public void serialize(CharacterInfoRequest characterInfo, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		gen.writeStartObject();

		gen.writeStringField("accountId", characterInfo.getAccountId());

		gen.writeArrayFieldStart("characters");
		for (String s : characterInfo.getTeamNames()) {
			gen.writeString(s);
		}

		gen.writeEndArray();
		gen.writeEndObject();
	}
}
