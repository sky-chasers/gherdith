package com.megalomaniac.pepperpotts.web.bossbalita;

import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/************************************

Sample Request:

POST /api/v1/boss-balita?aid=123 HTTP/1.1
Host: localhost:6969
Content-Type: application/json
X-Boss-Balita-Account-Id: 76561198255116129
X-Boss-Balita-Key: abc123

{
	"bossLog": {
		"Demon Lord Mirtis": "03/26/2020-01:43.40",
		"Demon Lord Something": "02/24/2020-03:38.23"
	},
	"teamName": "Eggs"
}

**********************************/

public class BossBalitaBodyRequest {

	@Getter
	@Setter
	private Map<String, String> bossLog;

	@Getter
	@Setter
	private String teamName;

}
