package com.megalomaniac.pepperpotts.web.bossbalita;

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter;
import com.megalomaniac.pepperpotts.ghervis.api.DemonLord;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class BossBalitaRequest {

	@Getter
	@Setter
	private DemonLord boss;

	@Getter
	@Setter
	private LocalDateTime dateTimeAppeared;

	@Getter
	@Setter
	private BossBalitaReporter reportedBy;

	public String getEstimatedDateOfArrival() {
		return formatInServerTime(this.dateTimeAppeared.plusMinutes(10L));
	}

	public String getDateTimeAppearedInServerTimeFormat() {
		return formatInServerTime(this.dateTimeAppeared);
	}

	private String formatInServerTime(LocalDateTime localDateTime) {
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MMM dd yyyy h:mm:ss a 'SVT'");
		ZonedDateTime zonedDateTime = ZonedDateTime.of(localDateTime, ZoneId.of("Singapore"));
		return zonedDateTime.format(dateFormat);
	}
}
