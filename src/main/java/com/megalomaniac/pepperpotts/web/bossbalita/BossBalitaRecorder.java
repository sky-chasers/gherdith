package com.megalomaniac.pepperpotts.web.bossbalita;

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter;
import com.megalomaniac.pepperpotts.db.demonlord.DemonLordSpawnDetails;
import com.megalomaniac.pepperpotts.google.sheets.SheetWriter;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class BossBalitaRecorder {

	private final SheetWriter sheetWriter;
	private final String worldBossRecordsSheetId;

	public BossBalitaRecorder(SheetWriter sheetReader, String worldBossRecordsSheetId) {
		this.sheetWriter = sheetReader;
		this.worldBossRecordsSheetId = worldBossRecordsSheetId;
	}

	void recordWorldBossHistory(DemonLordSpawnDetails spawnDetails, BossBalitaReporter reporter) {
		String range = spawnDetails.getName().name() + "!A2:B";

		List<Object> row = new ArrayList<>();
		row.add((Object) spawnDetails.getServerTimeDate());
		row.add((Object) reporter.getId());

		List<List<Object>> body = new ArrayList<>();
		body.add(row);

		try {
			sheetWriter.write(worldBossRecordsSheetId, range, body);
		} catch (IOException e) {
			log.error("There was an error when appending new world boss record into google sheet {}", worldBossRecordsSheetId);
			e.printStackTrace();
		}

	}

}
