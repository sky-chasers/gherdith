package com.megalomaniac.pepperpotts.web.bossbalita;

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter;
import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporterRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Log4j2
@Controller
public class BossBalitaController {

	private final BossBalitaService bossBalitaService;
	private final BossBalitaReporterRepository bossBalitaReporterRepository;

	public BossBalitaController(BossBalitaService bossBalitaService, BossBalitaReporterRepository bossBalitaReporterRepository) {
		this.bossBalitaService = bossBalitaService;
		this.bossBalitaReporterRepository = bossBalitaReporterRepository;
	}

	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(value="/api/v1/boss-balita", consumes="application/json")
	public void announceBossArrival(@RequestHeader("X-Boss-Balita-Account-Id") String accountId,
									@RequestHeader("X-Boss-Balita-Key") String key,
									@Valid @RequestBody BossBalitaBodyRequest bossBalitaBodyRequest) {

		Optional<BossBalitaReporter> reporterResults = bossBalitaReporterRepository.findByInGameAccountIdAndKey(accountId, key);

		log.info("Received Boss Balita request from accountId: {}", accountId);

		if (reporterResults.isEmpty()) {
			log.info("Ignored Boss Balita request from accountId: {} (key: {}) due to unrecognized reporter.", accountId, key);
			return;
		}

		BossBalitaReporter reporter = reporterResults.get();
		log.info("Processing Boss Balita request from accountId: {} teamName: {}", accountId, reporter.getTeamName());

		Map<String, String> bossLog = bossBalitaBodyRequest.getBossLog();
		List<BossBalitaRequest> requestBodyList = bossBalitaService.generateRequests(reporter, bossLog);
		bossBalitaService.sendBossAlertToDiscord(requestBodyList);
	}
}
