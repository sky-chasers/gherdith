package com.megalomaniac.pepperpotts.web.bossbalita;

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter;
import com.megalomaniac.pepperpotts.db.demonlord.DemonLordRepository;
import com.megalomaniac.pepperpotts.db.demonlord.DemonLordService;
import com.megalomaniac.pepperpotts.db.demonlord.DemonLordSpawnDetails;
import com.megalomaniac.pepperpotts.ghervis.api.DemonLord;
import com.megalomaniac.pepperpotts.ghervis.api.GhervisApi;
import com.megalomaniac.pepperpotts.utils.DateUtils;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Log4j2
@Service
@Transactional
public class BossBalitaService {

	private final DateUtils dateUtils;
	private final String REQUEST_DATE_TIME_FORMAT = "MM/dd/yyyy-HH:mm.ss";
	private final DemonLordRepository demonLordRepository;
	private final DemonLordService demonLordService;
	private final GhervisApi ghervisApi;
	private final BossBalitaRecorder bossBalitaRecorder;

	private HashMap<DemonLord, LocalDateTime> demonTimeCache = new HashMap<>();

	public BossBalitaService(DateUtils dateUtils,
							 DemonLordRepository demonLordRepository,
							 DemonLordService demonLordService,
							 GhervisApi ghervisApi,
							 BossBalitaRecorder bossBalitaRecorder) {
		this.dateUtils = dateUtils;
		this.demonLordRepository = demonLordRepository;
		this.demonLordService = demonLordService;
		this.ghervisApi = ghervisApi;
		this.bossBalitaRecorder = bossBalitaRecorder;
	}

	List<BossBalitaRequest> generateRequests(BossBalitaReporter reportedBy, Map<String, String> rawRequest) {
		List<BossBalitaRequest> requestBodies = new ArrayList<>();

		rawRequest.forEach((bossName, timeAppeared) -> {
			LocalDateTime dateTimeAppeared = dateUtils.convertStringToLocalDateTime(timeAppeared, REQUEST_DATE_TIME_FORMAT);
			Optional<DemonLord> boss = DemonLord.findDemonLordByName(bossName);

			if (boss.isPresent()) {
				BossBalitaRequest bossBalitaRequestBody = new BossBalitaRequest();
				bossBalitaRequestBody.setBoss(boss.get());
				bossBalitaRequestBody.setDateTimeAppeared(dateTimeAppeared);
				bossBalitaRequestBody.setReportedBy(reportedBy);

				requestBodies.add(bossBalitaRequestBody);
			}

		}) ;

		return requestBodies;
	}

	void sendBossAlertToDiscord(List<BossBalitaRequest> bossBalitaRequests) {
		bossBalitaRequests.forEach(bossBalitaRequest ->  {
			LocalDateTime dateTimeBossAppearedWithoutSecs = bossBalitaRequest.getDateTimeAppeared().truncatedTo(ChronoUnit.MINUTES);
			DemonLord boss = bossBalitaRequest.getBoss();

			if (isPresentInCache(bossBalitaRequest) && isCalledRecently(boss, dateTimeBossAppearedWithoutSecs)) {
				log.info("Boss: {} has been reported recently. This post will be ignored.", bossBalitaRequest.getBoss().getName());
				return;
			}

			DemonLordSpawnDetails demonLordSpawnDetails = demonLordService
					.fetchOrCreateDemonLord(bossBalitaRequest.getBoss(), bossBalitaRequest.getDateTimeAppeared());

			demonTimeCache.put(demonLordSpawnDetails.getName(), bossBalitaRequest.getDateTimeAppeared());

			LocalDateTime lastSpawnTime = demonLordSpawnDetails.getDateAppeared().truncatedTo(ChronoUnit.MINUTES);

			if (lastSpawnTime.isBefore(dateTimeBossAppearedWithoutSecs)) {
				demonLordSpawnDetails.setDateAppeared(bossBalitaRequest.getDateTimeAppeared());
			}

			ghervisApi.sendBossBalitaAlertToGhervis(bossBalitaRequest, demonLordSpawnDetails.getDiscordTagId());
			bossBalitaRecorder.recordWorldBossHistory(demonLordSpawnDetails, bossBalitaRequest.getReportedBy());
			demonLordRepository.save(demonLordSpawnDetails);
		});
	}

	private boolean isPresentInCache(BossBalitaRequest bossBalitaRequest) {
		return demonTimeCache.get(bossBalitaRequest.getBoss()) != null;
	}

	private boolean isCalledRecently(DemonLord boss, LocalDateTime dateTimeBossAppeared) {
		LocalDateTime startTime = demonTimeCache.get(boss).truncatedTo(ChronoUnit.MINUTES);
		LocalDateTime endTime = startTime.plusHours(3);

		boolean isWithinSpawnDuration = dateTimeBossAppeared.isAfter(startTime) && dateTimeBossAppeared.isBefore(endTime);

		return  dateTimeBossAppeared.equals(startTime) || isWithinSpawnDuration;
	}
}
