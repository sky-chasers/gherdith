package com.megalomaniac.pepperpotts.web.bossbalita;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BossBalitaConfig {

	@Value("${megalomaniac.pepperpotts.googleSheets.bossBalitaRecords}")
	private String worldBossRecordsSheetId;

	@Bean(name = "worldBossRecordsSheetId")
	public String getWorldBossRecordsSheetId() {
		return worldBossRecordsSheetId;
	}

	@Value("${megalomaniac.pepperpotts.discord.channels.worldBoss}")
	private String worldBossChannelId;

	@Bean(name = "worldBossChannelId")
	public String getWorldBossChannelId() {
		return worldBossChannelId;
	}
}
