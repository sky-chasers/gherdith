package com.megalomaniac.pepperpotts.web.bossbalitareporter;

import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporter;
import com.megalomaniac.pepperpotts.db.bossbalitareporter.BossBalitaReporterRepository;
import com.megalomaniac.pepperpotts.ghervis.api.GhervisApi;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.DiscordApi;
import org.javacord.api.entity.user.User;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Optional;
import java.util.concurrent.CompletionException;

@Log4j2
@Controller
public class BossBalitaReporterController {

	private final BossBalitaReporterRepository bossBalitaReporterRepository;
	private final DiscordApi pepperPotDiscordApi;
	private final GhervisApi ghervisApi;

	public BossBalitaReporterController(BossBalitaReporterRepository bossBalitaReporterRepository,
										DiscordApi pepperPotDiscordApi, GhervisApi ghervisApi) {
		this.bossBalitaReporterRepository = bossBalitaReporterRepository;
		this.pepperPotDiscordApi = pepperPotDiscordApi;
		this.ghervisApi = ghervisApi;
	}

	@ResponseBody
	@PostMapping(value = "/api/v1/boss-balita/init", consumes = "application/json")
	public ResponseEntity setupReporter(@RequestHeader("X-Boss-Balita-Account-Id") String accountId,
							  @RequestHeader("X-Boss-Balita-Key") String key) {

		log.info("Setting up boss balita reporter... accountId: {}", accountId);

		Optional<BossBalitaReporter> reporterResults = bossBalitaReporterRepository.findByInGameAccountIdAndKey(accountId, key);

		try {

			if (reporterResults.isPresent()) {
				User user = pepperPotDiscordApi.getUserById(reporterResults.get().getDiscordUserId()).join();

				ghervisApi.sendSuccessfulBossBalitaReporterSetupMessage(user);
				return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.ACCEPTED);
			}

			log.info("Failed to fetch reporter with accountId: {}. Please generate keys first.", accountId);
			return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.NOT_FOUND);
		} catch (CompletionException e) {
			e.printStackTrace();
			log.info("Failed to setup accountId: {}, discordId is invalid.", accountId);
			return new ResponseEntity<Void>(new HttpHeaders(), HttpStatus.NOT_FOUND);
		}
	}

}
