package com.megalomaniac.pepperpotts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication(scanBasePackages = {"com.megalomaniac.pepperpotts", "com.megalomaniac.pepperpotts.web.character"})
public class PepperPottsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PepperPottsApplication.class, args);
	}

}
