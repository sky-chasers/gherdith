package com.megalomaniac.pepperpotts.ghervis.api;

import com.megalomaniac.pepperpotts.utils.PostRequestException;
import com.megalomaniac.pepperpotts.utils.RestApi;
import com.megalomaniac.pepperpotts.web.bossbalita.BossBalitaRequest;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.entity.user.User;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Log4j2
@Service
public class GhervisApi {

	private final RestApi restApi;
	private final BossBalitaRequestBuilder bossBalitaRequestBuilder;
	private final ReporterSetupMessageBuilder reporterSetupMessageBuilder;
	private String sendChannelMessageUrl;
	private String sendPrivateMessageUrl;

	public GhervisApi(RestApi restApi,
					  BossBalitaRequestBuilder bossBalitaRequestBuilder,
					  ReporterSetupMessageBuilder successReporterSetupMessageBuilder,
					  String ghervisBaseUrl) {

		this.restApi = restApi;
		this.bossBalitaRequestBuilder = bossBalitaRequestBuilder;
		this.reporterSetupMessageBuilder = successReporterSetupMessageBuilder;
		this.sendChannelMessageUrl = ghervisBaseUrl + "send-channel-message";
		this.sendPrivateMessageUrl = ghervisBaseUrl + "send-private-message";
	}

	public void sendChannelMessage(JSONObject messageDetails) {
		try {
			log.info("Sending message via Ghervis...");
			restApi.postRequest(sendChannelMessageUrl, messageDetails);
		} catch (PostRequestException e) {
			log.error("Unable to send a POST Request to {}", sendChannelMessageUrl);
		}
	}

	public void sendSuccessfulBossBalitaReporterSetupMessage(User user) {
		try {

			log.info("{} successfully setup as boss balita reporter. ", user.getDiscriminatedName());

			JSONObject jsonRequest = reporterSetupMessageBuilder.buildSuccessSetupMessage(user);
			restApi.postRequest(sendPrivateMessageUrl, jsonRequest);

		} catch (PostRequestException e) {
			log.error("Unable to send a POST Request to {}", sendPrivateMessageUrl);
		}
	}

	public void sendFailedBossBalitaReporterSetupMessage(User user) {
		try {

			log.info("{} failed to sign up as a boss balita reporter. ", user.getDiscriminatedName());

			JSONObject jsonRequest = reporterSetupMessageBuilder.buildFailedSetupMessage(user);
			restApi.postRequest(sendPrivateMessageUrl, jsonRequest);

		} catch (PostRequestException e) {
			log.error("Unable to send a POST Request to {}", sendPrivateMessageUrl);
		}
	}

	public void sendBossBalitaAlertToGhervis(BossBalitaRequest request, Long discordTagId) {
		try {

			log.info("Sending boss alert to Ghervis : {}", request.getBoss().getName());

			JSONObject jsonRequest = bossBalitaRequestBuilder.buildRequest(request, discordTagId);
			restApi.postRequest(sendChannelMessageUrl, jsonRequest);

			log.info("Successfully sent boss balita request with details: BossName: {}, DateTimeAppeared: {}, ReportedBy: {}",
					request.getBoss().getName(), request.getDateTimeAppeared(), request.getReportedBy().getTeamName());

		} catch (PostRequestException e) {
			log.error("Unable to send a POST Request to {}", sendChannelMessageUrl);
		}

	}
}
