package com.megalomaniac.pepperpotts.ghervis.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GhervisApiConfig {

	@Value("${megalomaniac.ghervis.discord.base.url}")
	private String ghervisBaseUrl;

	@Bean(name = "ghervisBaseUrl")
	public String getGhervisBaseUrl() {
		return ghervisBaseUrl;
	}

	@Value("${megalomaniac.pepperpotts.discord.roles.allWorldBossTagId}")
	private Long allWorldBossTagId;

	@Bean(name = "allWorldBossTagId")
	public Long getAllWorldBossTagId() {
		return allWorldBossTagId;
	}

}
