package com.megalomaniac.pepperpotts.ghervis.api;

import org.javacord.api.entity.user.User;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class ReporterSetupMessageBuilder {

	JSONObject buildSuccessSetupMessage(User user) {

		String description = "Hello " + user.getMentionTag() + "! You have successfully setup your boss reporter account!";

		JSONObject embedValue = new JSONObject()
				.put("color", "#4CAF50")
				.put("title", ":white_check_mark: Boss reporter setup is complete!")
				.put("description", description);

		return buildMessage(embedValue, user);
	}

	JSONObject buildFailedSetupMessage(User user) {

		String description = "Hello " + user.getMentionTag() + "! Unfortunately we cannot find your account in our database. \n" +
				"Please contact our admins for assistance.";

		JSONObject embedValue = new JSONObject()
				.put("color", "#E63B37")
				.put("title", ":warning: Boss reporter setup failed!")
				.put("description", description);

		return buildMessage(embedValue, user);
	}

	private JSONObject buildMessage(JSONObject embedValue, User user) {
		JSONObject message = new JSONObject()
				.put("content", user.getMentionTag())
				.put("embed", embedValue);

		String discordId = String.valueOf(user.getId());

		return new JSONObject()
				.put("discordUserId", discordId)
				.put("message", message);
	}
}
