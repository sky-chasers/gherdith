package com.megalomaniac.pepperpotts.ghervis.api;

public interface DemonLordDetails {

	String getMapName();
	String getImageUrl();
	String getMapUrl();
	String getName();

}
