package com.megalomaniac.pepperpotts.ghervis.api;

import java.util.Optional;

public enum DemonLord implements DemonLordDetails {

	DEMON_LORD_MARNOX() {
		@Override
		public String getMapName() {
			return "Inner Wall District 8";
		}

		@Override
		public String getImageUrl() {
			return "https://tos.guru/assets/icons/boss_marnoks.png";
		}

		@Override
		public String getMapUrl() {
			return "https://i.imgur.com/OCpC52a.png";
		}

		@Override
		public String getName() {
			return "Demon Lord Marnox";
		}
	},

	DEMON_LORD_MIRTIS() {
		@Override
		public String getMapName() {
			return "Inner Wall District 8";
		}

		@Override
		public String getImageUrl() {
			return "https://tos.guru/assets/icons/boss_mirtis.png";
		}

		@Override
		public String getMapUrl() {
			return "https://i.imgur.com/xEPdclu.png";
		}

		@Override
		public String getName() {
			return "Demon Lord Mirtis";
		}
	},

	DEMON_LORD_BLUT() {
		@Override
		public String getMapName() {
			return "Emmet Forest";
		}

		@Override
		public String getImageUrl() {
			return "https://tos.guru/assets/icons/boss_blud.png";
		}

		@Override
		public String getMapUrl() {
			return "https://i.imgur.com/z3uoOVi.png";
		}

		@Override
		public String getName() {
			return "Demon Lord Blut";
		}
	},

	DEMON_LORD_HELGASERCLE() {
		@Override
		public String getMapName() {
			return "Inner Wall District 8";
		}

		@Override
		public String getImageUrl() {
			return "https://tos.guru/assets/icons/boss_helgasercle.png";
		}

		@Override
		public String getMapUrl() {
			return "https://i.imgur.com/AltbsZ6.png";
		}

		@Override
		public String getName() {
			return "Demon Lord Helgasercle";
		}
	},

	DEMON_LORD_MORINGPONIA() {
		@Override
		public String getMapName() {
			return "Northern Parias Forest";
		}

		@Override
		public String getImageUrl() {
			return "https://tos.guru/assets/icons/boss_moringponia.png";
		}

		@Override
		public String getMapUrl() {
			return "https://i.imgur.com/fbiTa7e.png";
		}

		@Override
		public String getName() {
			return "Demon Lord Moringponia";
		}
	},

	DEMON_LORD_NUAELE() {
		@Override
		public String getMapName() {
			return "Emmet Forest";
		}

		@Override
		public String getImageUrl() {
			return "https://tos.guru/assets/icons/boss_nuaelle.png";
		}

		@Override
		public String getMapUrl() {
			return "https://i.imgur.com/mE1K74I.png";
		}

		@Override
		public String getName() {
			return "Demon Lord Nuaele";
		}
	},

	DEMON_LORD_REXIPHER() {
		@Override
		public String getMapName() {
			return "Inner Wall District 8";
		}

		@Override
		public String getImageUrl() {
			return "https://tos.guru/assets/icons/boss_lecifer.png";
		}

		@Override
		public String getMapUrl() {
			return "https://i.imgur.com/YCraXUx.png";
		}

		@Override
		public String getName() {
			return "Demon Lord Rexipher";
		}
	},

	DEMON_LORD_ZAURA() {
		@Override
		public String getMapName() {
			return "Emmet Forest";
		}

		@Override
		public String getImageUrl() {
			return "https://tos.guru/assets/icons/boss_zawra.png";
		}

		@Override
		public String getMapUrl() {
			return "https://i.imgur.com/QPgjIsg.png";
		}

		@Override
		public String getName() {
			return "Demon Lord Zaura";
		}
	},

	DEMON_LORD_SKIACLIPSE() {
		@Override
		public String getMapName() {
			return "Vienibe Shelter";
		}

		@Override
		public String getImageUrl() {
			return "https://tos.guru/assets/icons/boss_skiaclipse.png";
		}

		@Override
		public String getMapUrl() {
			return "https://i.imgur.com/s8sP1GU.png";
		}

		@Override
		public String getName() {
			return "Skiaclipse";
		}
	},;

	public static Optional<DemonLord> findDemonLordByName(String name) {
		for(DemonLord details : values()){
			if(details.getName().equals(name)){
				return Optional.of(details);
			}
		}
		return Optional.empty();
	}
}
