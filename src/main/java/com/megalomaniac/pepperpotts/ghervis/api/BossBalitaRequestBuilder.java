package com.megalomaniac.pepperpotts.ghervis.api;

import com.megalomaniac.pepperpotts.web.bossbalita.BossBalitaRequest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

/** Sample Request:
 {
	 "message":{
		 "embed":{
			 "image":{
			 	"url":"https://i.imgur.com/xEPdclu.png"
		 	},
	 	"thumbnail":{
	 		"url":"https://tos.guru/assets/icons/boss_mirtis.png"
	 	},
	 	"color":16759296,
	 	"footer":{
	 		"icon_url":"https://i.imgur.com/Xuo0yh6.png",
	 		"text":"*SVT = Server Time"
	 	},
	 	"title":"Demon Lord Mirtis",
		 "fields":[
			 {
				"skill":"**Server Time Announcement**",
				"value":"Sep 06 2020 2:36 PM SVT"
			 },
			 {
				"skill":"**Estimated Time of Arrival**",
				"value":"Sep 06 2020 2:46 PM SVT"
			 },
			 {
				 "skill":"**Location**",
				"value":"Inner Wall District 8"
			 }
		 ]
	 },
 	"content":"<@&751396954214367308> "
	 },
	 "channelId":"598150065034625056"
 }
 **/

@Service
public class BossBalitaRequestBuilder {

	private final String worldBossChannelId;
	private final Long allWorldBossTagId;

	public BossBalitaRequestBuilder(String worldBossChannelId, Long allWorldBossTagId) {
		this.worldBossChannelId = worldBossChannelId;
		this.allWorldBossTagId = allWorldBossTagId;
	}

	JSONObject buildRequest(BossBalitaRequest request, Long discordTagId) {

		JSONObject timeOfAnnouncement = new JSONObject()
				.put("skill", "**Server Time Announcement**")
				.put("value", request.getDateTimeAppearedInServerTimeFormat());

		JSONObject estimatedTimeOfArrival = new JSONObject()
				.put("skill", "**Estimated Time of Arrival**")
				.put("value", request.getEstimatedDateOfArrival());

		JSONObject mapName = new JSONObject()
				.put("skill", "**Location**")
				.put("value", request.getBoss().getMapName());

		String subscriptionMessage = "Click :bell: to subscribe to this boss." +
				"\nClick :a: to subscribe to all bosses.";

		JSONObject subscriptionMessageField = new JSONObject()
				.put("skill", "\u200B")
				.put("value", subscriptionMessage);

		JSONArray fields = new JSONArray()
				.put(timeOfAnnouncement)
				.put(estimatedTimeOfArrival)
				.put(mapName)
				.put(subscriptionMessageField);

		JSONObject mapImageValue = new JSONObject()
				.put("url", request.getBoss().getMapUrl());

		JSONObject thumbnailValue = new JSONObject()
				.put("url", request.getBoss().getImageUrl());

		JSONObject footer = new JSONObject()
				.put("text", "*SVT = Server Time")
				.put("icon_url", "https://i.imgur.com/Xuo0yh6.png");

		int COLOR = 16759296;
		JSONObject embedValue = new JSONObject()
				.put("color", COLOR)
				.put("title", request.getBoss().getName())
				.put("thumbnail", thumbnailValue)
				.put("fields", fields)
				.put("image", mapImageValue)
				.put("footer", footer);

		String tagContent = appendTag(allWorldBossTagId) + " " + appendTag(discordTagId);
		JSONObject message = new JSONObject()
				.put("embed", embedValue)
				.put("content", tagContent);

		return new JSONObject()
				.put("channelId", worldBossChannelId)
				.put("message", message);
	}

	private String appendTag(Long tagId) {
		if (tagId != null) {
			return "<@&" + tagId + ">";
		}

		return "";
	}
}
