package com.megalomaniac.pepperpotts;

import com.megalomaniac.pepperpotts.discord.announce.AnnounceCommandListener;
import com.megalomaniac.pepperpotts.discord.boruta.attendance.filler.AttendanceFillerListener;
import com.megalomaniac.pepperpotts.discord.boruta.bonuspoints.filler.BonusPointsFillerListener;
import com.megalomaniac.pepperpotts.discord.bossbalita.GenerateBossBalitaKeyListener;
import com.megalomaniac.pepperpotts.discord.character.whois.WhoIsListener;
import com.megalomaniac.pepperpotts.discord.character.whoisaccountid.WhoIsAccountIdListener;
import com.megalomaniac.pepperpotts.discord.gtw.salary.CalculateGtwSalaryListener;
import com.megalomaniac.pepperpotts.discord.help.admin.AdminHelpListener;
import com.megalomaniac.pepperpotts.discord.help.user.UserHelpListener;
import com.megalomaniac.pepperpotts.discord.tos.news.NewsGrabberListener;
import lombok.extern.log4j.Log4j2;
import org.javacord.api.DiscordApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Log4j2
@Configuration
public class DiscordConfig {

	private CalculateGtwSalaryListener calculateGtwSalaryListener;
	private WhoIsAccountIdListener whoIsAccountIdListener;
	private WhoIsListener whoIsListener;
	private NewsGrabberListener newsGrabberListener;
	private UserHelpListener userHelpListener;
	private AttendanceFillerListener attendanceFillerListener;
	private BonusPointsFillerListener bonusPointsFillerListener;
	private GenerateBossBalitaKeyListener generateBossBalitaKeyListener;
	private AdminHelpListener adminHelpListener;
	private AnnounceCommandListener announceCommandListener;
	private DiscordApi pepperPotDiscordApi;

	public DiscordConfig(CalculateGtwSalaryListener calculateGtwSalaryListener,
						 WhoIsAccountIdListener whoIsAccountIdListener,
						 WhoIsListener whoIsListener,
						 NewsGrabberListener newsGrabberListener,
						 UserHelpListener userHelpListener,
						 AttendanceFillerListener attendanceFillerListener,
						 BonusPointsFillerListener bonusPointsFillerListener,
						 GenerateBossBalitaKeyListener generateBossBalitaKeyListener,
						 AdminHelpListener adminHelpListener,
						 AnnounceCommandListener announceCommandListener,
						 DiscordApi pepperPotDiscordApi) {
		this.calculateGtwSalaryListener = calculateGtwSalaryListener;
		this.whoIsAccountIdListener = whoIsAccountIdListener;
		this.whoIsListener = whoIsListener;
		this.newsGrabberListener = newsGrabberListener;
		this.userHelpListener = userHelpListener;
		this.attendanceFillerListener = attendanceFillerListener;
		this.bonusPointsFillerListener = bonusPointsFillerListener;
		this.generateBossBalitaKeyListener = generateBossBalitaKeyListener;
		this.adminHelpListener = adminHelpListener;
		this.pepperPotDiscordApi = pepperPotDiscordApi;
		this.announceCommandListener = announceCommandListener;
	}

	@Bean
	public void configureListeners() {
		log.info("Setting up listeners....");
		pepperPotDiscordApi.addListener(calculateGtwSalaryListener);
		pepperPotDiscordApi.addListener(whoIsAccountIdListener);
		pepperPotDiscordApi.addListener(whoIsListener);
		pepperPotDiscordApi.addListener(newsGrabberListener);
		pepperPotDiscordApi.addListener(userHelpListener);
		pepperPotDiscordApi.addListener(attendanceFillerListener);
		pepperPotDiscordApi.addListener(bonusPointsFillerListener);
		pepperPotDiscordApi.addListener(generateBossBalitaKeyListener);
		pepperPotDiscordApi.addListener(adminHelpListener);
		pepperPotDiscordApi.addListener(announceCommandListener);
	}

}
