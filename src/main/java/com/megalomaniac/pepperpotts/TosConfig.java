package com.megalomaniac.pepperpotts;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TosConfig {

	@Bean(name = "tosAllNewsBaseUrl")
	public String getTosAllNewsBaseUrl() {
		return "https://treeofsavior.com/page/news/?n=1";
	}

	@Bean(name = "tosHomePageUrl")
	public String getTosHomePageUrl() {
		return "https://treeofsavior.com";
	}

	@Bean(name = "tosAnnouncementsUrl")
	public String getTosAnnouncementsUrl() {
		return "https://treeofsavior.com/page/news/?c=2&n=1";
	}

	@Bean(name = "tosEventsUrl")
	public String getTosEventsUrl() {
		return "https://treeofsavior.com/page/news/?c=33&n=1";
	}

	@Bean(name = "tosPatchUrl")
	public String getTosPatchUrl() {
		return "https://treeofsavior.com/page/news/?c=3&n=1";
	}

	@Bean(name = "tosBlogUrl")
	public String getTosBlogUrl() {
		return "https://treeofsavior.com/page/news/?c=31&n=1";
	}

	@Bean(name = "tosIssuesUrl")
	public String getTosIssuesUrl() {
		return "https://treeofsavior.com/page/news/?c=32&n=1";
	}

	@Bean(name = "tosIconUrl")
	public String getTosIconUrl() {
		return "https://treeofsavior.com/img/common/logo.png";
	}

	@Bean(name = "ktestNewsBaseUrl")
	public String getKtestNewsBaseUrl() {
		return "https://tos.nexon.com/ts/notice/list";
	}

	@Bean(name = "ktestHomePageUrl")
	public String getKtestHomePageUrl() {
		return "https://tos.nexon.com";
	}

	@Bean(name = "ktestServerHomePageUrl")
	public String getKtestServerHomePageUrl() {
		return "https://tos.nexon.com/ts/download/index";
	}

	@Bean(name = "ktestGeneralBulletinBoardUrl")
	public String getKtestGeneralBulletinBoardUrl() {
		return "https://tos.nexon.com/ts/board/List?n4ArticleCategorySN=1";
	}

	@Bean(name = "ktosHomePageUrl")
	public String getKtosHomePageUrl() {
		return "https://tos.nexon.com/main/index";
	}

	@Bean(name = "ktosNewsPageUrl")
	public String getKtosNewsPageUrl() {
		return "https://tos.nexon.com/news/tosnotice/list";
	}

	@Bean(name = "ktosAllNewsBaseUrl")
	public String getKtosAllNewsBaseUrl() {
		return "https://tos.nexon.com";
	}

	@Bean(name = "ktosUpdatesUrl")
	public String getKtosUpdatesUrl() {
		return "https://tos.nexon.com/news/update/list";
	}

	@Bean(name = "ktosEventPageUrl")
	public String getKtosEventPageUrl() {
		return "https://tos.nexon.com/news/events/list";
	}

	@Bean(name = "ktosDevBlogUrl")
	public String getKtosDevBlogUrl() {
		return "https://tos.nexon.com/lab/dev/list";
	}

	@Bean(name = "ktosFeatureUrl")
	public String getKtosFeatureUrl() {
		return "https://tos.nexon.com/info/feature";
	}

	@Bean(name = "ktosCommunityBoard")
	public String getFreeBoard() {
		return "https://tos.nexon.com/lab/suggest/Index";
	}
}

