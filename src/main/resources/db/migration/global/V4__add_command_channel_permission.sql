CREATE TABLE command_channel_permission (
    id bigint NOT NULL,
    command character varying(255) NOT NULL,
    allowed_channel bigint
);

ALTER TABLE ONLY command_channel_permission
    ADD CONSTRAINT command_channel_permission_pkey PRIMARY KEY (id);

CREATE SEQUENCE command_channel_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

