CREATE TABLE demon_lord_spawn_details (
    id bigint NOT NULL,
    name character varying(255),
    date_appeared timestamp without time zone,
    CONSTRAINT demon_lord_spawn_details_name_unique UNIQUE (name)
);

ALTER TABLE ONLY demon_lord_spawn_details
    ADD CONSTRAINT demon_lord_pkey PRIMARY KEY (id);

CREATE SEQUENCE demon_lord_spawn_details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
