CREATE TABLE boss_balita_reporter (
    id bigint NOT NULL,
    key character varying(255) NOT NULL,
    team_name character varying(255) NOT NULL,
    in_game_account_id character varying(255) NOT NULL
);

ALTER TABLE ONLY "boss_balita_reporter"
    ADD CONSTRAINT boss_balita_reporter_pkey PRIMARY KEY (id);

CREATE SEQUENCE boss_balita_reporter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
