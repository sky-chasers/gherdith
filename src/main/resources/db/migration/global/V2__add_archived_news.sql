CREATE TABLE archived_news (
    id bigint NOT NULL,
    url character varying(255),
    date_published_by_imc DATE
);

ALTER TABLE ONLY archived_news
    ADD CONSTRAINT archived_news_pkey PRIMARY KEY (id);

CREATE SEQUENCE archived_news_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
