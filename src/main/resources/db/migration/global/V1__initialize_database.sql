CREATE TABLE "character" (
    id bigint NOT NULL,
    date_updated timestamp without time zone,
    team_name character varying(255) NOT NULL,
    tos_account_id bigint
);

ALTER TABLE ONLY "character"
    ADD CONSTRAINT character_pkey PRIMARY KEY (id);

CREATE SEQUENCE character_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE tos_account (
    id bigint NOT NULL,
    in_game_account_id character varying(255)
);

ALTER TABLE ONLY tos_account
    ADD CONSTRAINT tos_account_pkey PRIMARY KEY (id);

CREATE SEQUENCE tos_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE ONLY "character"
    ADD CONSTRAINT fkililcx704ox5mbp63vckdsf64 FOREIGN KEY (tos_account_id) REFERENCES tos_account(id);