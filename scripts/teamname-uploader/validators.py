import sys
import environments

def validateArguments():
	if(len(sys.argv) != 3 ):
		print_usage()
		sys.exit(0)

	environment = str(sys.argv[2])
	is_valid_environment = environment in environments.valid_environments

	if(not is_valid_environment):
		print_usage()
		sys.exit(0)


def print_usage():
	print ("ERROR! ----------------------------------------------------")
	print ("USAGE: main.py <filename.json> <environment>")
	print ("filename = required, must be a json file.")
	print ("environment = required, can be one of the following: ", str(environments.valid_environments))