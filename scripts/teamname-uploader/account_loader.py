import sys
import json
import account as account_factory

def load_accounts():
	json_file_name = sys.argv[1]
	accounts = []

	print ("Loading accounts from json file: " + json_file_name)

	with open(json_file_name) as json_data_file:
		accounts_json = json.load(json_data_file)
		json_data_file.close()

		for (account_id, team_names_json) in accounts_json.items():
			team_names = extract_team_names(team_names_json)
			account = account_factory.Account(account_id, team_names)
			accounts.append(account)

	return accounts


def extract_team_names(team_names_json):
	team_names = []
	for(team_name, date_updated) in team_names_json.items():
	    team_names.append(team_name)

	return team_names