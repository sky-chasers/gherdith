
valid_environments = ["staging", "production", "local"]

environments = {
	"staging" : {
		"endpoint" : "https://megalo-pepperpotts.herokuapp.com/"
	},
	"production": {
		"endpoint" : "https://megalo-pepperpotts-prod.herokuapp.com/"
	},
	"local": {
		"endpoint" : "http://localhost:6969/"
	}
}
