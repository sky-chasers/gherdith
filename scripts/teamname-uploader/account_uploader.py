import account as account_factory
import requests
import json

def upload_account(endpoint, account):
	print("uploading account with id: ", account.id, " to: ", endpoint)

	save_team_name_endpoint = endpoint + "api/v1/character-info"

	headers = {
		"Content-Type": "application/json"
	}

	data = {
		"accountId": account.id,
		"teamNames": account.team_names
	}

	response = requests.post(url = save_team_name_endpoint, data = json.dumps(data), headers = headers)

	print ("response: ", response.text)
