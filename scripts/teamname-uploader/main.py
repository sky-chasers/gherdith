import validators
import account_loader
import account_uploader
import environments
import sys

validators.validateArguments()

selected_environment = str(sys.argv[2])
endpoint = environments.environments[selected_environment]['endpoint']
accounts = account_loader.load_accounts()

for account in accounts:
	account_uploader.upload_account(endpoint,account)

print ("Done uploading...")